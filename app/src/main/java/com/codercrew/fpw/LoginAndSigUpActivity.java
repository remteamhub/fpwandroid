package com.codercrew.fpw;

import androidx.appcompat.app.AppCompatActivity;

import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.Toast;

import com.codercrew.fpw.Utils.utils;


public class LoginAndSigUpActivity extends AppCompatActivity {

    Button login,signUp;
    public static LoginAndSigUpActivity loginAndSigUpActivity;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_login_and_sig_up);
        loginAndSigUpActivity = this;
        move();
    }

    private void move() {
        login = findViewById(R.id.login);
        signUp = findViewById(R.id.signUp);

        login.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if(utils.CheckInternet(getApplicationContext())) {
                    Intent intent = new Intent(getApplicationContext(), LoginActivity.class);
                    startActivity(intent);
                }else
                    Toast.makeText(LoginAndSigUpActivity.this, "Check your internet connection!", Toast.LENGTH_SHORT).show();


            }
        });

        signUp.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if(utils.CheckInternet(getApplicationContext())) {
                    Intent intent = new Intent(getApplicationContext(), SignUpActivity.class);
                    startActivity(intent);
                }else
                    Toast.makeText(LoginAndSigUpActivity.this, "Check your internet connection!", Toast.LENGTH_SHORT).show();

            }
        });
    }


}