package com.codercrew.fpw;

import androidx.annotation.Nullable;
import androidx.appcompat.app.AppCompatActivity;
import androidx.lifecycle.Observer;
import androidx.lifecycle.ViewModelProvider;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import android.app.ProgressDialog;
import android.content.Intent;
import android.os.Bundle;
import android.text.Editable;
import android.text.TextWatcher;
import android.view.View;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;

import com.codercrew.fpw.Utils.SharedPrefrencesMain;
import com.codercrew.fpw.adapter.SuggestedUsersAdapter;
import com.codercrew.fpw.adapter.SuggestedUsersChooseAdapter;
import com.codercrew.fpw.chat.ChatActivity;
import com.codercrew.fpw.home.interfaces.OnItemClick;
import com.codercrew.fpw.home.ui.profile.ProfileViewModel;
import com.codercrew.fpw.model.SignUp;
import com.codercrew.fpw.model.User;

import java.util.ArrayList;
import java.util.List;

public class GroupMessagesActivity extends AppCompatActivity {
    public static GroupMessagesActivity groupMessagesActivity;
    private ProfileViewModel profileViewModel;
    private SuggestedUsersChooseAdapter familyMembersAdapter;
    ProgressDialog pd;
    private List<User> user = new ArrayList<>();
    private SharedPrefrencesMain sharedPrefrencesMain;
    RecyclerView recyclerView;
    TextView textViewNext;
    EditText editTextSearch;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_group_messages);

        groupMessagesActivity = this;
        pd =new ProgressDialog(this);
        pd.setMessage(getString(R.string.loading));
        recyclerView = findViewById(R.id.recyclerView);
        textViewNext = findViewById(R.id.tvNext);
        editTextSearch = findViewById(R.id.etTo);


        LinearLayoutManager layoutManager = new LinearLayoutManager(this,RecyclerView.VERTICAL,false);
//        layoutManager.setStackFromEnd(true);
        recyclerView.setLayoutManager(layoutManager);
        recyclerView.addOnLayoutChangeListener((view, left, top, right, bottom, oldLeft, oldTop, oldRight, oldBottom) -> {
            if (bottom < oldBottom) {
                recyclerView.scrollBy(0, oldBottom - bottom);
            }
        });

        textViewNext.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if(familyMembersAdapter==null || familyMembersAdapter.GetSelectedUsers().size()==0){
                    Toast.makeText(GroupMessagesActivity.this, "Select users to continue", Toast.LENGTH_SHORT).show();
                    return;
                }
                Intent intent = new Intent(GroupMessagesActivity.this, CreateGroupActivity.class);
                intent.putExtra("user", familyMembersAdapter.GetSelectedUsers());
                startActivity(intent);
            }
        });

        editTextSearch.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {

            }

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {
                familyMembersAdapter.getFilter().filter(s);
            }

            @Override
            public void afterTextChanged(Editable s) {

            }
        });
//        recyclerView.setOnClickListener(this);
        profileViewModel =
                new ViewModelProvider(this).get(ProfileViewModel.class);

        pd.show();
        profileViewModel.hitProfileApi();

        profileViewModel.getText().observe(this, new Observer<SignUp>() {
            @Override
            public void onChanged(@Nullable SignUp familyMembersModel) {
                pd.dismiss();
                if(familyMembersModel!=null && familyMembersModel.getData()!=null) {
                    pd.dismiss();
                    if(familyMembersModel.getData().getMembers()!=null && familyMembersModel.getData().getMembers().size()>0) {
                        user = familyMembersModel.getData().getMembers();
                        familyMembersAdapter = new SuggestedUsersChooseAdapter(getApplicationContext(), familyMembersModel.getData().getMembers(), new OnItemClick() {
                            @Override
                            public void onClick(View view, int pos) {
                                if(view.getId()==R.id.imgMenu){
                                    familyMembersAdapter.SelectUser(pos);
                                }
                            }
                        });
                        recyclerView.setAdapter(familyMembersAdapter);
                    }
                }
            }
        });
    }
}