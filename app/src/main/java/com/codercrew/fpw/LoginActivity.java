package com.codercrew.fpw;

import androidx.appcompat.app.AppCompatActivity;
import androidx.constraintlayout.widget.ConstraintLayout;

import android.app.Activity;
import android.app.ProgressDialog;
import android.content.Intent;
import android.os.Bundle;
import android.text.Editable;
import android.text.TextWatcher;
import android.view.View;
import android.view.inputmethod.InputMethodManager;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;


import com.codercrew.fpw.ApiClient.ApiClient;
import com.codercrew.fpw.ApiClient.ApiInterface;
import com.codercrew.fpw.Utils.SharedPrefrencesMain;
import com.codercrew.fpw.Utils.utils;
import com.codercrew.fpw.home.MainActivity;
import com.codercrew.fpw.model.SignUp;
import com.google.firebase.iid.FirebaseInstanceId;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class LoginActivity extends AppCompatActivity {
    ConstraintLayout constraintLayout;
    EditText email,password;
    Button loginBtn;
    TextView forgotPassword;
    SharedPrefrencesMain sharedPrefrencesMain;
    ProgressDialog pd;



    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_login);

        email = findViewById(R.id.emailEditText);
        password = findViewById(R.id.passwordEditText);
        loginBtn = findViewById(R.id.loginBtn);
        forgotPassword = findViewById(R.id.forgotPassword);
        sharedPrefrencesMain = SharedPrefrencesMain.getInstance(this);
        pd =new ProgressDialog(LoginActivity.this);
        pd.setMessage(getString(R.string.loading));

        loginBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                SetValidations();
            }
        });

        constraintLayout = findViewById(R.id.constraintLogin);
        constraintLayout.setOnFocusChangeListener(new View.OnFocusChangeListener() {
            @Override
            public void onFocusChange(View v, boolean hasFocus) {
                if(hasFocus) {
                    hideKeyboard(v);
                }
            }
        });
        setForgotPassword();

        email.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {

            }

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {
                String str = s.toString();
                if(str.length() > 0 && str.contains(" "))
                {
//                    emailAddress.setError("Space is not allowed");
                    email.setText(s.toString().replace(" ",""));
                    email.setSelection(email.getText().length());
                }
            }

            @Override
            public void afterTextChanged(Editable s) {

            }
        });

        password.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {

            }

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {
                String str = s.toString();
                if(str.length() > 0 && str.contains(" "))
                {
                    password.setError("Space is not allowed");
                    password.setText(s.toString().trim());
                }

            }


            @Override
            public void afterTextChanged(Editable s) {

            }
        });
    }

    public void setForgotPassword() {
        forgotPassword.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(LoginActivity.this,ForgotPasswordActivity.class);
                startActivity(intent);
            }
        });
        password.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {

            }

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {

                String str = s.toString();
                if(str.length() > 0 && str.contains(" "))
                {
                    password.setError("Space is not allowed");
                    password.setText(s.toString().trim());
                }


            }

            @Override
            public void afterTextChanged(Editable s) {

            }
        });

    }

    private void SetValidations() {
        if(!utils.CheckInternet(getApplicationContext())){
            Toast.makeText(this, "Check your internet connection!", Toast.LENGTH_SHORT).show();
            return;
        }
        //Email Validations
        if(email.getText().toString().isEmpty()) {
            email.setError("enter email address");
            email.requestFocus();
            return;
        }

        if(password.getText().toString().isEmpty()) {
            password.setError("enter password");
            password.requestFocus();
            return;
        }

        hitLoginApi();

    }

    private void hitLoginApi() {
        pd.show();
        Call<SignUp> login;

        ApiInterface apiInterface = ApiClient.getClient().create(ApiInterface.class);
        login = apiInterface.dologin(email.getText().toString().trim(),
                password.getText().toString()
        );
        login.enqueue(new Callback<SignUp>() {
            @Override
            public void onResponse(Call<SignUp> call, Response<SignUp> response) {
                if (response.isSuccessful()) {
                    pd.dismiss();
                    SignUp model = response.body();
                    Toast.makeText(LoginActivity.this, ""+model.getMessage(), Toast.LENGTH_SHORT).show();

                    if(model != null) {

                        if (model.getData() != null) {
                            pd.dismiss();
                            if(model.getData().getUser().getStatus().equals("1")){
                                Toast.makeText(LoginActivity.this, "Banned by admin", Toast.LENGTH_SHORT).show();
                               return;
                            }
                            Toast.makeText(LoginActivity.this, ""+model.getMessage(), Toast.LENGTH_SHORT).show();
                            AppContext.hitUpdateTokenApi(model.getData().getToken(), FirebaseInstanceId.getInstance().getToken());
//                            Toast.makeText(LoginActivity.this, "Successful", Toast.LENGTH_SHORT).show();
                            sharedPrefrencesMain.setIsLogin(true);
                            sharedPrefrencesMain.setToken(model.getData().getToken());

                            Intent intent = new Intent(LoginActivity.this, MainActivity.class);
                            startActivity(intent);
                            if(LoginAndSigUpActivity.loginAndSigUpActivity!=null)
                                LoginAndSigUpActivity.loginAndSigUpActivity.finish();
                            finish();
                        }
                    }
                }
            }

            @Override
            public void onFailure(Call<SignUp> call, Throwable t) {
                pd.dismiss();

                Toast.makeText(LoginActivity.this, ""+t.getMessage(), Toast.LENGTH_LONG).show();

            }
        });
    }

    public void hideKeyboard(View view) {
        InputMethodManager inputMethodManager =(InputMethodManager)getSystemService(
                Activity.INPUT_METHOD_SERVICE);
        inputMethodManager.hideSoftInputFromWindow(view.getWindowToken(), 0);
    }

    @Override
    public void onBackPressed() {
        super.onBackPressed();
        finish();
    }
}