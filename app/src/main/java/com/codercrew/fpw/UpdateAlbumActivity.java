package com.codercrew.fpw;

import android.Manifest;
import android.app.AlertDialog;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.database.Cursor;
import android.graphics.Bitmap;
import android.media.MediaScannerConnection;
import android.net.Uri;
import android.os.AsyncTask;
import android.os.Build;
import android.os.Bundle;
import android.os.Environment;
import android.provider.MediaStore;
import android.util.Log;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.appcompat.app.AppCompatActivity;
import androidx.appcompat.widget.AppCompatSpinner;
import androidx.core.app.ActivityCompat;
import androidx.core.content.FileProvider;
import androidx.lifecycle.Observer;
import androidx.lifecycle.ViewModelProvider;

import com.codercrew.fpw.ApiClient.ApiClient;
import com.codercrew.fpw.ApiClient.ApiInterface;
import com.codercrew.fpw.Utils.ProgressRequestBody;
import com.codercrew.fpw.Utils.SharedPrefrencesMain;
import com.codercrew.fpw.home.ui.media.MediaFragment;
import com.codercrew.fpw.model.GetAlbumPhotos;
import com.codercrew.fpw.model.GetAlbums;
import com.codercrew.fpw.viewmodel.AddAlbumViewModel;
import com.github.dhaval2404.imagepicker.ImagePicker;
import com.yalantis.ucrop.UCrop;

import java.io.ByteArrayOutputStream;
import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.net.URISyntaxException;
import java.util.ArrayList;
import java.util.Calendar;

import okhttp3.MediaType;
import okhttp3.MultipartBody;
import okhttp3.OkHttpClient;
import okhttp3.Request;
import okhttp3.RequestBody;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

import static android.os.Environment.getExternalStoragePublicDirectory;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

public class UpdateAlbumActivity extends AppCompatActivity implements View.OnClickListener, ProgressRequestBody.UploadCallbacks {

    AddAlbumViewModel addAlbumViewModel;
    ProgressDialog pd;
    SharedPrefrencesMain sharedPrefrencesMain;
    ArrayList<String> arrayList = new ArrayList<>();
    String url,type;
    private static final int PICK_IMAGE = 100,CAMERA=2,PICK_VIDEO=200,CAMERAVIDEO=300;
    private static final String IMAGE_DIRECTORY = "/encoded_image";
    AppCompatSpinner appCompatSpinner;
    String filePAth="";
    EditText editText,editTextCaption;
    TextView textViewPath,tvAlbumName,textViewTitle;
    String albumId;
    ArrayList<GetAlbums.Data> dataArrayList = new ArrayList<>();
    Uri cameraUri;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_add_album);

        sharedPrefrencesMain = new SharedPrefrencesMain(this);
        type = getIntent().getStringExtra("type");
        albumId = getIntent().getStringExtra("id");
        pd =new ProgressDialog(this);
        pd.setCancelable(false);
        pd.setMessage(getString(R.string.loading));
        pd.show();
        editTextCaption = findViewById(R.id.etCaption);
        textViewTitle = findViewById(R.id.tvTitle);
        appCompatSpinner = findViewById(R.id.spinner);
        editText = findViewById(R.id.etAlbumName);
        final ImageView imageView = findViewById(R.id.imgBack);
        textViewPath = findViewById(R.id.tvImgPAth);
        tvAlbumName = findViewById(R.id.tvAlbumName);
        final TextView textView = findViewById(R.id.tvAddImg);
        final ImageView imageViewSave = findViewById(R.id.imgSave);
        imageViewSave.setOnClickListener(this);
        textView.setOnClickListener(this);
        imageView.setOnClickListener(this);
        editText.setVisibility(View.GONE);
        tvAlbumName.setVisibility(View.GONE);

        if(type.equals("1"))
            textView.setText("Add Image");
        else textView.setText("Add Video");
        textViewTitle.setText("Update Album");
        addAlbumViewModel =
                new ViewModelProvider(this).get(AddAlbumViewModel.class);

        addAlbumViewModel.getText().observe(this, new Observer<ArrayList<GetAlbums.Data>>() {
            @Override
            public void onChanged(@Nullable ArrayList<GetAlbums.Data> familyMembersModel) {
                pd.dismiss();
                if(familyMembersModel!=null) {
                    dataArrayList = familyMembersModel;
                    for(GetAlbums.Data data : familyMembersModel){
                        if(data.getId().equals(albumId))
                            arrayList.add(data.getAlbumname());
                    }
//                    arrayList.add(0,"Create New Album");
                    ArrayAdapter<String> spinnerArrayAdapter = new ArrayAdapter<String>
                            (UpdateAlbumActivity.this, android.R.layout.simple_spinner_item,
                                    arrayList); //selected item will look like a spinner set from XML
                    spinnerArrayAdapter.setDropDownViewResource(android.R.layout
                            .simple_spinner_dropdown_item);
                    appCompatSpinner.setAdapter(spinnerArrayAdapter);
                }
            }
        });

        addAlbumViewModel.getAdd().observe(this, new Observer<ArrayList<GetAlbumPhotos.Data>>() {
            @Override
            public void onChanged(ArrayList<GetAlbumPhotos.Data> data) {
                pd.dismiss();
                finish();
            }
        });

        addAlbumViewModel.hitGetAlbumApi(getIntent().getStringExtra("type"));

        appCompatSpinner.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
                if(position==0){
                    editText.setFocusable(true);
                    editText.setFocusableInTouchMode(true);
                }else {
                    albumId = dataArrayList.get(position-1).getId();
                    editText.setFocusable(false);
                    editText.setFocusableInTouchMode(false);
                }
            }

            @Override
            public void onNothingSelected(AdapterView<?> parent) {

            }
        });

        appCompatSpinner.setEnabled(false);

    }

    private void openGallery() {
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
            if (checkSelfPermission(Manifest.permission.READ_EXTERNAL_STORAGE) == PackageManager.PERMISSION_GRANTED) {
                Intent gallery = new Intent(Intent.ACTION_PICK, MediaStore.Images.Media.INTERNAL_CONTENT_URI);
                gallery.setType("image/*");
                startActivityForResult(gallery, PICK_IMAGE);
            }
            else {
                ActivityCompat.requestPermissions( this,new String[]{Manifest.permission.READ_EXTERNAL_STORAGE,Manifest.permission.WRITE_EXTERNAL_STORAGE}, 5);
            }
        }else {
            Intent gallery = new Intent(Intent.ACTION_PICK, MediaStore.Images.Media.INTERNAL_CONTENT_URI);
            gallery.setType("image/*");
            startActivityForResult(gallery, PICK_IMAGE);
        }
    }


    private void showVideoDialog(){

        AlertDialog.Builder pictureDialog = new AlertDialog.Builder(UpdateAlbumActivity.this);
        pictureDialog.setTitle("Choose From");
        String[] pictureDialogItems = {
                "Gallery",
                "Camera"};
        pictureDialog.setItems(pictureDialogItems,
                new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {
                        switch (which) {
                            case 0:
                                openGalleryVideo();
                                break;
                            case 1:
                                takeVideoFromCamera();
                                break;
                        }
                    }
                });
        pictureDialog.show();

    }

    private void openGalleryVideo() {
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
            if (checkSelfPermission(Manifest.permission.READ_EXTERNAL_STORAGE) == PackageManager.PERMISSION_GRANTED) {
                Intent gallery = new Intent(Intent.ACTION_PICK, MediaStore.Images.Media.INTERNAL_CONTENT_URI);
                gallery.setType("video/*");
                startActivityForResult(gallery, PICK_VIDEO);
            }
            else {
                ActivityCompat.requestPermissions( this,new String[]{Manifest.permission.READ_EXTERNAL_STORAGE,Manifest.permission.WRITE_EXTERNAL_STORAGE}, 6);
            }
        }else {
            Intent gallery = new Intent(Intent.ACTION_PICK, MediaStore.Images.Media.INTERNAL_CONTENT_URI);
            gallery.setType("video/*");
            startActivityForResult(gallery, PICK_VIDEO);
        }
    }

    private void takePhotoFromCamera() {
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
            if (checkSelfPermission(Manifest.permission.CAMERA) == PackageManager.PERMISSION_GRANTED) {
                Intent intent = new Intent(android.provider.MediaStore.ACTION_IMAGE_CAPTURE);
                if (Build.VERSION.SDK_INT > Build.VERSION_CODES.Q) {
                    cameraUri = FileProvider.getUriForFile(getApplicationContext(),"com.codercrew.fpw.provider",new File(getExternalStoragePublicDirectory(Environment.DIRECTORY_DCIM), "pic_"+ String.valueOf(System.currentTimeMillis()) + ".jpg"));
                }else
                    cameraUri = FileProvider.getUriForFile(getApplicationContext(),"com.codercrew.fpw.provider",new File(Environment.getExternalStorageDirectory(), "pic_"+ String.valueOf(System.currentTimeMillis()) + ".jpg"));                intent.addFlags(Intent.FLAG_GRANT_READ_URI_PERMISSION);
                intent.putExtra(MediaStore.EXTRA_OUTPUT, cameraUri);
//                    intent.setType("image/*");
                startActivityForResult(intent, CAMERA);
            }
            else {
                ActivityCompat.requestPermissions(this,new String[]{Manifest.permission.CAMERA}, 4);
            }
        }else {

            Intent intent = new Intent(android.provider.MediaStore.ACTION_IMAGE_CAPTURE);
//                intent.setType("image/*");
            startActivityForResult(intent, CAMERA);
        }
    }
    private void takeVideoFromCamera() {
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
            if (checkSelfPermission(Manifest.permission.CAMERA) == PackageManager.PERMISSION_GRANTED) {
                Intent intent = new Intent(MediaStore.ACTION_VIDEO_CAPTURE);
//                    intent.setType("video/*");
                startActivityForResult(intent, CAMERAVIDEO);
            }
            else {
                ActivityCompat.requestPermissions(this,new String[]{Manifest.permission.CAMERA}, 7);
            }
        }else {
            Intent intent = new Intent(MediaStore.ACTION_VIDEO_CAPTURE);
//                intent.setType("video/*");
            startActivityForResult(intent, CAMERAVIDEO);
        }
    }
    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        if (resultCode == RESULT_CANCELED) {
            return;
        }
        if (requestCode == PICK_IMAGE) {
            if (data != null) {
                type = "1";
                UCrop.of(data.getData(), Uri.fromFile(new File(getCacheDir(), "IMG_" + System.currentTimeMillis())))
                        .start(UpdateAlbumActivity.this);
            }
        }else if (requestCode == PICK_VIDEO) {
            if (data != null) {
                type = "2";
                Uri contentURI = data.getData();
                String[] filePathColumn = {MediaStore.Images.Media.DATA};
                Cursor cursor = getContentResolver().query(contentURI, filePathColumn, null, null, null);
                cursor.moveToFirst();
                int columnIndex = cursor.getColumnIndex(filePathColumn[0]);
                String picturePath = cursor.getString(columnIndex);
                cursor.close();

                filePAth = picturePath;
                textViewPath.setText(picturePath);


            }
        } else if (requestCode == CAMERA) {
            type = "1";
            UCrop.of(cameraUri, Uri.fromFile(new File(getCacheDir(), "IMG_" + System.currentTimeMillis())))
                    .start(UpdateAlbumActivity.this);
        }else if(requestCode == CAMERAVIDEO){
            type = "2";
            Uri contentURI = data.getData();
            String[] filePathColumn = {MediaStore.Images.Media.DATA};
            Cursor cursor = getContentResolver().query(contentURI, filePathColumn, null, null, null);
            cursor.moveToFirst();
            int columnIndex = cursor.getColumnIndex(filePathColumn[0]);
            String picturePath = cursor.getString(columnIndex);
            cursor.close();

            filePAth = picturePath;
            textViewPath.setText(filePAth);
        }else if(requestCode==UCrop.REQUEST_CROP){
            Uri imgUri = UCrop.getOutput(data);
            if (imgUri != null) {
                String selectedImage = imgUri.getPath();
                try {
                    Bitmap bitmap = MediaStore.Images.Media.getBitmap(getContentResolver(), imgUri);
                    String path = saveImage(bitmap);
                } catch (IOException e) {
                    e.printStackTrace();
                    Toast.makeText(getApplicationContext(), "Failed!", Toast.LENGTH_SHORT).show();
                }
                // load selectedImage into ImageView
            }
        }
    }

    @Override
    public void onRequestPermissionsResult(int requestCode, @NonNull String[] permissions, @NonNull int[] grantResults) {
        super.onRequestPermissionsResult(requestCode, permissions, grantResults);
        if (grantResults.length > 0 && grantResults[0] == PackageManager.PERMISSION_GRANTED && requestCode == 4) {
            takePhotoFromCamera();
        }
        else if (grantResults.length > 0 && grantResults[0] == PackageManager.PERMISSION_GRANTED && requestCode == 5) {
            openGallery();
        }else if (grantResults.length > 0 && grantResults[0] == PackageManager.PERMISSION_GRANTED && requestCode == 6) {
            openGalleryVideo();
        }else if (grantResults.length > 0 && grantResults[0] == PackageManager.PERMISSION_GRANTED && requestCode == 7) {
            takeVideoFromCamera();
        }
    }

    public String saveImage(Bitmap myBitmap) {
        ByteArrayOutputStream bytes = new ByteArrayOutputStream();
        myBitmap.compress(Bitmap.CompressFormat.JPEG, 90, bytes);
        File wallpaperDirectory;
        if (Build.VERSION.SDK_INT > Build.VERSION_CODES.Q) {
            wallpaperDirectory = new File(
                    getExternalStoragePublicDirectory(Environment.DIRECTORY_DCIM) + IMAGE_DIRECTORY);
        }else {
            wallpaperDirectory = new File(
                    Environment.getExternalStorageDirectory() + IMAGE_DIRECTORY);
        }
        // have the object build the directory structure, if needed.
        if (!wallpaperDirectory.exists()) {
            wallpaperDirectory.mkdirs();
        }
        try {
            File f = new File(wallpaperDirectory, Calendar.getInstance()
                    .getTimeInMillis() + ".jpg");
            if (!f.getParentFile().exists())
                f.getParentFile().mkdirs();
            if (!f.exists())
                f.createNewFile();
            FileOutputStream fo = new FileOutputStream(f);
            fo.write(bytes.toByteArray());

            MediaScannerConnection.scanFile(this,
                    new String[]{f.getPath()},
                    new String[]{"image/jpeg"}, null);

            fo.close();
            filePAth = f.getAbsolutePath();
            textViewPath.setText(filePAth);
            return f.getAbsolutePath();
        } catch (IOException e1) {
            e1.printStackTrace();
        }
        return "";
    }

    @Override
    public void onClick(View v) {
        if(v.getId()==R.id.imgSave){
            String caption="";
            if(editTextCaption.getText().toString()!=null && editTextCaption.getText().toString().length()>0)
                caption = editTextCaption.getText().toString();

                if(filePAth.isEmpty()){
                    if(type.equals("0"))
                        Toast.makeText(this, "Choose any image from gallery to upload!", Toast.LENGTH_SHORT).show();
                    else Toast.makeText(this, "Choose any video from gallery to upload!", Toast.LENGTH_SHORT).show();
                    return;
                }
                if(type.equals("1"))
                    CheckImage(filePAth,caption,albumId,type);
                else
                    CheckVideo(filePAth,caption,albumId,type);
//                hitAddUpdateAlbumApi(type,albumId,filePAth,caption);

        }else if(v.getId()==R.id.tvAddImg){
            if(type.equals("1")){
                showPictureDialog();
            }else showVideoDialog();
        }else {
            finish();
        }
    }

    private void showPictureDialog(){

        AlertDialog.Builder pictureDialog = new AlertDialog.Builder(UpdateAlbumActivity.this);
        pictureDialog.setTitle("Choose From");
        String[] pictureDialogItems = {
                "Gallery",
                "Camera"};
        pictureDialog.setItems(pictureDialogItems,
                new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {
                        switch (which) {
                            case 0:
                                openGallery();
                                break;
                            case 1:
                                takePhotoFromCamera();
                                break;
                        }
                    }
                });
        pictureDialog.show();

    }

    @Override
    public void onProgressUpdate(int percentage) {
        pd.setMessage(percentage+"% Uploading...");
        pd.show();
    }

    @Override
    public void onError() {

    }

    @Override
    public void onFinish() {

    }

    public void hitAddUpdateAlbumApi(String type,String albumname,String path,String cap) {
        File file = new File(path);
        MultipartBody.Part part;
        RequestBody requestBodyId = RequestBody.create(MediaType.parse("application/octet-stream"),albumname);
        RequestBody requestBodyType = RequestBody.create(MediaType.parse("application/octet-stream"),type);
        RequestBody requestBodyCap = RequestBody.create(MediaType.parse("application/octet-stream"),cap);
        ProgressRequestBody fileBody;
        if(type.equals("1")) {
            fileBody = new ProgressRequestBody(file,"image", this);
            part = MultipartBody.Part.createFormData("image", file.getName(), fileBody);
        }
        else {
            fileBody = new ProgressRequestBody(file,"video", this);
            part = MultipartBody.Part.createFormData("video",file.getName(),fileBody);
        }
        Call<GetAlbumPhotos> verify;

        ApiInterface apiInterface = ApiClient.getClient().create(ApiInterface.class);
        verify = apiInterface.updateAlbum("Bearer "+sharedPrefrencesMain.getToken(),requestBodyId,requestBodyType,requestBodyCap,part);

        verify.enqueue(new Callback<GetAlbumPhotos>() {
            @Override
            public void onResponse(Call<GetAlbumPhotos> call, Response<GetAlbumPhotos> response) {
                if (response.isSuccessful()) {
                    file.delete();
                    MediaScannerConnection.scanFile(UpdateAlbumActivity.this, new String[]{Environment.getExternalStorageDirectory().toString()}, null, new MediaScannerConnection.OnScanCompletedListener() {
                        /*
                         *   (non-Javadoc)
                         * @see android.media.MediaScannerConnection.OnScanCompletedListener#onScanCompleted(java.lang.String, android.net.Uri)
                         */
                        public void onScanCompleted(String path, Uri uri) {
                            Log.e("ExternalStorage", "Scanned " + path + ":");
                            Log.e("ExternalStorage", "-> uri=" + uri);
                        }
                    });
                    MediaFragment.ISMEDIAUPDATED =true;
                    finish();
                }else {
                    Toast.makeText(UpdateAlbumActivity.this, ""+response.errorBody().toString(), Toast.LENGTH_SHORT).show();
                }
            }
            @Override
            public void onFailure(Call<GetAlbumPhotos> call, Throwable t) {
                pd.dismiss();
                Toast.makeText(AppContext.getAppContext(), "fail", Toast.LENGTH_LONG).show();
            }
        });
    }

    private void CheckImage(String path,String cap,String albumname,String type){
        File file = new File(path);
        ProgressDialog progressDialog = new ProgressDialog(UpdateAlbumActivity.this);
        progressDialog.setMessage("Checking Image Validity....");
        progressDialog.show();
        new Thread(new Runnable() {
            @Override
            public void run() {
                OkHttpClient client = new OkHttpClient().newBuilder()
                        .build();
                MediaType mediaType = MediaType.parse("text/plain");
                RequestBody body = new MultipartBody.Builder().setType(MultipartBody.FORM)
                        .addFormDataPart("models","nudity")
                        .addFormDataPart("media",file.getName(),
                                RequestBody.create(MediaType.parse("application/octet-stream"),
                                        file))
                        .addFormDataPart("api_user",getString(R.string.api_key))
                        .addFormDataPart("api_secret",getString(R.string.api_secret))
                        .build();
                Request request = new Request.Builder()
                        .url("https://api.sightengine.com/1.0/check.json")
                        .method("POST", body)
                        .build();
                try {
                    okhttp3.Response response = client.newCall(request).execute();
                    Double raw,safe,partial;
                    try {
                        JSONObject jsonObject = new JSONObject(response.body().string());
                        JSONObject jsonObjectNudity = jsonObject.getJSONObject("nudity");
                        raw = jsonObjectNudity.getDouble("raw");
                        safe = jsonObjectNudity.getDouble("safe");
                        partial = jsonObjectNudity.getDouble("partial");
                        runOnUiThread(new Runnable() {
                            @Override
                            public void run() {
                                progressDialog.dismiss();
                            }
                        });
                        if(raw>=getMax(safe,partial)){
                            Toast.makeText(getApplicationContext(), "Selected image is against our privacy policies", Toast.LENGTH_SHORT).show();
                        }else {
                            runOnUiThread(new Runnable() {
                                @Override
                                public void run() {
                                    if(cap.length()>0){
                                        CheckText(path,cap,albumname,type);
                                    }else
                                        hitAddUpdateAlbumApi(type,albumname,path,cap);
                                }
                            });

                        }
                    } catch (NullPointerException | JSONException e) {
                        progressDialog.dismiss();
                        e.printStackTrace();
                    }
                } catch (IOException e) {
                    runOnUiThread(new Runnable() {
                        @Override
                        public void run() {
                            progressDialog.dismiss();
                        }
                    });
                    e.printStackTrace();
                }
            }
        }).start();
    }

    private void CheckVideo(String path,String cap,String albumname,String type){
        File file = new File(path);
        ProgressDialog progressDialog = new ProgressDialog(UpdateAlbumActivity.this);
        progressDialog.setMessage("Checking Video Validity....");
        progressDialog.show();
        new Thread(new Runnable() {
            @Override
            public void run() {
                OkHttpClient client = new OkHttpClient().newBuilder()
                        .build();
                MediaType mediaType = MediaType.parse("text/plain");
                RequestBody body = new MultipartBody.Builder().setType(MultipartBody.FORM)
                        .addFormDataPart("models","nudity")
                        .addFormDataPart("media",file.getName(),
                                RequestBody.create(MediaType.parse("application/octet-stream"),
                                        file))
                        .addFormDataPart("api_user",getString(R.string.api_key))
                        .addFormDataPart("api_secret",getString(R.string.api_secret))
                        .build();
                Request request = new Request.Builder()
                        .url("https://api.sightengine.com/1.0/video/check-sync.json")
                        .method("POST", body)
                        .build();
                try {
                    okhttp3.Response response = client.newCall(request).execute();
                    Double raw,safe,partial;
                    boolean isValid = true;
                    try {
                        JSONObject jsonObject = new JSONObject(response.body().string());
                        JSONObject jsonObjectDate = jsonObject.getJSONObject("data");
                        JSONArray jsonArrayFrames = jsonObjectDate.getJSONArray("frames");
                        runOnUiThread(new Runnable() {
                            @Override
                            public void run() {
                                progressDialog.dismiss();
                            }
                        });

                        for(int i = 0; i< jsonArrayFrames.length();i++){
                            JSONObject jsonObjectNudity = jsonArrayFrames.getJSONObject(i).getJSONObject("nudity");
                            raw = jsonObjectNudity.getDouble("raw");
                            safe = jsonObjectNudity.getDouble("safe");
                            partial = jsonObjectNudity.getDouble("partial");
                            if(raw>=getMax(safe,partial)){
                                isValid = false;
                            }
                            if(isValid && i==jsonArrayFrames.length()-1){
                                runOnUiThread(new Runnable() {
                                    @Override
                                    public void run() {
                                        if(cap.length()>0){
                                            CheckText(path,cap,albumname,type);
                                        }else
                                            hitAddUpdateAlbumApi(type,albumname,path,cap);
                                    }
                                });
                            }else if(!isValid && i==jsonArrayFrames.length()-1) {
                                Toast.makeText(getApplicationContext(), "Selected video is against our privacy policies", Toast.LENGTH_SHORT).show();
                            }
                        }
                    } catch (NullPointerException | JSONException e) {
                        runOnUiThread(new Runnable() {
                            @Override
                            public void run() {
                                progressDialog.dismiss();
                            }
                        });
                        e.printStackTrace();
                    }
                } catch (IOException e) {
                    runOnUiThread(new Runnable() {
                        @Override
                        public void run() {
                            progressDialog.dismiss();
                        }
                    });
                    e.printStackTrace();
                }
            }
        }).start();


    }

    private Double getMax(double safe,double partial){
        if(safe>partial)
            return safe;
        else return partial;
    }

    private void CheckText(String path,String cap,String albumname,String type){
        ProgressDialog progressDialog = new ProgressDialog(UpdateAlbumActivity.this);
        progressDialog.setMessage("Checking Caption Validity....");
        progressDialog.show();
        new Thread(new Runnable() {
            @Override
            public void run() {
                OkHttpClient client = new OkHttpClient().newBuilder()
                        .build();
                MediaType mediaType = MediaType.parse("text/plain");
                RequestBody body = new MultipartBody.Builder().setType(MultipartBody.FORM)
                        .addFormDataPart("models","text")
                        .addFormDataPart("text",cap)
                        .addFormDataPart("api_user",getString(R.string.api_key))
                        .addFormDataPart("api_secret",getString(R.string.api_secret))
                        .addFormDataPart("lang","en")
                        .addFormDataPart("mode","standard")
                        .build();
                Request request = new Request.Builder()
                        .url("https://api.sightengine.com/1.0/text/check.json")
                        .method("POST", body)
                        .build();
                try {
                    okhttp3.Response response = client.newCall(request).execute();
                    try {
                        JSONObject jsonObject = new JSONObject(response.body().string());
                        JSONObject jsonObjectNudity = jsonObject.getJSONObject("profanity");

                        if(jsonObjectNudity.getJSONArray("matches").length()>0){
                            runOnUiThread(new Runnable() {
                                @Override
                                public void run() {
                                    progressDialog.dismiss();
                                    Toast.makeText(getApplicationContext(), "Caption is against our privacy policies.", Toast.LENGTH_SHORT).show();
                                }
                            });
                        }else {
                            runOnUiThread(new Runnable() {
                                @Override
                                public void run() {
                                        hitAddUpdateAlbumApi(type,albumname,path,cap);
                                }
                            });

                        }

                    } catch (NullPointerException | JSONException e) {
                        runOnUiThread(new Runnable() {
                            @Override
                            public void run() {
                                progressDialog.dismiss();
                            }
                        });
                        e.printStackTrace();
                    }
                } catch (IOException e) {
                    runOnUiThread(new Runnable() {
                        @Override
                        public void run() {
                            progressDialog.dismiss();
                        }
                    });
                    e.printStackTrace();
                }
            }
        }).start();
    }
}