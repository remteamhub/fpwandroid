package com.codercrew.fpw.viewmodel;

import android.widget.Toast;

import androidx.lifecycle.LiveData;
import androidx.lifecycle.MutableLiveData;
import androidx.lifecycle.ViewModel;

import com.codercrew.fpw.ApiClient.ApiClient;
import com.codercrew.fpw.ApiClient.ApiInterface;
import com.codercrew.fpw.AppContext;
import com.codercrew.fpw.Utils.SharedPrefrencesMain;
import com.codercrew.fpw.home.model.FamilyMembersModel;
import com.codercrew.fpw.model.GetComments;
import com.codercrew.fpw.model.GetUsers;

import java.util.ArrayList;
import java.util.List;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class BlockedMemberViewModel extends ViewModel {

    private MutableLiveData<GetUsers> mText;
    SharedPrefrencesMain sharedPrefrencesMain;

    public BlockedMemberViewModel() {
        sharedPrefrencesMain = SharedPrefrencesMain.getInstance(AppContext.getAppContext());
        mText = new MutableLiveData<>();
//        mText.setValue(SetListValues());
    }

    public LiveData<GetUsers> getText() {
        return mText;
    }

    private List<FamilyMembersModel> SetListValues(){
        List<FamilyMembersModel> familyMembersModels = new ArrayList<>();
        familyMembersModels.add(new FamilyMembersModel(1,"","",""));
        familyMembersModels.add(new FamilyMembersModel(2,"","",""));
        familyMembersModels.add(new FamilyMembersModel(3,"","",""));
        familyMembersModels.add(new FamilyMembersModel(4,"","",""));
        familyMembersModels.add(new FamilyMembersModel(5,"","",""));
        return familyMembersModels;
    }

    public void hitGetBlockApi(String pid)
    {
        Call<GetUsers> verify;

        ApiInterface apiInterface = ApiClient.getClient().create(ApiInterface.class);
        verify = apiInterface.getblockers("Bearer "+sharedPrefrencesMain.getToken(),pid);

        verify.enqueue(new Callback<GetUsers>() {
            @Override
            public void onResponse(Call<GetUsers> call, Response<GetUsers> response) {
                if (response.isSuccessful()) {
                    mText.setValue(response.body());
//                    Toast.makeText(AppContext.getAppContext(), response.body().getMessage(), Toast.LENGTH_LONG).show();

                }
            }
            @Override
            public void onFailure(Call<GetUsers> call, Throwable t) {
                mText.setValue(null);
                Toast.makeText(AppContext.getAppContext(), "fail", Toast.LENGTH_LONG).show();
            }
        });
    }

    public void hitDecesed(String pid)
    {
        Call<GetUsers> verify;
        ApiInterface apiInterface = ApiClient.getClient().create(ApiInterface.class);
        verify = apiInterface.getdeceased("Bearer "+sharedPrefrencesMain.getToken(),pid);

        verify.enqueue(new Callback<GetUsers>() {
            @Override
            public void onResponse(Call<GetUsers> call, Response<GetUsers> response) {
                if (response.isSuccessful()) {
                    mText.setValue(response.body());
//                    Toast.makeText(AppContext.getAppContext(), response.body().getMessage(), Toast.LENGTH_LONG).show();
                }
            }
            @Override
            public void onFailure(Call<GetUsers> call, Throwable t) {
                mText.setValue(null);
                Toast.makeText(AppContext.getAppContext(), "fail", Toast.LENGTH_LONG).show();
            }
        });
    }

    public void hitUnblock(String pid)
    {
        Call<GetUsers> verify;
        ApiInterface apiInterface = ApiClient.getClient().create(ApiInterface.class);
        verify = apiInterface.unblock("Bearer "+sharedPrefrencesMain.getToken(),pid);

        verify.enqueue(new Callback<GetUsers>() {
            @Override
            public void onResponse(Call<GetUsers> call, Response<GetUsers> response) {
                if (response.isSuccessful()) {
                    hitGetBlockApi("");
//                    Toast.makeText(AppContext.getAppContext(), response.body().getMessage(), Toast.LENGTH_LONG).show();
                }
            }
            @Override
            public void onFailure(Call<GetUsers> call, Throwable t) {
                mText.setValue(null);
                Toast.makeText(AppContext.getAppContext(), "fail", Toast.LENGTH_LONG).show();
            }
        });
    }
}