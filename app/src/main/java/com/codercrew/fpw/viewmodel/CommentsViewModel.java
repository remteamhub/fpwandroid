package com.codercrew.fpw.viewmodel;

import android.widget.Toast;

import androidx.lifecycle.LiveData;
import androidx.lifecycle.MutableLiveData;
import androidx.lifecycle.ViewModel;

import com.codercrew.fpw.ApiClient.ApiClient;
import com.codercrew.fpw.ApiClient.ApiInterface;
import com.codercrew.fpw.AppContext;
import com.codercrew.fpw.Utils.SharedPrefrencesMain;
import com.codercrew.fpw.model.GetComments;
import com.codercrew.fpw.model.GetPost;

import java.util.ArrayList;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

import static com.codercrew.fpw.home.ui.walls.WallsFragment.ISUPDATED;

public class CommentsViewModel extends ViewModel {

    private MutableLiveData<ArrayList<GetComments.CommentsDetails>> mText;
    private SharedPrefrencesMain sharedPrefrencesMain;

    public CommentsViewModel() {
        mText = new MutableLiveData<>();
//        mText.setValue(SetListValues());
        sharedPrefrencesMain = new SharedPrefrencesMain(AppContext.getAppContext());
    }

    public LiveData<ArrayList<GetComments.CommentsDetails>> getText() {
        return mText;
    }

    public void hitGetCommentApi(String pid)
    {
        Call<GetComments> verify;

        ApiInterface apiInterface = ApiClient.getClient().create(ApiInterface.class);
        verify = apiInterface.getcomments("Bearer "+sharedPrefrencesMain.getToken(),pid);

        verify.enqueue(new Callback<GetComments>() {
            @Override
            public void onResponse(Call<GetComments> call, Response<GetComments> response) {
                if (response.isSuccessful()) {
                    mText.setValue(response.body().getData());
//                    Toast.makeText(AppContext.getAppContext(), response.body().getMessage(), Toast.LENGTH_LONG).show();

                }
            }
            @Override
            public void onFailure(Call<GetComments> call, Throwable t) {
                mText.setValue(null);
                Toast.makeText(AppContext.getAppContext(), "fail", Toast.LENGTH_LONG).show();
            }
        });
    }

    public void hitDeleteCommentApi(String pid,String cid)
    {
        Call<GetComments> verify;

        ApiInterface apiInterface = ApiClient.getClient().create(ApiInterface.class);
        verify = apiInterface.deletecomment("Bearer "+sharedPrefrencesMain.getToken(),cid);

        verify.enqueue(new Callback<GetComments>() {
            @Override
            public void onResponse(Call<GetComments> call, Response<GetComments> response) {
                if (response.isSuccessful()) {
                    hitGetCommentApi(pid);
                    Toast.makeText(AppContext.getAppContext(), response.body().getMessage(), Toast.LENGTH_LONG).show();

                }
            }
            @Override
            public void onFailure(Call<GetComments> call, Throwable t) {
                mText.setValue(null);
                Toast.makeText(AppContext.getAppContext(), "fail", Toast.LENGTH_LONG).show();
            }
        });
    }

    public void hitCommentApi(String postId,String comment)
    {
        Call<GetPost> verify;

        ApiInterface apiInterface = ApiClient.getClient().create(ApiInterface.class);
        verify = apiInterface.addComment("Bearer "+sharedPrefrencesMain.getToken(), comment,postId);

        verify.enqueue(new Callback<GetPost>() {
            @Override
            public void onResponse(Call<GetPost> call, Response<GetPost> response) {
                if (response.isSuccessful()) {
                    ISUPDATED = true;
                    hitGetCommentApi(postId);
//                    Toast.makeText(AppContext.getAppContext(), response.body().getMessage(), Toast.LENGTH_LONG).show();

                }
            }
            @Override
            public void onFailure(Call<GetPost> call, Throwable t) {
                Toast.makeText(AppContext.getAppContext(), "fail", Toast.LENGTH_LONG).show();
            }
        });
    }

}