package com.codercrew.fpw.viewmodel;

import androidx.lifecycle.LiveData;
import androidx.lifecycle.MutableLiveData;
import androidx.lifecycle.ViewModel;

import com.codercrew.fpw.home.model.FamilyMembersModel;

import java.util.ArrayList;
import java.util.List;

public class PendingMembersViewModel extends ViewModel {

    private MutableLiveData<List<FamilyMembersModel>> mText;

    public PendingMembersViewModel() {
        mText = new MutableLiveData<>();
        mText.setValue(SetListValues());
    }

    public LiveData<List<FamilyMembersModel>> getText() {
        return mText;
    }

    private List<FamilyMembersModel> SetListValues(){
        List<FamilyMembersModel> familyMembersModels = new ArrayList<>();
        familyMembersModels.add(new FamilyMembersModel(1,"","",""));
        familyMembersModels.add(new FamilyMembersModel(2,"","",""));
        familyMembersModels.add(new FamilyMembersModel(3,"","",""));
        familyMembersModels.add(new FamilyMembersModel(4,"","",""));
        familyMembersModels.add(new FamilyMembersModel(5,"","",""));
        return familyMembersModels;
    }
}