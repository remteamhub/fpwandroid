package com.codercrew.fpw;

import androidx.appcompat.app.AppCompatActivity;
import androidx.constraintlayout.widget.ConstraintLayout;

import android.app.Activity;
import android.app.ProgressDialog;
import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.view.inputmethod.InputMethodManager;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;


import com.codercrew.fpw.ApiClient.ApiClient;
import com.codercrew.fpw.ApiClient.ApiInterface;
import com.codercrew.fpw.Utils.SharedPrefrencesMain;
import com.codercrew.fpw.Utils.utils;
import com.codercrew.fpw.home.MainActivity;
import com.codercrew.fpw.model.Resend;
import com.codercrew.fpw.model.SignUp;
import com.google.firebase.iid.FirebaseInstanceId;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class EmailVerificationActivity extends AppCompatActivity {
    ConstraintLayout constraintLayout;
    Button verify;
    EditText code;
    SharedPrefrencesMain sharedPrefrencesMain;
    TextView resendBtnto;
    ProgressDialog pd;
    String email="";
    TextView textViewEmail;



    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_email_verification);
        constraintLayout = findViewById(R.id.constraintEmailVerification);
        textViewEmail = findViewById(R.id.tvEmail);
        verify = findViewById(R.id.verify);
        code = findViewById(R.id.digitsEntered);
        sharedPrefrencesMain = SharedPrefrencesMain.getInstance(this);
        pd =new ProgressDialog(EmailVerificationActivity.this);
        pd.setMessage(getString(R.string.loading));

        email = getIntent().getStringExtra("email");
        textViewEmail.setText(email);

        resendBtnto=findViewById(R.id.resendBtn);

//        Toast.makeText(this, "token"+sharedPrefrencesMain.getToken(), Toast.LENGTH_SHORT).show();

        constraintLayout.setOnFocusChangeListener(new View.OnFocusChangeListener() {
            @Override
            public void onFocusChange(View v, boolean hasFocus) {
                if (hasFocus) {
                    hideKeyboard(v);
                }
            }
        });
        verify.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
//                Toast.makeText(EmailVerificationActivity.this, "clicked", Toast.LENGTH_SHORT).show();
                if(!utils.CheckInternet(getApplicationContext())){
                    Toast.makeText(getApplicationContext(), "Check your internet connection!", Toast.LENGTH_SHORT).show();
                }else
                    hitApi();
            }
        });
        resendBtnto.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if(!utils.CheckInternet(getApplicationContext())){
                    Toast.makeText(getApplicationContext(), "Check your internet connection!", Toast.LENGTH_SHORT).show();
                }else
                    hitApiResend();
            }
        });
    }
    public void hideKeyboard(View view) {
        InputMethodManager inputMethodManager =(InputMethodManager)getSystemService(
                Activity.INPUT_METHOD_SERVICE);
        inputMethodManager.hideSoftInputFromWindow(view.getWindowToken(), 0);
    }
    @Override
    public void onBackPressed() {
        super.onBackPressed();
        finish();
    }

    public void hitApi()
    {
        pd.show();

        Call<SignUp> verify;

        ApiInterface apiInterface = ApiClient.getClient().create(ApiInterface.class);
//        verify = apiInterface.doverify(code.getText().toString().trim(),"Bearer"+sharedPrefrencesMain.getToken());
        verify = apiInterface.doverify("Bearer "+sharedPrefrencesMain.getToken(),code.getText().toString().trim());

        verify.enqueue(new Callback<SignUp>() {
            @Override
            public void onResponse(Call<SignUp> call, Response<SignUp> response) {
                if (response.isSuccessful()) {
                    pd.dismiss();

                    SignUp model = response.body();
                    if(model != null) {
                        Toast.makeText(EmailVerificationActivity.this, ""+model.getMessage(), Toast.LENGTH_SHORT).show();

                        if (model.getData() != null) {
//                            Toast.makeText(EmailVerificationActivity.this, ""+response.message(), Toast.LENGTH_SHORT).show();
                            sharedPrefrencesMain.setIsLogin(true);
                            AppContext.hitUpdateTokenApi(sharedPrefrencesMain.getToken(), FirebaseInstanceId.getInstance().getToken());
                            Intent intent = new Intent(EmailVerificationActivity.this, MainActivity.class);
                            startActivity(intent);
                            if(LoginAndSigUpActivity.loginAndSigUpActivity!=null)
                                LoginAndSigUpActivity.loginAndSigUpActivity.finish();
                            if(SignUpActivity.signUpActivity!=null)
                                SignUpActivity.signUpActivity.finish();
                            finish();
                        }
                    }
                }
            }

            @Override
            public void onFailure(Call<SignUp> call, Throwable t) {
                pd.dismiss();

                Toast.makeText(EmailVerificationActivity.this, "fail", Toast.LENGTH_LONG).show();
            }
        });
    }

    public void hitApiResend()
    {
        pd.show();

        Call<Resend> resendCall;

        ApiInterface apiInterface = ApiClient.getClient().create(ApiInterface.class);
        resendCall = apiInterface.resend("Bearer "+sharedPrefrencesMain.getToken());

        resendCall.enqueue(new Callback<Resend>() {
            @Override
            public void onResponse(Call<Resend> call, Response<Resend> response) {
                if (response.isSuccessful()) {
                    pd.dismiss();

                    Toast.makeText(EmailVerificationActivity.this, ""+response.body().getMessage(), Toast.LENGTH_SHORT).show();

                    Resend model = response.body();

                    if(model != null) {
                        pd.dismiss();

                        if (model.getData() != null) {
                            Toast.makeText(EmailVerificationActivity.this, ""+response.message(), Toast.LENGTH_SHORT).show();
                        }
                    }
                    if(model.getData() == null)
                    {
                        Toast.makeText(EmailVerificationActivity.this, "Unauthorized Key", Toast.LENGTH_SHORT).show();

                    }
                }
            }

            @Override
            public void onFailure(Call<Resend> call, Throwable t) {
                Toast.makeText(EmailVerificationActivity.this, ""+t.getMessage(), Toast.LENGTH_LONG).show();
            }
        });
    }
}