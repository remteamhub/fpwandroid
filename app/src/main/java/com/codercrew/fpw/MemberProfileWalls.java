package com.codercrew.fpw;

import android.app.ProgressDialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.os.Bundle;
import android.view.MenuItem;
import android.view.View;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import androidx.annotation.Nullable;
import androidx.appcompat.app.AppCompatActivity;
import androidx.appcompat.widget.PopupMenu;
import androidx.lifecycle.Observer;
import androidx.lifecycle.ViewModelProvider;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;
import androidx.swiperefreshlayout.widget.SwipeRefreshLayout;

import com.bumptech.glide.Glide;
import com.codercrew.fpw.Utils.SharedPrefrencesMain;
import com.codercrew.fpw.adapter.FamilyMemberWallAdapter;
import com.codercrew.fpw.home.interfaces.OnItemClick;
import com.codercrew.fpw.model.GetPostPagination;
import com.codercrew.fpw.model.GetProfile;
import com.codercrew.fpw.model.User;
import com.codercrew.fpw.pagination.PaginationListener;
import com.codercrew.fpw.viewmodel.MemberProfileViewModel;

import java.util.ArrayList;

import static com.codercrew.fpw.pagination.PaginationListener.PAGE_START;

public class MemberProfileWalls extends AppCompatActivity implements OnItemClick,View.OnClickListener {
    private MemberProfileViewModel memberProfileViewModel;
    private FamilyMemberWallAdapter familyMemberWallsAdapter;
    SharedPrefrencesMain sharedPrefrencesMain;
    ProgressDialog pd;
    ImageView imageViewProfile;
    TextView textViewName,textViewAdd;
    ArrayList<GetPostPagination.Data.PostDetails> getProfile=new ArrayList<>();
    User model;
    LinearLayout textViewNoPost;

    private boolean isLastPage = false;
    private int totalPage = 10;
    private boolean isLoading = false;
    int itemCount = 0;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_member_profile);

        sharedPrefrencesMain = new SharedPrefrencesMain(this);
        pd =new ProgressDialog(this);
        pd.setMessage(getString(R.string.loading));
        pd.show();
        memberProfileViewModel =
                new ViewModelProvider(this).get(MemberProfileViewModel.class);
        //receive
        model = (User) getIntent().getSerializableExtra("user");

        final RecyclerView recyclerView = findViewById(R.id.rvWalls);
        final ImageView imageView = findViewById(R.id.imgBack);
        final SwipeRefreshLayout swipeRefreshLayout = findViewById(R.id.swipe);
        imageViewProfile = findViewById(R.id.imgProfile);
        textViewName = findViewById(R.id.tvName);
        textViewAdd = findViewById(R.id.tvAddress);
        textViewNoPost = findViewById(R.id.textNoPost);

        memberProfileViewModel.hitProfileApi(model.getId(),"0");
        swipeRefreshLayout.setOnRefreshListener(new SwipeRefreshLayout.OnRefreshListener() {
            @Override
            public void onRefresh() {
                getProfile.clear();
                familyMemberWallsAdapter.clearItems();
                memberProfileViewModel.hitProfileApi(model.getId(),"0");
            }
        });

        imageView.setOnClickListener(this);

        recyclerView.setHasFixedSize(true);
        LinearLayoutManager layoutManager = new LinearLayoutManager(getApplicationContext(),RecyclerView.VERTICAL,false);
        recyclerView.setLayoutManager(layoutManager);
        familyMemberWallsAdapter = new FamilyMemberWallAdapter(getApplicationContext(), new ArrayList<>(), MemberProfileWalls.this);
        recyclerView.setAdapter(familyMemberWallsAdapter);

        memberProfileViewModel.getText().observe(this, new Observer<GetProfile>() {
            @Override
            public void onChanged(@Nullable GetProfile familyMembersModel) {
                pd.dismiss();
                swipeRefreshLayout.setRefreshing(false);
                if (familyMembersModel!=null) {
                    if(familyMembersModel.getData().getProfilepic().length()>0)
                        Glide.with(AppContext.getAppContext()).load(familyMembersModel.getData().getProfilepic()).into(imageViewProfile);
                    textViewName.setText(familyMembersModel.getData().getName());
                    if(familyMembersModel.getData().getCity().length()>0)
                        textViewAdd.setText(familyMembersModel.getData().getCity()+","+familyMembersModel.getData().getCountry());

                    if(familyMembersModel.getData()!=null && familyMembersModel.getData().getWall().getPosts().size()>0) {
                        if (memberProfileViewModel.currentPage != PAGE_START && getProfile.size()>=PAGE_START)
                            familyMemberWallsAdapter.removeLoading();
                        familyMemberWallsAdapter.addItems(familyMembersModel.getData().getWall().getPosts());

                        // check weather is last page or not
                        if (memberProfileViewModel.currentPage>0) {
                            familyMemberWallsAdapter.addLoading();
                        } else {
                            isLastPage = true;
                        }
                        isLoading = false;
                        getProfile.addAll(familyMembersModel.getData().getWall().getPosts());
                    }else {
                        textViewNoPost.setVisibility(View.VISIBLE);
                    }
                }

            }
        });

        recyclerView.addOnScrollListener(new PaginationListener(layoutManager) {
            @Override
            protected void loadMoreItems() {
                isLoading = true;
//                newsfeedViewModel.currentPage++;
                memberProfileViewModel.hitProfileApi(model.getId(),memberProfileViewModel.currentPage+"");
            }
            @Override
            public boolean isLastPage() {
                return isLastPage;
            }
            @Override
            public boolean isLoading() {
                return isLoading;
            }
        });
    }

    @Override
    public void onClick(View view, int pos) {
        if(view.getId() == R.id.tvLikes){
            Intent intent = new Intent(getApplicationContext(), LikesActivity.class);
            intent.putExtra("pid",getProfile.get(pos).getId());
            startActivity(intent);
        }else if(view.getId()==R.id.tvComments){
            Intent intent = new Intent(getApplicationContext(), CommentsActivity.class);
            intent.putExtra("pid",getProfile.get(pos).getId());
            startActivity(intent);
        }else if(view.getId()==R.id.imgLike){
//            pd.show();
            if(getProfile.get(pos).getIsliked().equals("0")) {
                getProfile.get(pos).setIsliked("1");
                getProfile.get(pos).setLikecount((Integer.parseInt(getProfile.get(pos).getLikecount()) + 1) + "");
                familyMemberWallsAdapter.notifyDataSetChanged();
                memberProfileViewModel.hitLikeApi(model.getId(), getProfile.get(pos).getId());
            }else {
                getProfile.get(pos).setIsliked("0");
                getProfile.get(pos).setLikecount((Integer.parseInt(getProfile.get(pos).getLikecount()) - 1) + "");
                familyMemberWallsAdapter.notifyDataSetChanged();
                memberProfileViewModel.hitLikeApi(model.getId(), getProfile.get(pos).getId());
            }
        }else if(view.getId()==R.id.imgSend){
//            pd.show();
            getProfile.get(pos).setCommentcount((Integer.parseInt(getProfile.get(pos).getCommentcount())+1)+"");
            familyMemberWallsAdapter.notifyDataSetChanged();
            memberProfileViewModel.hitCommentApi(model.getId(),getProfile.get(pos).getId());
        }
        else if(view.getId()==R.id.imgMenu){
            PopupMenu popup = new PopupMenu(MemberProfileWalls.this, view);
            //Inflating the Popup using xml file
            popup.getMenuInflater()
                    .inflate(R.menu.popup_menu, popup.getMenu());

            //registering popup with OnMenuItemClickListener
            popup.setOnMenuItemClickListener(new PopupMenu.OnMenuItemClickListener() {
                public boolean onMenuItemClick(MenuItem item) {
                    if(item.getItemId()==R.id.edit){
                        Intent intent = new Intent(getApplicationContext(), EditPostActivity.class);
                        intent.putExtra("post",getProfile.get(pos));
                        startActivity(intent);

                    }else
                        showConfirmationDialog(getProfile.get(pos).getId());
                    return true;
                }
            });

            popup.show(); //showing popup menu
        }
        else if(view.getId()==R.id.imgWall|| view.getId()==R.id.imgPlay){
            Intent intent = new Intent(getApplicationContext(), ViewPostActivity.class);
            intent.putExtra("post",getProfile.get(pos));
            intent.putExtra("from","wall");
            startActivity(intent);
        }
    }

    @Override
    public void onClick(View v) {
        finish();
    }

    private void showConfirmationDialog(String pid){
        android.app.AlertDialog.Builder builder1 = new android.app.AlertDialog.Builder(MemberProfileWalls.this);

        builder1.setTitle("Delete Post");
        builder1.setMessage("Are you sure");
        builder1.setCancelable(false);

        builder1.setPositiveButton(
                "Yes",
                new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int id) {
                        pd.show();
                        memberProfileViewModel.hitDeletePostApi(model.getId(),pid);
                    }
                });
        builder1.setNegativeButton("No", new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
                dialog.cancel();
            }
        });
        builder1.show();
    }

}