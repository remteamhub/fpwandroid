package com.codercrew.fpw;

import androidx.annotation.Nullable;
import androidx.appcompat.app.AppCompatActivity;
import androidx.lifecycle.Observer;
import androidx.lifecycle.ViewModelProvider;
import androidx.recyclerview.widget.GridLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import android.app.ProgressDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.os.Bundle;
import android.os.Handler;
import android.view.MotionEvent;
import android.view.View;
import android.view.inputmethod.InputMethodManager;
import android.widget.ImageView;
import android.widget.TextView;

import com.codercrew.fpw.Utils.SharedPrefrencesMain;
import com.codercrew.fpw.adapter.AlbumsGalleryAdapter;
import com.codercrew.fpw.home.interfaces.OnItemClick;
import com.codercrew.fpw.home.ui.media.MediaFragment;
import com.codercrew.fpw.model.GetAlbumPhotos;
import com.codercrew.fpw.viewmodel.AlbumsGalleryViewModel;

import java.util.ArrayList;

public class AlbumGalleryActivity extends AppCompatActivity implements OnItemClick, View.OnClickListener {

    private AlbumsGalleryAdapter albumAdapter;
    protected AlbumsGalleryViewModel AlbumsViewModel;
    ProgressDialog pd;
    SharedPrefrencesMain sharedPrefrencesMain;
    ImageView imageViewAdd;
    ArrayList<GetAlbumPhotos.Data> arrayList = new ArrayList<>();

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_album_gallery);

        sharedPrefrencesMain = new SharedPrefrencesMain(this);

        imageViewAdd = findViewById(R.id.imgAdd);
        imageViewAdd.setOnClickListener(this);
        AlbumsViewModel =
                new ViewModelProvider(this).get(AlbumsGalleryViewModel.class);
        final RecyclerView recyclerView = findViewById(R.id.rvBlockedMembers);
        final ImageView imageView = findViewById(R.id.imgBack);
        final TextView textViewTitle = findViewById(R.id.tvTitle);

        textViewTitle.setText(getIntent().getStringExtra("name"));

        imageView.setOnClickListener(this);
        recyclerView.setLayoutManager(new GridLayoutManager(AlbumGalleryActivity.this, 2));

        AlbumsViewModel.getText().observe(this, new Observer<ArrayList<GetAlbumPhotos.Data>>() {
            @Override
            public void onChanged(@Nullable ArrayList<GetAlbumPhotos.Data> familyMembersModel) {
                pd.dismiss();
                if (familyMembersModel != null) {
                    arrayList = familyMembersModel;
                    albumAdapter = new AlbumsGalleryAdapter(AlbumGalleryActivity.this, familyMembersModel, AlbumGalleryActivity.this);
                    recyclerView.setAdapter(albumAdapter);
                }
            }
        });


    }

    @Override
    public void onClick(View view, int pos) {
        if (view.getId() == R.id.imgProfile) {
            Intent intent = new Intent(getApplicationContext(), ViewPostForVideoActivity.class);
            intent.putExtra("post", arrayList.get(pos));
            intent.putExtra("from", "gallery");
            startActivity(intent);
        } else if (view.getId() == R.id.imgDelete) {
            showConfirmationDialog(pos, arrayList.get(pos).getId());
        }
    }

    @Override
    public void onClick(View v) {
        if (v.getId() == R.id.imgAdd) {
            Intent intent = new Intent(getApplicationContext(), UpdateAlbumActivity.class);
            intent.putExtra("type", getIntent().getStringExtra("type"));
            intent.putExtra("id", getIntent().getStringExtra("id"));
            startActivity(intent);
        } else {
            finish();
        }
    }

    @Override
    public void onResume() {
        super.onResume();
        pd = new ProgressDialog(this);
        pd.setMessage(getString(R.string.loading));
        pd.setCancelable(false);
        pd.show();

        if (MediaFragment.ISMEDIAUPDATED) {
            MediaFragment.ISMEDIAUPDATED = false;
            AlbumsViewModel =
                    new ViewModelProvider(this).get(AlbumsGalleryViewModel.class);
            new Handler().postDelayed(new Runnable() {
                @Override
                public void run() {
                    AlbumsViewModel.hitGetAlbumApi(getIntent().getStringExtra("id"), getIntent().getStringExtra("type"));
                }
            }, 3000);
        } else
            AlbumsViewModel.hitGetAlbumApi(getIntent().getStringExtra("id"), getIntent().getStringExtra("type"));

    }

    private void showConfirmationDialog(int pos, String pid) {
        android.app.AlertDialog.Builder builder1 = new android.app.AlertDialog.Builder(AlbumGalleryActivity.this);
        if (getIntent().getStringExtra("type").toString().equals("1"))
            builder1.setTitle("Delete Photo");
        else builder1.setTitle("Delete Video");
        builder1.setMessage("Are you sure");
        builder1.setCancelable(false);

        builder1.setPositiveButton(
                "Yes",
                new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int id) {
//                        arrayList.remove(pos);
                        albumAdapter.clearItem(pos);
                        AlbumsViewModel.hitDelAlbumApi(pid, getIntent().getStringExtra("type"));
                    }
                });
        builder1.setNegativeButton("No", new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
                dialog.cancel();
            }
        });
        builder1.show();
    }

    @Override
    public boolean dispatchTouchEvent(MotionEvent ev) {
        if (getCurrentFocus() != null) {
            InputMethodManager imm = (InputMethodManager) getSystemService(Context.INPUT_METHOD_SERVICE);
            imm.hideSoftInputFromWindow(getCurrentFocus().getWindowToken(), 0);
        }
        return super.dispatchTouchEvent(ev);
    }

    @Override
    public void onBackPressed() {
        super.onBackPressed();
        finish();
    }
}