package com.codercrew.fpw.adapter;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.constraintlayout.widget.ConstraintLayout;
import androidx.recyclerview.widget.RecyclerView;

import com.bumptech.glide.Glide;
import com.bumptech.glide.request.RequestOptions;
import com.codercrew.fpw.R;
import com.codercrew.fpw.home.interfaces.OnItemClick;
import com.codercrew.fpw.model.GetAlbums;

import java.util.ArrayList;

public class AlbumsOfUserAdapter extends RecyclerView.Adapter<AlbumsOfUserAdapter.LikesViewHolder> {

    ArrayList<GetAlbums.Data> familyMembersModelList = new ArrayList<>();
    Context context;
    OnItemClick onItemClick;

    public AlbumsOfUserAdapter(Context context, ArrayList<GetAlbums.Data> familyMembersModelList, OnItemClick onItemClick){
        this.context = context;
        this.familyMembersModelList = familyMembersModelList;
        this.onItemClick = onItemClick;
    }


    @NonNull
    @Override
    public LikesViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        LayoutInflater layoutInflater = LayoutInflater.from(parent.getContext());
        View view = layoutInflater.inflate(R.layout.albums_of_user_row_layout,parent,false);
        return new LikesViewHolder(view);
    }

    @Override
    public void onBindViewHolder(@NonNull LikesViewHolder holder, int position) {

        if(!familyMembersModelList.get(position).getImage().isEmpty()) {
            if(familyMembersModelList.get(position).getType().equals("1")) {
                Glide.with(context).load(familyMembersModelList.get(position).getImage()).into(holder.imageViewProfile);
            }else {
                RequestOptions requestOptions = new RequestOptions();
                            requestOptions.isMemoryCacheable();
                Glide.with(context).setDefaultRequestOptions(requestOptions).load(familyMembersModelList.get(position).getThumb()).into(holder.imageViewProfile);
            }
        }
        holder.textViewName.setText(familyMembersModelList.get(position).getAlbumname());

        holder.constraintLayout.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                onItemClick.onClick(v,position);
            }
        });

//        holder.imageViewDel.setOnClickListener(new View.OnClickListener() {
//            @Override
//            public void onClick(View v) {
//                onItemClick.onClick(v,position);
//            }
//        });
    }
    public void clearItem(int pos) {
        familyMembersModelList.remove(pos);
        notifyDataSetChanged();
    }
    @Override
    public int getItemCount() {
        return familyMembersModelList.size();
    }

    public class LikesViewHolder extends RecyclerView.ViewHolder{
        private ImageView imageViewProfile,imageViewDel;
        private ConstraintLayout constraintLayout;
        private TextView textViewName;


        public LikesViewHolder(@NonNull View itemView) {
            super(itemView);
            imageViewProfile = itemView.findViewById(R.id.imgProfile);
            textViewName = itemView.findViewById(R.id.tvName);
            imageViewDel = itemView.findViewById(R.id.imgDelete);
            constraintLayout = itemView.findViewById(R.id.constraintLayout);
        }
    }
}
