package com.codercrew.fpw.home.ui.subscription;

import androidx.lifecycle.ViewModelProvider;

import android.app.Activity;
import android.app.AlertDialog;
import android.content.Intent;
import android.os.Bundle;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.fragment.app.Fragment;

import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.LinearLayout;
import android.widget.Toast;

import com.codercrew.fpw.R;
import com.codercrew.fpw.Utils.SharedPrefrencesMain;
import com.codercrew.fpw.gpay.util.PaymentsUtil;
import com.google.android.gms.common.api.Status;
import com.google.android.gms.wallet.AutoResolveHelper;
import com.google.android.gms.wallet.PaymentData;
import com.google.android.gms.wallet.PaymentDataRequest;
import com.google.android.gms.wallet.PaymentsClient;

import org.json.JSONException;
import org.json.JSONObject;

import java.util.Optional;

public class PaymentSubscriptionFragment extends Fragment implements View.OnClickListener{

    private PaymentSubscriptionViewModel mViewModel;

    // Arbitrarily-picked constant integer you define to track a request for payment data activity.
    public static final int LOAD_PAYMENT_DATA_REQUEST_CODE = 991;

    private static final long SHIPPING_COST_CENTS = 90 * PaymentsUtil.CENTS_IN_A_UNIT.longValue();

    // A client for interacting with the Google Pay API.
    private PaymentsClient paymentsClient;

    LinearLayout llOneMonthSubscription,ll_mediuam_subscription,ll_large_subscription,llOneMessageSubscription;
    public static String plan;
    SharedPrefrencesMain sharedPrefrencesMain;
    public static PaymentSubscriptionFragment newInstance() {
        return new PaymentSubscriptionFragment();
    }

    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container,
                             @Nullable Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.payment_subscription_fragment, container, false);
        // Initialize a Google Pay API client for an environment suitable for testing.
        // It's recommended to create the PaymentsClient object inside of the onCreate method.
        paymentsClient = PaymentsUtil.createPaymentsClient(getActivity());
        llOneMonthSubscription = view.findViewById(R.id.llOneMonthSubscription);
        llOneMessageSubscription = view.findViewById(R.id.llOneMessageSubscription);
        ll_mediuam_subscription = view.findViewById(R.id.ll_mediuam_subscription);
        ll_large_subscription = view.findViewById(R.id.ll_large_subscription);

        llOneMonthSubscription.setOnClickListener(this);
        ll_large_subscription.setOnClickListener(this);
        ll_mediuam_subscription.setOnClickListener(this);
        llOneMessageSubscription.setOnClickListener(this);
        sharedPrefrencesMain = new SharedPrefrencesMain(getContext());
        return view;
    }

    @Override
    public void onActivityCreated(@Nullable Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);
        mViewModel = new ViewModelProvider(this).get(PaymentSubscriptionViewModel.class);
        // TODO: Use the ViewModel
    }

    public void requestPayment(double price) {


        // The price provided to the API should include taxes and shipping.
        // This price is not displayed to the user.
        try {
            double garmentPrice = price;
            long garmentPriceCents = Math.round(garmentPrice * PaymentsUtil.CENTS_IN_A_UNIT.longValue());
            long priceCents = garmentPriceCents + SHIPPING_COST_CENTS;

            Optional<JSONObject> paymentDataRequestJson = PaymentsUtil.getPaymentDataRequest(priceCents);
            if (!paymentDataRequestJson.isPresent()) {
                return;
            }

            PaymentDataRequest request =
                    PaymentDataRequest.fromJson(paymentDataRequestJson.get().toString());

            // Since loadPaymentData may show the UI asking the user to select a payment method, we use
            // AutoResolveHelper to wait for the user interacting with it. Once completed,
            // onActivityResult will be called with the result.
            if (request != null) {
                AutoResolveHelper.resolveTask(
                        paymentsClient.loadPaymentData(request),
                        getActivity(), LOAD_PAYMENT_DATA_REQUEST_CODE);
            }

        } catch (Exception e) {
            throw new RuntimeException("The price cannot be deserialized from the JSON object.");
        }
    }


    @Override
    public void onClick(View v) {
        if(v.getId()==R.id.llOneMonthSubscription){
            if(!sharedPrefrencesMain.getPlan().equals("0")){
                Toast.makeText(getContext(), "You are currently subscribed!", Toast.LENGTH_SHORT).show();
                return;
            }
            plan = "1";
            requestPayment(12.99);
        }else if(v.getId()==R.id.ll_mediuam_subscription){
            if(!sharedPrefrencesMain.getPlan().equals("0")){
                Toast.makeText(getContext(), "You are currently subscribed!", Toast.LENGTH_SHORT).show();
                return;
            }
            plan = "2";
            requestPayment(25.99);
        }else if(v.getId()==R.id.ll_large_subscription){
            if(!sharedPrefrencesMain.getPlan().equals("0")){
                Toast.makeText(getContext(), "You are currently subscribed!", Toast.LENGTH_SHORT).show();
                return;
            }
            plan = "3";
            requestPayment(40.99);
        }else if(v.getId()==R.id.llOneMessageSubscription){
            if(!sharedPrefrencesMain.getPlan().equals("0")){
                Toast.makeText(getContext(), "You are currently subscribed!", Toast.LENGTH_SHORT).show();
                return;
            }
            plan = "0";
            requestPayment(4.99);
        }
    }

    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        switch (requestCode) {
            // value passed in AutoResolveHelper
            case LOAD_PAYMENT_DATA_REQUEST_CODE:
                switch (resultCode) {

                    case Activity.RESULT_OK:
                        PaymentData paymentData = PaymentData.getFromIntent(data);
                        handlePaymentSuccess(paymentData);
                        break;

                    case Activity.RESULT_CANCELED:
                        // The user cancelled the payment attempt
                        break;

                    case AutoResolveHelper.RESULT_ERROR:
                        Status status = AutoResolveHelper.getStatusFromIntent(data);
                        handleError(status.getStatusCode());
                        break;
                }
        }
    }

    private void handlePaymentSuccess(PaymentData paymentData) {

        // Token will be null if PaymentDataRequest was not constructed using fromJson(String).
        final String paymentInfo = paymentData.toJson();
        if (paymentInfo == null) {
            return;
        }

        try {
            JSONObject paymentMethodData = new JSONObject(paymentInfo).getJSONObject("paymentMethodData");
            // If the gateway is set to "example", no payment information is returned - instead, the
            // token will only consist of "examplePaymentMethodToken".

            final JSONObject tokenizationData = paymentMethodData.getJSONObject("tokenizationData");
            final String tokenizationType = tokenizationData.getString("type");
            final String token = tokenizationData.getString("token");

            if ("PAYMENT_GATEWAY".equals(tokenizationType) && "examplePaymentMethodToken".equals(token)) {
                new AlertDialog.Builder(getActivity())
                        .setTitle("Warning")
                        .setMessage(getString(R.string.gateway_replace_name_example))
                        .setPositiveButton("OK", null)
                        .create()
                        .show();
            }

            final JSONObject info = paymentMethodData.getJSONObject("info");
            final String billingName = "";//info.getJSONObject("billingAddress").getString("name");
            Toast.makeText(
                    getActivity(), getString(R.string.payments_show_name, billingName),
                    Toast.LENGTH_LONG).show();

            // Logging token string.
            Log.d("Google Pay token: ", token);

        } catch (JSONException e) {
            throw new RuntimeException("The selected garment cannot be parsed from the list of elements");
        }
    }


    private void handleError(int statusCode) {
        Log.e("loadPaymentData failed", String.format("Error code: %d", statusCode));
    }
}