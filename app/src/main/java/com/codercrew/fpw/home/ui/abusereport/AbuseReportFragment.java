package com.codercrew.fpw.home.ui.abusereport;

import androidx.lifecycle.Observer;
import androidx.lifecycle.ViewModelProvider;

import android.app.ProgressDialog;
import android.graphics.drawable.Drawable;
import android.os.Bundle;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.fragment.app.Fragment;

import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.bumptech.glide.Glide;
import com.bumptech.glide.load.DataSource;
import com.bumptech.glide.load.engine.GlideException;
import com.bumptech.glide.request.RequestListener;
import com.bumptech.glide.request.target.Target;
import com.codercrew.fpw.AppContext;
import com.codercrew.fpw.R;
import com.codercrew.fpw.home.MainActivity;
import com.codercrew.fpw.home.ui.profile.ProfileFragment;
import com.codercrew.fpw.home.ui.profile.ProfileViewModel;
import com.codercrew.fpw.home.ui.profile.adapter.FamilyMembersAdapter;
import com.codercrew.fpw.model.SignUp;
import com.codercrew.fpw.model.User;

import org.w3c.dom.Text;

public class AbuseReportFragment extends Fragment {

    private TextView textViewCount;
    private AbuseReportViewModel mViewModel;
    ProgressDialog pd;
    public static AbuseReportFragment newInstance() {
        return new AbuseReportFragment();
    }

    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container,
                             @Nullable Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.abuse_report_fragment, container, false);

        textViewCount = view.findViewById(R.id.tvCount);
        pd =new ProgressDialog(getActivity());
        pd.setCancelable(false);
        pd.setMessage(getActivity().getString(R.string.loading));

        return view;
    }

    @Override
    public void onActivityCreated(@Nullable Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);
        pd.show();
        mViewModel =
                new ViewModelProvider(this).get(AbuseReportViewModel.class);
        mViewModel.hitProfileApi();
        mViewModel.getText().observe(getViewLifecycleOwner(), new Observer<SignUp>() {
            @Override
            public void onChanged(@Nullable SignUp familyMembersModel) {
                pd.dismiss();
                if(familyMembersModel!=null && familyMembersModel.getData()!=null) {
                  textViewCount.setText(familyMembersModel.getData().getUser().getReportCount());
                }
            }
        });
    }

}