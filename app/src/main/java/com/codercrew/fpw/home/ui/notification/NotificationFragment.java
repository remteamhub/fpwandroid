package com.codercrew.fpw.home.ui.notification;

import androidx.lifecycle.Observer;
import androidx.lifecycle.ViewModelProvider;

import android.app.ProgressDialog;
import android.content.Intent;
import android.os.Bundle;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.fragment.app.Fragment;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.codercrew.fpw.CommentsActivity;
import com.codercrew.fpw.R;
import com.codercrew.fpw.SinglePostActivity;
import com.codercrew.fpw.Utils.SharedPrefrencesMain;
import com.codercrew.fpw.adapter.CommentsAdapter;
import com.codercrew.fpw.home.interfaces.OnItemClick;
import com.codercrew.fpw.home.ui.notification.adapter.NotificationsAdapter;
import com.codercrew.fpw.model.GetComments;
import com.codercrew.fpw.model.GetNotifications;

import java.util.ArrayList;

public class NotificationFragment extends Fragment implements OnItemClick,View.OnClickListener{

    private NotificationViewModel mViewModel;
    private NotificationsAdapter notificationsAdapter;
    ProgressDialog pd;
    SharedPrefrencesMain sharedPrefrencesMain;
    ArrayList<GetNotifications.notificationsDetails> notificationsDetailsArrayList;
    RecyclerView recyclerView;

    public static NotificationFragment newInstance() {
        return new NotificationFragment();
    }

    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container,
                             @Nullable Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.notification_fragment, container, false);
        recyclerView = view.findViewById(R.id.recyclerView);
        recyclerView.setLayoutManager(new LinearLayoutManager(getActivity(),RecyclerView.VERTICAL,false));
        sharedPrefrencesMain = new SharedPrefrencesMain(getActivity());
        pd =new ProgressDialog(getActivity());
        pd.setMessage(getString(R.string.loading));
        pd.show();

        return view;
    }

    @Override
    public void onActivityCreated(@Nullable Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);
        mViewModel = new ViewModelProvider(this).get(NotificationViewModel.class);

        mViewModel.getText().observe(getViewLifecycleOwner(), new Observer<ArrayList<GetNotifications.notificationsDetails>>() {
            @Override
            public void onChanged(@Nullable ArrayList<GetNotifications.notificationsDetails> familyMembersModel) {
                pd.dismiss();
                if(familyMembersModel!=null) {
                    notificationsDetailsArrayList = familyMembersModel;
                    notificationsAdapter = new NotificationsAdapter(getContext(), notificationsDetailsArrayList, NotificationFragment.this);
                    recyclerView.setAdapter(notificationsAdapter);
                }
            }
        });

    }

    @Override
    public void onResume(){
        mViewModel.hitGetNotificationApi("");
        super.onResume();
    }

    @Override
    public void onClick(View v) {

    }

    @Override
    public void onClick(View view, int pos) {
        Intent intent = new Intent(getContext(), SinglePostActivity.class);
        intent.putExtra("postid",notificationsDetailsArrayList.get(pos).getPostid());
        intent.putExtra("id",notificationsDetailsArrayList.get(pos).getId());
        startActivity(intent);

    }
}