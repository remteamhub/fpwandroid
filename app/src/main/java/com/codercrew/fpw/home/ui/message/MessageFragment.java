package com.codercrew.fpw.home.ui.message;

import androidx.appcompat.app.AlertDialog;
import androidx.appcompat.widget.PopupMenu;
import androidx.lifecycle.ViewModelProvider;

import android.app.ProgressDialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.os.Bundle;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.fragment.app.Fragment;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import android.view.LayoutInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.bumptech.glide.Glide;
import com.codercrew.fpw.AddGroupMemberActivity;
import com.codercrew.fpw.ApiClient.ApiClient;
import com.codercrew.fpw.ApiClient.ApiInterface;
import com.codercrew.fpw.AppContext;
import com.codercrew.fpw.EditPostActivity;
import com.codercrew.fpw.MemberProfileActivity;
import com.codercrew.fpw.MessagesActivity;
import com.codercrew.fpw.R;
import com.codercrew.fpw.Utils.SharedPrefrencesMain;
import com.codercrew.fpw.Utils.utils;
import com.codercrew.fpw.chat.ChatActivity;
import com.codercrew.fpw.chat.model.Message;
import com.codercrew.fpw.chat.model.User;
import com.codercrew.fpw.home.interfaces.OnItemClick;
import com.codercrew.fpw.home.ui.message.adapter.UserMsgsAdapter;
import com.codercrew.fpw.home.ui.profile.ProfileFragment;
import com.codercrew.fpw.model.GetPost;
import com.codercrew.fpw.model.GetProfile;
import com.codercrew.fpw.model.GetUsers;
import com.google.firebase.database.ChildEventListener;
import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.FirebaseDatabase;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.HashMap;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class MessageFragment extends Fragment {

    private MessageViewModel mViewModel;
    SharedPrefrencesMain sharedPrefrencesMain;
    ArrayList<User> users = new ArrayList<>();
    ArrayList<String> members = new ArrayList<>();
    UserMsgsAdapter adapter;
    RecyclerView recyclerView;
    ProgressDialog pd;

    public static MessageFragment newInstance() {
        return new MessageFragment();
    }

    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container,
                             @Nullable Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.message_fragment, container, false);

        pd =new ProgressDialog(getActivity());
        pd.setCancelable(false);
        pd.setMessage(getActivity().getString(R.string.loading));

        sharedPrefrencesMain = new SharedPrefrencesMain(getActivity());
        recyclerView = view.findViewById(R.id.recyclerView);
        LinearLayoutManager layoutManager = new LinearLayoutManager(getActivity(),RecyclerView.VERTICAL,false);
        recyclerView.setLayoutManager(layoutManager);
        adapter = new UserMsgsAdapter(getContext(),users, new OnItemClick() {
            @Override
            public void onClick(View view, int pos) {
                if(view.getId()==R.id.imgMenu){
                    PopupMenu popup = new PopupMenu(getActivity(), view);
                    //Inflating the Popup using xml file
                    popup.getMenuInflater()
                            .inflate(R.menu.group_menu, popup.getMenu());

                    //registering popup with OnMenuItemClickListener
                    popup.setOnMenuItemClickListener(new PopupMenu.OnMenuItemClickListener() {
                        public boolean onMenuItemClick(MenuItem item) {
                            if(item.getItemId()==R.id.edit){

                                Intent intent = new Intent(getContext(), AddGroupMemberActivity.class);
                                intent.putExtra("user", users.get(pos));
                                startActivity(intent);
                            }
//                        showConfirmationDialog(pos,postDetailsArrayList.get(pos).getId());
                            return true;
                        }
                    });
                    popup.show(); //showing popup menu
                }else {
                    pd.show();
                    hitCanMessageApi(users.get(pos).id,pos);


                }

            }
        });
        recyclerView.setAdapter(adapter);


        return view;
    }

    @Override
    public void onActivityCreated(@Nullable Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);
        mViewModel = new ViewModelProvider(this).get(MessageViewModel.class);
        // TODO: Use the ViewModel


    }

    @Override
    public void onResume(){
        super.onResume();
        users.clear();
        sharedPrefrencesMain = new SharedPrefrencesMain(getActivity());
        FirebaseDatabase.getInstance().getReference().child("Friends").child(sharedPrefrencesMain.getId()).orderByChild("timestamp").addChildEventListener(new ChildEventListener() {
            @Override
            public void onChildAdded(DataSnapshot dataSnapshot, String s) {
                if (dataSnapshot.getValue() != null) {
                    HashMap mapUser = (HashMap) dataSnapshot.getValue();
                    User user = new User();
                    user.id = (String) mapUser.get("id");
                    user.name = (String) mapUser.get("name");
                    user.avata = (String) mapUser.get("avata");

                    if(mapUser.get("type")!=null)
                        user.type = (String) mapUser.get("type").toString();
                    else user.type = "1";

                    if(mapUser.get("message")!=null) {
                        HashMap mapMsg = (HashMap) mapUser.get("message");
                        if(mapMsg.get("text")!=null && mapMsg.get("text").toString().length()>0)
                            user.message.text = (String) mapMsg.get("text");
                        else user.message.text = (String) mapMsg.get("");
                        if(mapMsg.get("status")!=null)
                            user.message.status = (String) mapMsg.get("status");
                    }
                    if(mapUser.get("memberIds")!=null){
                        user.memberIds = (String) mapUser.get("memberIds");
                    }
                    users.add(user);
                    adapter.notifyDataSetChanged();
                }
            }

            @Override
            public void onChildChanged(DataSnapshot dataSnapshot, String s) {

            }

            @Override
            public void onChildRemoved(DataSnapshot dataSnapshot) {

            }

            @Override
            public void onChildMoved(DataSnapshot dataSnapshot, String s) {

            }

            @Override
            public void onCancelled(DatabaseError databaseError) {
                databaseError.getMessage();
            }
        });
    }


    public void hitCanMessageApi(String postId,int pos)
    {
        Call<GetProfile> verify;

        ApiInterface apiInterface = ApiClient.getClient().create(ApiInterface.class);
        verify = apiInterface.canmessage("Bearer "+sharedPrefrencesMain.getToken(),postId);

        verify.enqueue(new Callback<GetProfile>() {
            @Override
            public void onResponse(Call<GetProfile> call, Response<GetProfile> response) {
                if (response.isSuccessful() && response.body().getStatus()) {
//                    if(response.body().getData().isCanmessage()){
                        if(users.get(pos).type!=null && users.get(pos).type.equals("1")) {
                            utils.updateMessagStatus(sharedPrefrencesMain.getId(), users.get(pos).id);
                        }
                        utils.GetUnreadMessagesCount(sharedPrefrencesMain.getId());
                        hitProfileApi(postId,pos,response.body().getMessage(),response.body().getData().isCanmessage());

//                    }else {
//                        pd.dismiss();
//                        Toast.makeText(getContext(), "" + response.body().getMessage(), Toast.LENGTH_SHORT).show();
//                    }
                }else pd.dismiss();
            }
            @Override
            public void onFailure(Call<GetProfile> call, Throwable t) {
                pd.dismiss();
                Toast.makeText(AppContext.getAppContext(), "fail", Toast.LENGTH_LONG).show();
            }
        });
    }

    public void hitProfileApi(String id,int pos,String msg,boolean canmessage)
    {
        Call<GetProfile> verify;
        ApiInterface apiInterface = ApiClient.getClient().create(ApiInterface.class);
//        verify = apiInterface.doverify(code.getText().toString().trim(),"Bearer"+sharedPrefrencesMain.getToken());
        verify = apiInterface.getprofilebyid("Bearer "+sharedPrefrencesMain.getToken(),id);

        verify.enqueue(new Callback<GetProfile>() {
            @Override
            public void onResponse(Call<GetProfile> call, Response<GetProfile> response) {
                pd.dismiss();
                if (response.isSuccessful() && response.body().getStatus()) {
                    Intent intent = new Intent(getActivity(), ChatActivity.class);
                    com.codercrew.fpw.model.User user = new com.codercrew.fpw.model.User();
                    user.setId(users.get(pos).id);
                    user.setEmail(response.body().getData().getEmail());
                    user.setName(users.get(pos).name);
                    user.setProfilepic(response.body().getData().getProfilepic());
                    user.setType(users.get(pos).type);

                    intent.putExtra("user", user);
                    intent.putExtra("canmessage",canmessage);
                    intent.putExtra("message",msg);
                    startActivity(intent);
                }
            }

            @Override
            public void onFailure(Call<GetProfile> call, Throwable t) {
                pd.dismiss();
                Toast.makeText(getContext(), "fail", Toast.LENGTH_LONG).show();
            }
        });
    }
}