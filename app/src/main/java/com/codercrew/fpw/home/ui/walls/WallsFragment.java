package com.codercrew.fpw.home.ui.walls;

import android.Manifest;
import android.app.AlertDialog;
import android.app.DatePickerDialog;
import android.app.ProgressDialog;
import android.app.TimePickerDialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.graphics.Bitmap;
import android.media.MediaScannerConnection;
import android.net.Uri;
import android.os.Build;
import android.os.Bundle;
import android.os.Environment;
import android.os.Handler;
import android.os.StrictMode;
import android.provider.MediaStore;
import android.view.LayoutInflater;
import android.view.MenuItem;
import android.view.MotionEvent;
import android.view.View;
import android.view.ViewGroup;
import android.widget.DatePicker;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.TimePicker;
import android.widget.Toast;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.appcompat.widget.PopupMenu;
import androidx.constraintlayout.widget.ConstraintLayout;
import androidx.core.content.FileProvider;
import androidx.fragment.app.Fragment;
import androidx.lifecycle.Observer;
import androidx.lifecycle.ViewModelProvider;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;
import androidx.swiperefreshlayout.widget.SwipeRefreshLayout;

import com.codercrew.fpw.AppContext;
import com.codercrew.fpw.CommentsActivity;
import com.codercrew.fpw.EditPostActivity;
import com.codercrew.fpw.LikesActivity;
import com.codercrew.fpw.PostActivity;
import com.codercrew.fpw.R;
import com.codercrew.fpw.Utils.SharedPrefrencesMain;
import com.codercrew.fpw.ViewPostActivity;
import com.codercrew.fpw.adapter.NewsfeedAdapter;
import com.codercrew.fpw.adapter.WallsAdapter;
import com.codercrew.fpw.home.interfaces.OnItemClick;
import com.codercrew.fpw.home.ui.newsfeed.NewsfeedFragment;
import com.codercrew.fpw.model.GetPost;
import com.bumptech.glide.Glide;
import com.codercrew.fpw.model.GetPostPagination;
import com.codercrew.fpw.pagination.PaginationListener;
import com.github.dhaval2404.imagepicker.ImagePicker;
import com.yalantis.ucrop.UCrop;

import java.io.ByteArrayOutputStream;
import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;

import static android.app.Activity.RESULT_CANCELED;
import static android.os.Environment.getExternalStoragePublicDirectory;
import static com.codercrew.fpw.pagination.PaginationListener.PAGE_START;

import org.json.JSONException;
import org.json.JSONObject;

import okhttp3.MediaType;
import okhttp3.MultipartBody;
import okhttp3.OkHttpClient;
import okhttp3.Request;
import okhttp3.RequestBody;
import okhttp3.Response;


public class WallsFragment extends Fragment implements OnItemClick,View.OnClickListener {
    private static final int PICK_IMAGE = 100,CAMERA=2,PICK_VIDEO=200,CAMERAVIDEO=300;
    private static final String IMAGE_DIRECTORY = "/encoded_image";
    private WallsViewModel wallsViewModel;
    private WallsAdapter familyMemberWallsAdapter;
    SharedPrefrencesMain sharedPrefrencesMain;
    ProgressDialog pd;
    private boolean isLastPage = false;
    private int totalPage = 10;
    private boolean isLoading = false;
    int itemCount = 0;
    ArrayList<GetPostPagination.Data.PostDetails> postDetailsArrayList = new ArrayList<>();
    EditText editTextDesc;
    public static boolean ISUPDATED = false;
    Uri cameraUri;
    Long futureTimeInMillis=0L;
    boolean isFutureMessage = false;

    public View onCreateView(@NonNull LayoutInflater inflater,
                             ViewGroup container, Bundle savedInstanceState) {
        wallsViewModel =
                new ViewModelProvider(this).get(WallsViewModel.class);
        View root = inflater.inflate(R.layout.fragment_newsfeed, container, false);
        sharedPrefrencesMain = new SharedPrefrencesMain(getContext());
        StrictMode.ThreadPolicy policy = new StrictMode.ThreadPolicy.Builder()
                .permitAll().build();
        StrictMode.setThreadPolicy(policy);

        pd =new ProgressDialog(getActivity());
        pd.setMessage(getActivity().getString(R.string.loading));
        pd.show();

        final RecyclerView recyclerView = root.findViewById(R.id.rvWalls);
        final ImageView imageViewProfile = root.findViewById(R.id.imgProfile);
        final ConstraintLayout constraintLayout = root.findViewById(R.id.constraintLayout);
        final ImageView imageViewPhoto = root.findViewById(R.id.ivPhoto);
        final ImageView imageViewVideo = root.findViewById(R.id.ivVideo);
        final ImageView imageViewPost = root.findViewById(R.id.ivPost);
        final ImageView imageViewCal = root.findViewById(R.id.ivcal);
        final SwipeRefreshLayout swipeRefreshLayout = root.findViewById(R.id.swipe);
        editTextDesc = root.findViewById(R.id.tvName);
        imageViewPhoto.setOnClickListener(this);
        imageViewVideo.setOnClickListener(this);
        imageViewPost.setOnClickListener(this);
        imageViewCal.setOnClickListener(this);

        constraintLayout.setOnTouchListener(new View.OnTouchListener() {
            @Override
            public boolean onTouch(View v, MotionEvent event) {
                editTextDesc.setError(null);
                return false;
            }
        });


        swipeRefreshLayout.setOnRefreshListener(new SwipeRefreshLayout.OnRefreshListener() {
            @Override
            public void onRefresh() {
                postDetailsArrayList.clear();
                familyMemberWallsAdapter.clearItems();
                wallsViewModel.hitGetPostApi1("0");
            }
        });
        if(sharedPrefrencesMain.getProfileUrl().length()>0)
            Glide.with(AppContext.getAppContext()).load(sharedPrefrencesMain.getProfileUrl()).into(imageViewProfile);


        recyclerView.setHasFixedSize(true);
        LinearLayoutManager layoutManager = new LinearLayoutManager(getContext(),RecyclerView.VERTICAL,false);
        recyclerView.setLayoutManager(layoutManager);
        familyMemberWallsAdapter = new WallsAdapter(getContext(), new ArrayList<>(), WallsFragment.this);
        recyclerView.setAdapter(familyMemberWallsAdapter);
        wallsViewModel.getText().observe(getViewLifecycleOwner(), new Observer<ArrayList<GetPostPagination.Data.PostDetails>>() {
            @Override
            public void onChanged(@Nullable ArrayList<GetPostPagination.Data.PostDetails> familyMembersModel) {
                pd.dismiss();
                swipeRefreshLayout.setRefreshing(false);
                if(familyMembersModel!=null) {
                    editTextDesc.setText("");
                    editTextDesc.clearFocus();

                    if (wallsViewModel.currentPage != PAGE_START && (postDetailsArrayList.size()>=PAGE_START ))
                        familyMemberWallsAdapter.removeLoading();
                    familyMemberWallsAdapter.addItems(familyMembersModel);

                    // check weather is last page or not
                    if (wallsViewModel.currentPage>0) {
                        familyMemberWallsAdapter.addLoading();
                    } else {
                        isLastPage = true;
                    }
                    isLoading = false;
                    postDetailsArrayList.addAll(familyMembersModel);
                }
            }
        });

        recyclerView.addOnScrollListener(new PaginationListener(layoutManager) {
            @Override
            protected void loadMoreItems() {
                isLoading = true;
//                newsfeedViewModel.currentPage++;
                wallsViewModel.hitGetPostApi1(wallsViewModel.currentPage+"");
            }
            @Override
            public boolean isLastPage() {
                return isLastPage;
            }
            @Override
            public boolean isLoading() {
                return isLoading;
            }
        });

        return root;
    }

    @Override
    public void onClick(View view, int pos) {

        if(view.getId() == R.id.tvLikes){
            Intent intent = new Intent(getContext(), LikesActivity.class);
            intent.putExtra("pid",postDetailsArrayList.get(pos).getId());
            startActivity(intent);
        }else if(view.getId()==R.id.tvComments){
            Intent intent = new Intent(getContext(), CommentsActivity.class);
            intent.putExtra("pid",postDetailsArrayList.get(pos).getId());
            startActivity(intent);
        }else if(view.getId()==R.id.imgLike){
//            pd.show();
            if(postDetailsArrayList.get(pos).getIsliked().equals("0")) {
                postDetailsArrayList.get(pos).setIsliked("1");
                postDetailsArrayList.get(pos).setLikecount((Integer.parseInt(postDetailsArrayList.get(pos).getLikecount()) + 1) + "");
                familyMemberWallsAdapter.notifyDataSetChanged();
                wallsViewModel.hitLikeApi(postDetailsArrayList.get(pos).getId());
            }else {
                postDetailsArrayList.get(pos).setIsliked("0");
                postDetailsArrayList.get(pos).setLikecount((Integer.parseInt(postDetailsArrayList.get(pos).getLikecount()) - 1) + "");
                familyMemberWallsAdapter.notifyDataSetChanged();
                wallsViewModel.hitLikeApi(postDetailsArrayList.get(pos).getId());
            }
        }else if(view.getId()==R.id.imgSend){
//            pd.show();

            postDetailsArrayList.get(pos).setCommentcount((Integer.parseInt(postDetailsArrayList.get(pos).getCommentcount())+1)+"");
            familyMemberWallsAdapter.notifyDataSetChanged();
            wallsViewModel.hitCommentApi(postDetailsArrayList.get(pos).getId());
        }else if(view.getId()==R.id.imgMenu){
            PopupMenu popup = new PopupMenu(getActivity(), view);
            //Inflating the Popup using xml file
            popup.getMenuInflater()
                    .inflate(R.menu.popup_menu, popup.getMenu());

            //registering popup with OnMenuItemClickListener
            popup.setOnMenuItemClickListener(new PopupMenu.OnMenuItemClickListener() {
                public boolean onMenuItemClick(MenuItem item) {
                    if(item.getItemId()==R.id.edit){
                        Intent intent = new Intent(getContext(), EditPostActivity.class);
                        intent.putExtra("post",postDetailsArrayList.get(pos));
                        startActivity(intent);

                    }else
                        showConfirmationDialog(pos,postDetailsArrayList.get(pos).getId());
                    return true;
                }
            });

            popup.show(); //showing popup menu
        }
        else if(view.getId()==R.id.imgWall|| view.getId()==R.id.imgPlay){
            Intent intent = new Intent(getContext(), ViewPostActivity.class);
            intent.putExtra("post",postDetailsArrayList.get(pos));
            intent.putExtra("from","wall");
            startActivity(intent);
        }
    }

    private void showVideoDialog(){

        AlertDialog.Builder pictureDialog = new AlertDialog.Builder(getActivity());
        pictureDialog.setTitle("Choose From");
        String[] pictureDialogItems = {
                "Gallery",
                "Camera"};
        pictureDialog.setItems(pictureDialogItems,
                new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {
                        switch (which) {
                            case 0:
                                openGalleryVideo();
                                break;
                            case 1:
                                takeVideoFromCamera();
                                break;
                        }
                    }
                });
        pictureDialog.show();

    }

    private void showPictureDialog(){

        AlertDialog.Builder pictureDialog = new AlertDialog.Builder(getActivity());
        pictureDialog.setTitle("Choose From");
        String[] pictureDialogItems = {
                "Gallery",
                "Camera"};
        pictureDialog.setItems(pictureDialogItems,
                new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {
                        switch (which) {
                            case 0:
                                openGallery();
                                break;
                            case 1:
                                takePhotoFromCamera();
                                break;
                        }
                    }
                });
        pictureDialog.show();

    }

    private void openGallery() {
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
            if (getActivity().checkSelfPermission(Manifest.permission.READ_EXTERNAL_STORAGE) == PackageManager.PERMISSION_GRANTED) {
                Intent gallery = new Intent(Intent.ACTION_PICK, MediaStore.Images.Media.INTERNAL_CONTENT_URI);
                gallery.setType("image/*");
                startActivityForResult(gallery, PICK_IMAGE);
            }
            else {
                requestPermissions( new String[]{Manifest.permission.READ_EXTERNAL_STORAGE,Manifest.permission.WRITE_EXTERNAL_STORAGE}, 5);
            }
        }else {
            Intent gallery = new Intent(Intent.ACTION_PICK, MediaStore.Images.Media.INTERNAL_CONTENT_URI);
            gallery.setType("image/*");
            startActivityForResult(gallery, PICK_IMAGE);
        }
    }

    private void openGalleryVideo() {
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
            if (getActivity().checkSelfPermission(Manifest.permission.READ_EXTERNAL_STORAGE) == PackageManager.PERMISSION_GRANTED) {
                Intent gallery = new Intent(Intent.ACTION_PICK, MediaStore.Images.Media.INTERNAL_CONTENT_URI);
                gallery.setType("video/*");
                startActivityForResult(gallery, PICK_VIDEO);
            }
            else {
                requestPermissions( new String[]{Manifest.permission.READ_EXTERNAL_STORAGE,Manifest.permission.WRITE_EXTERNAL_STORAGE}, 6);
            }
        }else {
            Intent gallery = new Intent(Intent.ACTION_PICK, MediaStore.Images.Media.INTERNAL_CONTENT_URI);
            gallery.setType("video/*");
            startActivityForResult(gallery, PICK_VIDEO);
        }
    }
    private void takeVideoFromCamera() {
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
            if (getActivity().checkSelfPermission(Manifest.permission.CAMERA) == PackageManager.PERMISSION_GRANTED) {
                Intent intent = new Intent(MediaStore.ACTION_VIDEO_CAPTURE);
//                    intent.setType("video/*");
                startActivityForResult(intent, CAMERAVIDEO);
            }
            else {
                requestPermissions(new String[]{Manifest.permission.CAMERA}, 7);
            }
        }else {
            Intent intent = new Intent(android.provider.MediaStore.ACTION_VIDEO_CAPTURE);
//                intent.setType("video/*");
            startActivityForResult(intent, CAMERAVIDEO);
        }
    }
    private void takePhotoFromCamera() {
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
            if (getActivity().checkSelfPermission(Manifest.permission.CAMERA) == PackageManager.PERMISSION_GRANTED) {
                Intent intent = new Intent(android.provider.MediaStore.ACTION_IMAGE_CAPTURE);
                if (Build.VERSION.SDK_INT > Build.VERSION_CODES.Q) {
                    cameraUri = FileProvider.getUriForFile(getContext(),"com.codercrew.fpw.provider",new File(getExternalStoragePublicDirectory(Environment.DIRECTORY_DCIM), "pic_"+ String.valueOf(System.currentTimeMillis()) + ".jpg"));
                }else
                    cameraUri = FileProvider.getUriForFile(getContext(),"com.codercrew.fpw.provider",new File(Environment.getExternalStorageDirectory(), "pic_"+ String.valueOf(System.currentTimeMillis()) + ".jpg"));                intent.addFlags(Intent.FLAG_GRANT_READ_URI_PERMISSION);
                intent.putExtra(MediaStore.EXTRA_OUTPUT, cameraUri);
//                    intent.setType("image/*");
                startActivityForResult(intent, CAMERA);
            }
            else {
                requestPermissions(new String[]{Manifest.permission.CAMERA}, 4);
            }
        }else {

            Intent intent = new Intent(android.provider.MediaStore.ACTION_IMAGE_CAPTURE);
//                intent.setType("image/*");
            startActivityForResult(intent, CAMERA);
        }
    }

    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        if (resultCode == RESULT_CANCELED) {
            return;
        }
        if (requestCode == PICK_IMAGE) {
            if (data != null) {
                Uri contentURI = data.getData();
                UCrop.of(data.getData(), Uri.fromFile(new File(getActivity().getCacheDir(), "IMG_" + System.currentTimeMillis())))
                        .start(getActivity(),WallsFragment.this);
            }
        }else if (requestCode == PICK_VIDEO) {
            if (data != null) {
                Uri contentURI = data.getData();
                Intent intent = new Intent(getContext(), PostActivity.class);
                intent.putExtra("Bitmap",contentURI);
                intent.putExtra("type","video");
                intent.putExtra("isFuture",isFutureMessage);
                intent.putExtra("futureTimeInMillis",futureTimeInMillis);
                if(!editTextDesc.getText().toString().isEmpty())
                    intent.putExtra("desc",editTextDesc.getText().toString());
                else
                    intent.putExtra("desc","");
                startActivity(intent);
            }
        } else if (requestCode == CAMERA) {
            UCrop.of(cameraUri, Uri.fromFile(new File(getActivity().getCacheDir(), "IMG_" + System.currentTimeMillis())))
                    .start(getActivity(),WallsFragment.this);

//            Toast.makeText(getContext(), "Image Saved!", Toast.LENGTH_SHORT).show();
        }else if(requestCode == CAMERAVIDEO){
            Uri videoUri = data.getData();
            Intent intent = new Intent(getContext(), PostActivity.class);
            intent.putExtra("Bitmap",videoUri);
            intent.putExtra("type","video");
            intent.putExtra("isFuture",isFutureMessage);
            intent.putExtra("futureTimeInMillis",futureTimeInMillis);
            if(!editTextDesc.getText().toString().isEmpty())
                intent.putExtra("desc",editTextDesc.getText().toString());
            else
                intent.putExtra("desc","");
            startActivity(intent);
        }else if(requestCode==UCrop.REQUEST_CROP){
            Uri imgUri = UCrop.getOutput(data);
            if (imgUri != null) {
                String selectedImage = imgUri.getPath();
                try {
                    Bitmap bitmap = MediaStore.Images.Media.getBitmap(getActivity().getContentResolver(), imgUri);
                    String path = saveImage(bitmap);
                } catch (IOException e) {
                    e.printStackTrace();
                    Toast.makeText(getContext(), "Failed!", Toast.LENGTH_SHORT).show();
                }
                // load selectedImage into ImageView
            }
        }
    }

    @Override
    public void onRequestPermissionsResult(int requestCode, @NonNull String[] permissions, @NonNull int[] grantResults) {
        super.onRequestPermissionsResult(requestCode, permissions, grantResults);
        if (grantResults.length > 0 && grantResults[0] == PackageManager.PERMISSION_GRANTED && requestCode == 4) {
            takePhotoFromCamera();
        }
        else if (grantResults.length > 0 && grantResults[0] == PackageManager.PERMISSION_GRANTED && requestCode == 5) {
            openGallery();
        }else if (grantResults.length > 0 && grantResults[0] == PackageManager.PERMISSION_GRANTED && requestCode == 6) {
            openGalleryVideo();
        }else if (grantResults.length > 0 && grantResults[0] == PackageManager.PERMISSION_GRANTED && requestCode == 7) {
            takeVideoFromCamera();
        }
    }

    public String saveImage(Bitmap myBitmap) {
        ByteArrayOutputStream bytes = new ByteArrayOutputStream();
        myBitmap.compress(Bitmap.CompressFormat.JPEG, 90, bytes);
        File wallpaperDirectory;
        if (Build.VERSION.SDK_INT > Build.VERSION_CODES.Q) {
            wallpaperDirectory = new File(
                    getExternalStoragePublicDirectory(Environment.DIRECTORY_DCIM) + IMAGE_DIRECTORY);
        }else {
            wallpaperDirectory = new File(
                    Environment.getExternalStorageDirectory() + IMAGE_DIRECTORY);
        }
        // have the object build the directory structure, if needed.
        if (!wallpaperDirectory.exists()) {
            wallpaperDirectory.mkdirs();
        }
        try {
            File f = new File(wallpaperDirectory, Calendar.getInstance()
                    .getTimeInMillis() + ".jpg");
            if (!f.getParentFile().exists())
                f.getParentFile().mkdirs();
            if (!f.exists())
                f.createNewFile();
            FileOutputStream fo = new FileOutputStream(f);
            fo.write(bytes.toByteArray());
            MediaScannerConnection.scanFile(getActivity(),
                    new String[]{f.getPath()},
                    new String[]{"image/jpeg"}, null);

            fo.close();
            Intent intent = new Intent(getContext(), PostActivity.class);
            intent.putExtra("Bitmap",f.getAbsolutePath());
            intent.putExtra("type","image");
            intent.putExtra("isFuture",isFutureMessage);
            intent.putExtra("futureTimeInMillis",futureTimeInMillis);
            if(!editTextDesc.getText().toString().isEmpty())
                intent.putExtra("desc",editTextDesc.getText().toString());
            else
                intent.putExtra("desc","");
            startActivity(intent);
//            callApiUploadImage(f.getAbsolutePath());
            return f.getAbsolutePath();
        } catch (IOException e1) {
            e1.printStackTrace();
        }
        return "";
    }

    @Override
    public void onClick(View v) {
        if(v.getId()==R.id.ivPhoto){

            showPictureDialog();
        }else if(v.getId()==R.id.ivVideo){
            showVideoDialog();
        }else if(v.getId()==R.id.ivPost){
            if(editTextDesc.getText().toString().isEmpty()){
                editTextDesc.setError("Enter something");
                editTextDesc.requestFocus();
                return;
            }
            CheckText(editTextDesc.getText().toString());

        }else if(v.getId()==R.id.ivcal){
            showDateTimePicker();
        }
    }

    private void showConfirmationDialog(int pos,String pid){
        android.app.AlertDialog.Builder builder1 = new android.app.AlertDialog.Builder(getActivity());

        builder1.setTitle("Delete Post");
        builder1.setMessage("Are you sure");
        builder1.setCancelable(false);

        builder1.setPositiveButton(
                "Yes",
                new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int id) {
//                        pd.show();
                        postDetailsArrayList.remove(postDetailsArrayList.get(pos));
                        familyMemberWallsAdapter.clearItem(pos);
                        wallsViewModel.hitDeletePostApi(pid);
                    }
                });
        builder1.setNegativeButton("No", new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
                dialog.cancel();
            }
        });
        builder1.show();
    }

    @Override
    public void onResume(){
        super.onResume();

        if(ISUPDATED){
            ISUPDATED = false;
            pd.show();
            wallsViewModel =
                    new ViewModelProvider(this).get(WallsViewModel.class);
            postDetailsArrayList.clear();
            new Handler().postDelayed(new Runnable() {
                @Override
                public void run() {
                    familyMemberWallsAdapter.clearItems();
                    familyMemberWallsAdapter.clearItems();
                    wallsViewModel.hitGetPostApi1("0");
                }
            },4000);

        }
    }

    Calendar date;
    public void showDateTimePicker() {
        final Calendar currentDate = Calendar.getInstance();
        date = Calendar.getInstance();
        DatePickerDialog datePickerDialog = new DatePickerDialog(getActivity(), new DatePickerDialog.OnDateSetListener() {
            @Override
            public void onDateSet(DatePicker view, int year, int monthOfYear, int dayOfMonth) {
                date.set(year, monthOfYear, dayOfMonth);
                TimePickerDialog timePickerDialog = new TimePickerDialog(getActivity(), new TimePickerDialog.OnTimeSetListener() {
                    @Override
                    public void onTimeSet(TimePicker view, int hourOfDay, int minute) {
                        date.set(Calendar.HOUR_OF_DAY, hourOfDay);
                        date.set(Calendar.MINUTE, minute);
                        isFutureMessage = true;
                        futureTimeInMillis = date.getTimeInMillis();
                        if(futureTimeInMillis<System.currentTimeMillis()){
                            Toast.makeText(getActivity(), "Time should be grater then current time", Toast.LENGTH_SHORT).show();
                            isFutureMessage = false;
                        }
//                        Toast.makeText(ChatActivity.this, "The choosen one " + date.getTime(), Toast.LENGTH_SHORT).show();
//                        Log.v(TAG, "The choosen one " + date.getTime());
                    }
                }, currentDate.get(Calendar.HOUR_OF_DAY), currentDate.get(Calendar.MINUTE), false);
                timePickerDialog.show();
            }
        }, currentDate.get(Calendar.YEAR), currentDate.get(Calendar.MONTH), currentDate.get(Calendar.DATE));
        datePickerDialog.getDatePicker().setMinDate(System.currentTimeMillis());
        Date maxDate = new Date (9999, 11, 31);
        datePickerDialog.getDatePicker().setMaxDate(maxDate.getTime());
        datePickerDialog.show();
    }

    private void CheckText(String text){
        ProgressDialog progressDialog = new ProgressDialog(getActivity());
        progressDialog.setMessage("Checking Validity....");
        progressDialog.show();
        new Thread(new Runnable() {
            @Override
            public void run() {
                OkHttpClient client = new OkHttpClient().newBuilder()
                        .build();
                MediaType mediaType = MediaType.parse("text/plain");
                RequestBody body = new MultipartBody.Builder().setType(MultipartBody.FORM)
                        .addFormDataPart("models","text")
                        .addFormDataPart("text",text)
                        .addFormDataPart("api_user",getActivity().getString(R.string.api_key))
                        .addFormDataPart("api_secret",getActivity().getString(R.string.api_secret))
                        .addFormDataPart("lang","en")
                        .addFormDataPart("mode","standard")
                        .build();
                Request request = new Request.Builder()
                        .url("https://api.sightengine.com/1.0/text/check.json")
                        .method("POST", body)
                        .build();
                try {
                    okhttp3.Response response = client.newCall(request).execute();
                    try {
                        JSONObject jsonObject = new JSONObject(response.body().string());
                        JSONObject jsonObjectNudity = jsonObject.getJSONObject("profanity");

                        if(jsonObjectNudity.getJSONArray("matches").length()>0){
                            getActivity().runOnUiThread(new Runnable() {
                                @Override
                                public void run() {
                                    progressDialog.dismiss();
                                    Toast.makeText(getActivity(), "Text is against our privacy policies.", Toast.LENGTH_SHORT).show();
                                }
                            });
                        }else {
                            getActivity().runOnUiThread(new Runnable() {
                                @Override
                                public void run() {
                                    postDetailsArrayList.clear();
                                    familyMemberWallsAdapter.clearItems();
                                    if(futureTimeInMillis!=null)
                                        wallsViewModel.hitPostApi(editTextDesc.getText().toString(),futureTimeInMillis.toString());
                                    else wallsViewModel.hitPostApi(editTextDesc.getText().toString(),null);
                                }
                            });

                        }

                    } catch (NullPointerException | JSONException e) {
                        getActivity().runOnUiThread(new Runnable() {
                            @Override
                            public void run() {
                                progressDialog.dismiss();
                            }
                        });
                        e.printStackTrace();
                    }
                } catch (IOException e) {
                    getActivity().runOnUiThread(new Runnable() {
                        @Override
                        public void run() {
                            progressDialog.dismiss();
                        }
                    });
                    e.printStackTrace();
                }
            }
        }).start();
    }
}