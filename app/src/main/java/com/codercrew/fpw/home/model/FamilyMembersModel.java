package com.codercrew.fpw.home.model;

public class FamilyMembersModel {
    private int id;
    private String name;
    private String imageUrl;
    private String status;

    public FamilyMembersModel(int id,String name,String imageUrl,String status){
        this.id = id;
        this.name = name;
        this.imageUrl = imageUrl;
        this.status = status;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getImageUrl() {
        return imageUrl;
    }

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }
}
