package com.codercrew.fpw.home.ui.profile;

import android.app.Application;
import android.content.Context;
import android.widget.Toast;

import androidx.lifecycle.AndroidViewModel;
import androidx.lifecycle.LiveData;
import androidx.lifecycle.MutableLiveData;

import com.codercrew.fpw.ApiClient.ApiClient;
import com.codercrew.fpw.ApiClient.ApiInterface;
import com.codercrew.fpw.AppContext;
import com.codercrew.fpw.Utils.SharedPrefrencesMain;
import com.codercrew.fpw.model.SignUp;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class ProfileViewModel extends AndroidViewModel {

//    private MutableLiveData<List<FamilyMembersModel>> mText;
    private MutableLiveData<SignUp> mText;
    private SharedPrefrencesMain sharedPrefrencesMain;
    private Context context;
    private SignUp model;


    public ProfileViewModel(Application application) {
        super(application);
        context = application.getApplicationContext();
        mText = new MutableLiveData<>();
        sharedPrefrencesMain = new SharedPrefrencesMain(AppContext.getAppContext());
//        hitProfileApi();

    }

    public LiveData<SignUp> getText() {
        return mText;
    }


    public void hitProfileApi()
    {
        Call<SignUp> verify;

        ApiInterface apiInterface = ApiClient.getClient().create(ApiInterface.class);
//        verify = apiInterface.doverify(code.getText().toString().trim(),"Bearer"+sharedPrefrencesMain.getToken());
        verify = apiInterface.getProfile("Bearer "+sharedPrefrencesMain.getToken(),"");

        verify.enqueue(new Callback<SignUp>() {
            @Override
            public void onResponse(Call<SignUp> call, Response<SignUp> response) {
                if (response.isSuccessful() && response.body().getStatus()) {
                    sharedPrefrencesMain.setPlan(response.body().getData().getUser().getPlan());
                    sharedPrefrencesMain.setFcount(response.body().getData().getUser().getFcount());
                    model = response.body();
                    mText.setValue(model);
//                    return model;
                }else {
                    Toast.makeText(context, ""+response.errorBody().toString(), Toast.LENGTH_SHORT).show();
                    mText.setValue(null);
                }
            }

            @Override
            public void onFailure(Call<SignUp> call, Throwable t) {
                mText.setValue(null);
                Toast.makeText(context, "fail", Toast.LENGTH_LONG).show();
            }
        });
    }

    public void hitMemberProfileApi(String email)
    {
        Call<SignUp> verify;

        ApiInterface apiInterface = ApiClient.getClient().create(ApiInterface.class);
//        verify = apiInterface.doverify(code.getText().toString().trim(),"Bearer"+sharedPrefrencesMain.getToken());
        verify = apiInterface.getmemberprofile("Bearer "+sharedPrefrencesMain.getToken(),email);

        verify.enqueue(new Callback<SignUp>() {
            @Override
            public void onResponse(Call<SignUp> call, Response<SignUp> response) {
                if (response.isSuccessful() && response.body().getStatus()) {
                    sharedPrefrencesMain.setPlan(response.body().getData().getUser().getPlan());
                    sharedPrefrencesMain.setFcount(response.body().getData().getUser().getFcount());
                    model = response.body();
                    mText.setValue(model);
//                    return model;
                }else {
                    Toast.makeText(context, ""+response.errorBody().toString(), Toast.LENGTH_SHORT).show();
                    mText.setValue(null);
                }
            }

            @Override
            public void onFailure(Call<SignUp> call, Throwable t) {
                mText.setValue(null);
                Toast.makeText(context, "fail", Toast.LENGTH_LONG).show();
            }
        });
    }

    public void hitRelationApi(String email,String rel)
    {
        Call<SignUp> verify;

        ApiInterface apiInterface = ApiClient.getClient().create(ApiInterface.class);
//        verify = apiInterface.doverify(code.getText().toString().trim(),"Bearer"+sharedPrefrencesMain.getToken());
        verify = apiInterface.changerelation("Bearer "+sharedPrefrencesMain.getToken(),email,rel);

        verify.enqueue(new Callback<SignUp>() {
            @Override
            public void onResponse(Call<SignUp> call, Response<SignUp> response) {
                if (response.isSuccessful()) {
                    hitProfileApi();
//                    return model;
                }
            }

            @Override
            public void onFailure(Call<SignUp> call, Throwable t) {
                Toast.makeText(context, "fail", Toast.LENGTH_LONG).show();
            }
        });
    }

    public void hitReportApi(String id)
    {
        Call<SignUp> verify;

        ApiInterface apiInterface = ApiClient.getClient().create(ApiInterface.class);
//        verify = apiInterface.doverify(code.getText().toString().trim(),"Bearer"+sharedPrefrencesMain.getToken());
        verify = apiInterface.reportuser("Bearer "+sharedPrefrencesMain.getToken(),id);

        verify.enqueue(new Callback<SignUp>() {
            @Override
            public void onResponse(Call<SignUp> call, Response<SignUp> response) {
                if (response.isSuccessful()) {
                    hitProfileApi();
//                    Toast.makeText(context, ""+response.body().getMessage(), Toast.LENGTH_SHORT).show();
//                    return model;
                }
            }

            @Override
            public void onFailure(Call<SignUp> call, Throwable t) {
                Toast.makeText(context, "fail", Toast.LENGTH_LONG).show();
            }
        });
    }


    public void hitDecesedApi(String id)
    {
        Call<SignUp> verify;

        ApiInterface apiInterface = ApiClient.getClient().create(ApiInterface.class);
//        verify = apiInterface.doverify(code.getText().toString().trim(),"Bearer"+sharedPrefrencesMain.getToken());
        verify = apiInterface.markdeceased("Bearer "+sharedPrefrencesMain.getToken(),id);

        verify.enqueue(new Callback<SignUp>() {
            @Override
            public void onResponse(Call<SignUp> call, Response<SignUp> response) {
                if (response.isSuccessful()) {
                    hitProfileApi();
//                    Toast.makeText(context, ""+response.body().getMessage(), Toast.LENGTH_SHORT).show();
//                    return model;
                }
            }

            @Override
            public void onFailure(Call<SignUp> call, Throwable t) {
                Toast.makeText(context, "fail", Toast.LENGTH_LONG).show();
            }
        });
    }


    public void hitBlockApi(String id)
    {
        Call<SignUp> verify;

        ApiInterface apiInterface = ApiClient.getClient().create(ApiInterface.class);
//        verify = apiInterface.doverify(code.getText().toString().trim(),"Bearer"+sharedPrefrencesMain.getToken());
        verify = apiInterface.block("Bearer "+sharedPrefrencesMain.getToken(),id);

        verify.enqueue(new Callback<SignUp>() {
            @Override
            public void onResponse(Call<SignUp> call, Response<SignUp> response) {
                if (response.isSuccessful()) {
//                    Toast.makeText(context, ""+response.body().getMessage(), Toast.LENGTH_SHORT).show();
                    hitProfileApi();
//                    return model;
                }
            }

            @Override
            public void onFailure(Call<SignUp> call, Throwable t) {
                Toast.makeText(context, "fail", Toast.LENGTH_LONG).show();
            }
        });
    }
}