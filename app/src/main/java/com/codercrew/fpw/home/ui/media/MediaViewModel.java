package com.codercrew.fpw.home.ui.media;

import android.widget.Toast;

import androidx.lifecycle.LiveData;
import androidx.lifecycle.MutableLiveData;
import androidx.lifecycle.ViewModel;

import com.codercrew.fpw.ApiClient.ApiClient;
import com.codercrew.fpw.ApiClient.ApiInterface;
import com.codercrew.fpw.AppContext;
import com.codercrew.fpw.Utils.SharedPrefrencesMain;
import com.codercrew.fpw.home.model.FamilyMembersModel;
import com.codercrew.fpw.model.GetMedia;

import java.util.ArrayList;
import java.util.List;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class MediaViewModel extends ViewModel {
    // TODO: Implement the ViewModel

    private MutableLiveData<GetMedia.Data> mText;
    private SharedPrefrencesMain sharedPrefrencesMain;

    public MediaViewModel() {
        mText = new MutableLiveData<>();
//        mText.setValue(SetListValues());
        sharedPrefrencesMain = new SharedPrefrencesMain(AppContext.getAppContext());

    }

    public LiveData<GetMedia.Data> getText() {
        return mText;
    }

    private List<FamilyMembersModel> SetListValues() {
        List<FamilyMembersModel> familyMembersModels = new ArrayList<>();
        familyMembersModels.add(new FamilyMembersModel(1, "", "", ""));
        familyMembersModels.add(new FamilyMembersModel(2, "", "", ""));
        familyMembersModels.add(new FamilyMembersModel(3, "", "", ""));
        familyMembersModels.add(new FamilyMembersModel(4, "", "", ""));
        familyMembersModels.add(new FamilyMembersModel(5, "", "", ""));
        familyMembersModels.add(new FamilyMembersModel(6, "", "", ""));
        return familyMembersModels;
    }

    public void hitGetMediaApi(String fCode) {
        Call<GetMedia> verify;

        ApiInterface apiInterface = ApiClient.getClient().create(ApiInterface.class);
        verify = apiInterface.getmedia("Bearer " + sharedPrefrencesMain.getToken(), fCode);

        verify.enqueue(new Callback<GetMedia>() {
            @Override
            public void onResponse(Call<GetMedia> call, Response<GetMedia> response) {
                if (response.isSuccessful()) {
                    mText.setValue(response.body().getData());
//                    Toast.makeText(AppContext.getAppContext(), response.body().getMessage(), Toast.LENGTH_LONG).show();

                }
            }

            @Override
            public void onFailure(Call<GetMedia> call, Throwable t) {
                mText.setValue(null);
                Toast.makeText(AppContext.getAppContext(), "fail", Toast.LENGTH_LONG).show();
            }
        });
    }

    public void hitGetMediaOfUserApi(String id) {
        Call<GetMedia> verify;
        ApiInterface apiInterface = ApiClient.getClient().create(ApiInterface.class);
        verify = apiInterface.getusermedia("Bearer " + sharedPrefrencesMain.getToken(), id);

        verify.enqueue(new Callback<GetMedia>() {
            @Override
            public void onResponse(Call<GetMedia> call, Response<GetMedia> response) {
                if (response.isSuccessful()) {
                    mText.setValue(response.body().getData());
//                    Toast.makeText(AppContext.getAppContext(), response.body().getMessage(), Toast.LENGTH_LONG).show();
                }
            }

            @Override
            public void onFailure(Call<GetMedia> call, Throwable t) {
                mText.setValue(null);
                Toast.makeText(AppContext.getAppContext(), "fail", Toast.LENGTH_LONG).show();
            }
        });
    }
}