package com.codercrew.fpw.home.ui.walls;

import android.widget.Toast;

import androidx.lifecycle.LiveData;
import androidx.lifecycle.MutableLiveData;
import androidx.lifecycle.ViewModel;

import com.codercrew.fpw.ApiClient.ApiClient;
import com.codercrew.fpw.ApiClient.ApiInterface;
import com.codercrew.fpw.AppContext;
import com.codercrew.fpw.Utils.SharedPrefrencesMain;
import com.codercrew.fpw.adapter.NewsfeedAdapter;
import com.codercrew.fpw.adapter.WallsAdapter;
import com.codercrew.fpw.model.GetPost;
import com.codercrew.fpw.model.GetPostPagination;

import java.util.ArrayList;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

import static com.codercrew.fpw.pagination.PaginationListener.PAGE_SIZE;
import static com.codercrew.fpw.pagination.PaginationListener.PAGE_START;

public class WallsViewModel extends ViewModel {

    private SharedPrefrencesMain sharedPrefrencesMain;
    private MutableLiveData<ArrayList<GetPostPagination.Data.PostDetails>> mText;
    public int currentPage = PAGE_START;
    public WallsViewModel() {
        mText = new MutableLiveData<>();
//        mText.setValue(SetListValues());
        sharedPrefrencesMain = new SharedPrefrencesMain(AppContext.getAppContext());
        hitGetPostApi1("0");
    }

    public LiveData<ArrayList<GetPostPagination.Data.PostDetails>> getText() {
        return mText;
    }



    public void hitPostApi(String text,String fdate)
    {
        Call<GetPost> verify;

        ApiInterface apiInterface = ApiClient.getClient().create(ApiInterface.class);
        verify = apiInterface.createTextPost("Bearer "+sharedPrefrencesMain.getToken(),sharedPrefrencesMain.getFmCode(),text,"0",fdate);

        verify.enqueue(new Callback<GetPost>() {
            @Override
            public void onResponse(Call<GetPost> call, Response<GetPost> response) {
                if (response.isSuccessful()) {
                    hitGetPostApi1("0");
                    Toast.makeText(AppContext.getAppContext(), response.body().getMessage(), Toast.LENGTH_LONG).show();

                }
            }
            @Override
            public void onFailure(Call<GetPost> call, Throwable t) {
                Toast.makeText(AppContext.getAppContext(), "fail", Toast.LENGTH_LONG).show();
            }
        });
    }

//    public void hitGetPostApi(String fCode)
//    {
//        Call<GetPost> verify;
//
//        ApiInterface apiInterface = ApiClient.getClient().create(ApiInterface.class);
//        verify = apiInterface.getpost("Bearer "+sharedPrefrencesMain.getToken(),fCode);
//
//        verify.enqueue(new Callback<GetPost>() {
//            @Override
//            public void onResponse(Call<GetPost> call, Response<GetPost> response) {
//                if (response.isSuccessful()) {
//                    mText.setValue(response.body().getData());
////                    Toast.makeText(AppContext.getAppContext(), response.body().getMessage(), Toast.LENGTH_LONG).show();
//
//                }
//            }
//            @Override
//            public void onFailure(Call<GetPost> call, Throwable t) {
//                mText.setValue(null);
//                Toast.makeText(AppContext.getAppContext(), "fail", Toast.LENGTH_LONG).show();
//            }
//        });
//    }

    public void hitGetPostApi1(String fCode)
    {
        Call<GetPostPagination> verify;

        ApiInterface apiInterface = ApiClient.getClient().create(ApiInterface.class);
        verify = apiInterface.getpost1("Bearer "+sharedPrefrencesMain.getToken(),fCode);

        verify.enqueue(new Callback<GetPostPagination>() {
            @Override
            public void onResponse(Call<GetPostPagination> call, Response<GetPostPagination> response) {
                if (response.isSuccessful()) {

                    if(response.body().getData().getPages().length()>0) {
                        PAGE_SIZE = response.body().getData().getPosts().size();
                        currentPage = Integer.parseInt(response.body().getData().getPages());
                    }
                    else currentPage = -1;
                    mText.setValue(response.body().getData().getPosts());
//                    Toast.makeText(AppContext.getAppContext(), response.body().getMessage(), Toast.LENGTH_LONG).show();

                }
            }
            @Override
            public void onFailure(Call<GetPostPagination> call, Throwable t) {
                mText.setValue(null);
                Toast.makeText(AppContext.getAppContext(), "fail", Toast.LENGTH_LONG).show();
            }
        });
    }

    public void hitLikeApi(String postId)
    {
        Call<GetPost> verify;

        ApiInterface apiInterface = ApiClient.getClient().create(ApiInterface.class);
        verify = apiInterface.likepost("Bearer "+sharedPrefrencesMain.getToken(),postId);

        verify.enqueue(new Callback<GetPost>() {
            @Override
            public void onResponse(Call<GetPost> call, Response<GetPost> response) {
                if (response.isSuccessful()) {
//                    hitGetPostApi1("111");
//                    Toast.makeText(AppContext.getAppContext(), response.body().getMessage(), Toast.LENGTH_LONG).show();

                }
            }
            @Override
            public void onFailure(Call<GetPost> call, Throwable t) {
                Toast.makeText(AppContext.getAppContext(), "fail", Toast.LENGTH_LONG).show();
            }
        });
    }

    public void hitCommentApi(String postId)
    {
        Call<GetPost> verify;

        ApiInterface apiInterface = ApiClient.getClient().create(ApiInterface.class);
        verify = apiInterface.addComment("Bearer "+sharedPrefrencesMain.getToken(), WallsAdapter.comment,postId);

        verify.enqueue(new Callback<GetPost>() {
            @Override
            public void onResponse(Call<GetPost> call, Response<GetPost> response) {
                WallsAdapter.comment="";
                if (response.isSuccessful()) {
//                    hitGetPostApi1("111");
//                    Toast.makeText(AppContext.getAppContext(), response.body().getMessage(), Toast.LENGTH_LONG).show();

                }
            }
            @Override
            public void onFailure(Call<GetPost> call, Throwable t) {
                WallsAdapter.comment="";
                Toast.makeText(AppContext.getAppContext(), "fail", Toast.LENGTH_LONG).show();
            }
        });
    }

    public void hitDeletePostApi(String postId)
    {
        Call<GetPost> verify;

        ApiInterface apiInterface = ApiClient.getClient().create(ApiInterface.class);
        verify = apiInterface.deletepost("Bearer "+sharedPrefrencesMain.getToken(),postId);

        verify.enqueue(new Callback<GetPost>() {
            @Override
            public void onResponse(Call<GetPost> call, Response<GetPost> response) {
                NewsfeedAdapter.comment="";
                if (response.isSuccessful()) {
//                    hitGetPostApi("111");
//                    Toast.makeText(AppContext.getAppContext(), response.body().getMessage(), Toast.LENGTH_LONG).show();

                }
            }
            @Override
            public void onFailure(Call<GetPost> call, Throwable t) {
                NewsfeedAdapter.comment="";
                Toast.makeText(AppContext.getAppContext(), "fail", Toast.LENGTH_LONG).show();
            }
        });
    }

}