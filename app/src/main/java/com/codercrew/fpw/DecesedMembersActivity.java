package com.codercrew.fpw;

import android.os.Bundle;
import android.view.View;
import android.widget.ImageView;
import android.widget.TextView;

import androidx.annotation.Nullable;
import androidx.appcompat.app.AppCompatActivity;
import androidx.lifecycle.Observer;
import androidx.lifecycle.ViewModelProvider;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import com.codercrew.fpw.adapter.BlockedFamilyMemberAdapter;
import com.codercrew.fpw.home.interfaces.OnItemClick;
import com.codercrew.fpw.model.GetUsers;
import com.codercrew.fpw.viewmodel.BlockedMemberViewModel;

public class DecesedMembersActivity extends AppCompatActivity implements OnItemClick,View.OnClickListener {

    private BlockedFamilyMemberAdapter blockedFamilyMemberAdapter;
    protected BlockedMemberViewModel blockedMemberViewModel;
    TextView textView;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_deceased_members);
        textView = findViewById(R.id.tvMessage);
        blockedMemberViewModel =
                new ViewModelProvider(this).get(BlockedMemberViewModel.class);
        final RecyclerView recyclerView = findViewById(R.id.rvBlockedMembers);
        final ImageView imageView = findViewById(R.id.imgBack);
        imageView.setOnClickListener(this);
        recyclerView.setLayoutManager(new LinearLayoutManager(DecesedMembersActivity.this,RecyclerView.VERTICAL,false));
        blockedMemberViewModel.getText().observe(this, new Observer<GetUsers>() {
            @Override
            public void onChanged(@Nullable GetUsers familyMembersModel) {
                blockedFamilyMemberAdapter = new BlockedFamilyMemberAdapter(DecesedMembersActivity.this,familyMembersModel.getData(), DecesedMembersActivity.this);
                recyclerView.setAdapter(blockedFamilyMemberAdapter);
                if(familyMembersModel.getData().size()>0) {
                    textView.setVisibility(View.GONE);
                }else textView.setVisibility(View.VISIBLE);
            }
        });
    }

    @Override
    public void onClick(View view, int pos) {

    }
    @Override
    public void onClick(View v) {
        finish();
    }

    @Override
    public void onResume(){
        super.onResume();
        blockedMemberViewModel =
                new ViewModelProvider(this).get(BlockedMemberViewModel.class);
        blockedMemberViewModel.hitDecesed("");
    }
}