package com.codercrew.fpw;

import android.app.Activity;
import android.app.ProgressDialog;
import android.content.Context;
import android.os.Bundle;
import android.view.MotionEvent;
import android.view.View;
import android.view.inputmethod.InputMethodManager;
import android.widget.EditText;
import android.widget.ImageView;

import androidx.annotation.Nullable;
import androidx.appcompat.app.AppCompatActivity;
import androidx.lifecycle.Observer;
import androidx.lifecycle.ViewModelProvider;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import com.codercrew.fpw.Utils.SharedPrefrencesMain;
import com.codercrew.fpw.adapter.CommentsAdapter;
import com.codercrew.fpw.home.interfaces.OnItemClick;
import com.codercrew.fpw.model.GetComments;
import com.codercrew.fpw.viewmodel.CommentsViewModel;
import com.bumptech.glide.Glide;

import java.util.ArrayList;

public class CommentsActivity extends AppCompatActivity implements OnItemClick,View.OnClickListener {

    private CommentsAdapter commentsAdapter;
    protected CommentsViewModel commentsViewModel;
    ProgressDialog pd;
    SharedPrefrencesMain sharedPrefrencesMain;
    ArrayList<GetComments.CommentsDetails> commentsDetailsrrayList;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_comments);

        sharedPrefrencesMain = new SharedPrefrencesMain(this);
        pd =new ProgressDialog(this);
        pd.setMessage(getString(R.string.loading));
        pd.setCancelable(false);
        pd.show();

        commentsViewModel =
                new ViewModelProvider(this).get(CommentsViewModel.class);
        final RecyclerView recyclerView = findViewById(R.id.rvBlockedMembers);
        final ImageView imageView = findViewById(R.id.imgBack);
        final EditText editTextComments = findViewById(R.id.tvName);
        final ImageView imageView1Profile = findViewById(R.id.imgProfile);
        final ImageView imageViewSend = findViewById(R.id.imgSend);


        imageView.setOnClickListener(this);
        imageViewSend.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if(editTextComments.getText().toString().isEmpty()){
                    editTextComments.setError("Enter comment");
                    editTextComments.requestFocus();
                    return;
                }
                pd.show();
                commentsViewModel.hitCommentApi(getIntent().getStringExtra("pid"),editTextComments.getText().toString());
            }
        });

        if(!sharedPrefrencesMain.getProfileUrl().isEmpty())
            Glide.with(this).load(sharedPrefrencesMain.getProfileUrl()).into(imageView1Profile);
        recyclerView.setLayoutManager(new LinearLayoutManager(CommentsActivity.this,RecyclerView.VERTICAL,false));
        commentsViewModel.getText().observe(this, new Observer<ArrayList<GetComments.CommentsDetails>>() {
            @Override
            public void onChanged(@Nullable ArrayList<GetComments.CommentsDetails> familyMembersModel) {
                pd.dismiss();

                editTextComments.setText("");
                editTextComments.setError(null);
                if(familyMembersModel!=null) {
                    commentsDetailsrrayList = familyMembersModel;
                    commentsAdapter = new CommentsAdapter(CommentsActivity.this, familyMembersModel, CommentsActivity.this);
                    recyclerView.setAdapter(commentsAdapter);
                }
            }
        });

        commentsViewModel.hitGetCommentApi(getIntent().getStringExtra("pid"));
    }

    @Override
    public void onClick(View view, int pos) {
        if(view.getId()==R.id.imgMenu){
            pd.show();
            commentsViewModel.hitDeleteCommentApi(getIntent().getStringExtra("pid"),commentsDetailsrrayList.get(pos).getId());
        }

    }
    @Override
    public void onClick(View v) {
        finish();
    }

    @Override
    public boolean dispatchTouchEvent(MotionEvent ev) {
        if (getCurrentFocus() != null) {
            InputMethodManager imm = (InputMethodManager) getSystemService(Context.INPUT_METHOD_SERVICE);
            imm.hideSoftInputFromWindow(getCurrentFocus().getWindowToken(), 0);
        }
        return super.dispatchTouchEvent(ev);
    }
}