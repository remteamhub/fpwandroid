package com.codercrew.fpw;

import androidx.annotation.Nullable;
import androidx.appcompat.app.AppCompatActivity;
import androidx.lifecycle.Observer;
import androidx.lifecycle.ViewModelProvider;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import android.content.DialogInterface;
import android.os.Bundle;
import android.view.View;
import android.widget.ImageView;
import android.widget.TextView;

import com.codercrew.fpw.adapter.BlockedFamilyMemberAdapter;
import com.codercrew.fpw.home.interfaces.OnItemClick;
import com.codercrew.fpw.home.model.FamilyMembersModel;
import com.codercrew.fpw.model.GetUsers;
import com.codercrew.fpw.model.User;
import com.codercrew.fpw.viewmodel.BlockedMemberViewModel;

import java.util.ArrayList;
import java.util.List;

public class BlockedMembersActivity extends AppCompatActivity implements OnItemClick,View.OnClickListener {

    private BlockedFamilyMemberAdapter blockedFamilyMemberAdapter;
    protected BlockedMemberViewModel blockedMemberViewModel;
    List<User> users = new ArrayList<>();
    TextView textView;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_blocked_members);
        textView = findViewById(R.id.tvMessage);

        blockedMemberViewModel =
                new ViewModelProvider(this).get(BlockedMemberViewModel.class);
        final RecyclerView recyclerView = findViewById(R.id.rvBlockedMembers);
        final ImageView imageView = findViewById(R.id.imgBack);
        imageView.setOnClickListener(this);
        recyclerView.setLayoutManager(new LinearLayoutManager(BlockedMembersActivity.this,RecyclerView.VERTICAL,false));
        blockedMemberViewModel.getText().observe(this, new Observer<GetUsers>() {
            @Override
            public void onChanged(@Nullable GetUsers familyMembersModel) {
                users = familyMembersModel.getData();
                blockedFamilyMemberAdapter = new BlockedFamilyMemberAdapter(BlockedMembersActivity.this, familyMembersModel.getData(), BlockedMembersActivity.this);
                recyclerView.setAdapter(blockedFamilyMemberAdapter);
                if(users.size()>0) {
                    textView.setVisibility(View.GONE);
                }else textView.setVisibility(View.VISIBLE);
            }
        });
    }

    @Override
    public void onClick(View view, int pos) {
        AskDialog("Unblock "+users.get(pos).getName(),users.get(pos).getId());
//        blockedMemberViewModel.hitUnblock(users.get(pos).getId());
    }
    @Override
    public void onClick(View v) {
        finish();
    }

    @Override
    public void onResume(){
        super.onResume();
        blockedMemberViewModel =
                new ViewModelProvider(this).get(BlockedMemberViewModel.class);
        blockedMemberViewModel.hitGetBlockApi("");
    }

    private void AskDialog(String title,String uid){
        android.app.AlertDialog.Builder builder1 = new android.app.AlertDialog.Builder(BlockedMembersActivity.this);

        builder1.setTitle(title);
        builder1.setMessage("Are you sure");
        builder1.setCancelable(false);

        builder1.setPositiveButton(
                "Yes",
                new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int id) {
                       blockedMemberViewModel.hitUnblock(uid);
                    }
                });
        builder1.setNegativeButton("No", new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
                dialog.cancel();
            }
        });
        builder1.show();
    }
}