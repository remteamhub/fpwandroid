package com.codercrew.fpw.ApiClient;

import android.os.Bundle;

import androidx.appcompat.app.AppCompatActivity;

import com.codercrew.fpw.R;
import com.github.barteksc.pdfviewer.PDFView;


public class PolicyActivity extends AppCompatActivity {

    PDFView pdfView;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_policy);

        pdfView=findViewById(R.id.pdfv);

        if(getIntent().getStringExtra("name").equalsIgnoreCase("policy"))
            pdfView.fromAsset("fpw.pdf").load();
        else pdfView.fromAsset("fpw_terms.pdf").load();


    }
}