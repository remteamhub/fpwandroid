package com.codercrew.fpw.ApiClient;


import com.codercrew.fpw.model.FCount;
import com.codercrew.fpw.model.Fpassword;
import com.codercrew.fpw.model.GetAlbumPhotos;
import com.codercrew.fpw.model.GetAlbums;
import com.codercrew.fpw.model.GetComments;
import com.codercrew.fpw.model.GetFamilies;
import com.codercrew.fpw.model.GetLikes;
import com.codercrew.fpw.model.GetMedia;
import com.codercrew.fpw.model.GetNotifications;
import com.codercrew.fpw.model.GetPost;
import com.codercrew.fpw.model.GetPostPagination;
import com.codercrew.fpw.model.GetProfile;
import com.codercrew.fpw.model.GetUserMedia;
import com.codercrew.fpw.model.GetUsers;
import com.codercrew.fpw.model.Resend;
import com.codercrew.fpw.model.SignUp;

import okhttp3.MultipartBody;
import okhttp3.RequestBody;
import retrofit2.Call;
import retrofit2.http.Body;
import retrofit2.http.Field;
import retrofit2.http.FormUrlEncoded;
import retrofit2.http.Header;
import retrofit2.http.Multipart;
import retrofit2.http.POST;
import retrofit2.http.Part;

public interface ApiInterface {


    @FormUrlEncoded
    @POST("signup")
    Call<SignUp> modelList1(
            @Field("name") String name,
            @Field("email") String email,
            @Field("password") String password,
            @Field("state") String state,
            @Field("zip") String zip,
            @Field("country") String country,
            @Field("family") String family,
            @Field("dob") String dob,
            @Field("familyCode") String familyCode,
            @Field("city") String city,
            @Field("gender") int gender
    );


    @FormUrlEncoded
    @POST("login")
    Call<SignUp> dologin(
            @Field("email") String email,
            @Field("password") String password
    );

    @FormUrlEncoded
    @POST("verify")
    Call<SignUp> doverify(@Header("authorization") String token,
                          @Field("code") String code
    );

    @FormUrlEncoded
    @POST("forgetpassword")
    Call<Fpassword> forgot_password(
            @Field("email") String email
    );

    @POST("resendcode")
    Call<Resend> resend(@Header("authorization") String token
    );


    @FormUrlEncoded
    @POST("profile")
    Call<SignUp> getProfile(@Header("authorization") String token,
                            @Field("code") String code
    );

    @FormUrlEncoded
    @POST("getmemberprofile")
    Call<SignUp> getmemberprofile(@Header("authorization") String token,
                            @Field("nuser") String email
    );

    @FormUrlEncoded
    @POST("getprofilebyid")
    Call<GetProfile> getprofilebyid(@Header("authorization") String token,
                                    @Field("id") String id
    );

    @FormUrlEncoded
    @POST("getprofilebyid2")
    Call<GetProfile> getprofilebyid2(@Header("authorization") String token,
                                     @Field("id") String id,
                                     @Field("page") String page
    );

    @FormUrlEncoded
    @POST("changerelation")
    Call<SignUp> changerelation(@Header("authorization") String token,
                                @Field("email") String code,
                                @Field("relation") String rel
    );

    @FormUrlEncoded
    @POST("getfamilies")
    Call<GetFamilies> getfamilies(@Header("authorization") String token,
                                  @Field("code") String code
    );

    @FormUrlEncoded
    @POST("changefamily")
    Call<SignUp> changefamily(@Header("authorization") String token,
                              @Field("familyCode") String code
    );

    @FormUrlEncoded
    @POST("addfamilybycode")
    Call<SignUp> addfamilybycode(@Header("authorization") String token,
                                 @Field("familyCode") String code
    );

    @FormUrlEncoded
    @POST("removefamily")
    Call<SignUp> removefamily(@Header("authorization") String token,
                              @Field("familyCode") String code
    );

    @FormUrlEncoded
    @POST("updateprofile")
    Call<SignUp> updateprofile(@Header("authorization") String token,
                               @Field("name") String name,
                               @Field("email") String email,
                               @Field("password") String password,
                               @Field("state") String state,
                               @Field("zip") String zip,
                               @Field("country") String country,
                               @Field("dob") String dob,
                               @Field("city") String city,
                               @Field("gender") int gender
    );


    @Multipart
    @POST("profilepic")
    Call<SignUp> updateprofile(@Header("authorization") String token,
                               @Part MultipartBody.Part file
    );


    @FormUrlEncoded
    @POST("updatepassword")
    Call<SignUp> updatepassword(@Header("authorization") String token,
                                @Field("oldpassword") String oldpassword,
                                @Field("newpassword") String newpassword
    );

    @FormUrlEncoded
    @POST("getnewsfeed")
    Call<GetPost> getnewsfeed(@Header("authorization") String token,
                              @Field("page") String code
    );

    @FormUrlEncoded
    @POST("getnewsfeed1")
    Call<GetPostPagination> getnewsfeed1(@Header("authorization") String token,
                                         @Field("page") String code
    );


    @FormUrlEncoded
    @POST("getpostbyid")
    Call<GetPost> getpost(@Header("authorization") String token,
                          @Field("postid") String code
    );


    @FormUrlEncoded
    @POST("getpost1")
    Call<GetPostPagination> getpost1(@Header("authorization") String token,
                                     @Field("page") String code
    );

    @FormUrlEncoded
    @POST("createpost")
    Call<GetPost> createTextPost(@Header("authorization") String token,
                                 @Field("familyCode") String familyCode,
                                 @Field("text") String text,
                                 @Field("type") String type,
                                 @Field("fdate") String fdate
    );


    @FormUrlEncoded
    @POST("editpost")
    Call<GetPost> editTextPost(@Header("authorization") String token,
                               @Field("postid") String postid,
                               @Field("familyCode") String familyCode,
                               @Field("text") String text,
                               @Field("type") String type
    );


    @FormUrlEncoded
    @POST("editposttext")
    Call<GetPost> editTextImagePost(@Header("authorization") String token,
                                    @Field("postid") String postid,
                                    @Field("familyCode") String familyCode,
                                    @Field("text") String text,
                                    @Field("type") String type,
                                    @Field("caption") String caption
    );

    @Multipart
    @POST("createpost")
    Call<GetPost> createImagePost(@Header("authorization") String token,
                                  @Part("familyCode") RequestBody familyCode,
                                  @Part("text") RequestBody text,
                                  @Part("type") RequestBody type,
                                  @Part("caption") RequestBody caption,
                                  @Part("fdate") RequestBody fdate,
                                  @Part MultipartBody.Part file
    );

    //    @Multipart
    @FormUrlEncoded
    @POST("createpost")
    Call<GetPost> createImagePost(@Header("authorization") String token,
                                  @Field("familyCode") RequestBody familyCode,
                                  @Field("text") RequestBody text,
                                  @Field("type") RequestBody type,
                                  @Body RequestBody file
    );

    @Multipart
    @POST("editpost")
    Call<GetPost> editImagePost(@Header("authorization") String token,
                                @Part("postid") RequestBody postid,
                                @Part("familyCode") RequestBody familyCode,
                                @Part("text") RequestBody text,
                                @Part("type") RequestBody type,
                                @Part("caption") RequestBody caption,
                                @Part MultipartBody.Part file
    );

    @FormUrlEncoded
    @POST("likepost")
    Call<GetPost> likepost(@Header("authorization") String token,
                           @Field("postid") String likepost
    );

    @FormUrlEncoded
    @POST("comment")
    Call<GetPost> addComment(@Header("authorization") String token,
                             @Field("comment") String comment,
                             @Field("postid") String likepost
    );

    @FormUrlEncoded
    @POST("getcomments")
    Call<GetComments> getcomments(@Header("authorization") String token,
                                  @Field("pid") String likepost
    );

    @FormUrlEncoded
    @POST("deletecomment")
    Call<GetComments> deletecomment(@Header("authorization") String token,
                                    @Field("cid") String likepost
    );


    @FormUrlEncoded
    @POST("getlikers")
    Call<GetLikes> getlikers(@Header("authorization") String token,
                             @Field("pid") String likepost
    );

    @FormUrlEncoded
    @POST("deletepost")
    Call<GetPost> deletepost(@Header("authorization") String token,
                             @Field("postid") String likepost
    );

    @FormUrlEncoded
    @POST("getmedia")
    Call<GetMedia> getmedia(@Header("authorization") String token,
                            @Field("code") String code
    );

    @FormUrlEncoded
    @POST("getmediaofuser")
    Call<GetMedia> getusermedia(@Header("authorization") String token,
                                @Field("uid") String user_id);


    @FormUrlEncoded
    @POST("getalbums")
    Call<GetAlbums> getalbums(@Header("authorization") String token,
                              @Field("type") String code
    );

    @FormUrlEncoded
    @POST("getalbumsofuser")
    Call<GetAlbums> getalbumsofuser(@Header("authorization") String token,
                                    @Field("type") String code, @Field("uid") String user_id
    );

    @FormUrlEncoded
    @POST("getalbum")
    Call<GetAlbumPhotos> getalbumGallery(@Header("authorization") String token,
                                         @Field("id") String id,
                                         @Field("type") String code
    );

    @Multipart
    @POST("addalbum")
    Call<GetAlbumPhotos> addalbum(@Header("authorization") String token,
                                  @Part("albumname") RequestBody text,
                                  @Part("type") RequestBody type,
                                  @Part("caption") RequestBody cap,
                                  @Part MultipartBody.Part file
    );

    @Multipart
    @POST("add")
    Call<GetAlbumPhotos> updateAlbum(@Header("authorization") String token,
                                     @Part("albumid") RequestBody text,
                                     @Part("type") RequestBody type,
                                     @Part("caption") RequestBody cap,
                                     @Part MultipartBody.Part file
    );

    @FormUrlEncoded
    @POST("deletepv")
    Call<GetAlbumPhotos> deletepv(@Header("authorization") String token,
                                  @Field("vpid") String id,
                                  @Field("type") String code
    );

    @FormUrlEncoded
    @POST("deletealbum")
    Call<GetAlbumPhotos> deletealbum(@Header("authorization") String token,
                                     @Field("id") String id,
                                     @Field("type") String code
    );


    @FormUrlEncoded
    @POST("fcmToken")
    Call<GetPost> fcmTokem(@Header("authorization") String token,
                           @Field("token") String postid
    );



    @FormUrlEncoded
    @POST("getnewtoken")
    Call<SignUp> getnewtoken(@Field("email") String postid);


    @FormUrlEncoded
    @POST("birthday")
    Call<GetPost> birthday(@Header("authorization") String token,
                           @Field("text") String text,
                           @Field("familyCode") String familyCode,
                           @Field("userid") String userid
    );


    @FormUrlEncoded
    @POST("getnotifications")
    Call<GetNotifications> getnotifications(@Header("authorization") String token,
                                            @Field("pid") String likepost
    );


    @FormUrlEncoded
    @POST("sendnotification")
    Call<GetNotifications> sendnotification(@Header("authorization") String token,
                                            @Field("uid") String likepost
    );


    @FormUrlEncoded
    @POST("delnotification")
    Call<GetNotifications> delnotification(@Header("authorization") String token,
                                           @Field("id") String likepost
    );


    @FormUrlEncoded
    @POST("markdeceased")
    Call<SignUp> markdeceased(@Header("authorization") String token,
                              @Field("id") String code
    );


    @FormUrlEncoded
    @POST("block")
    Call<SignUp> block(@Header("authorization") String token,
                       @Field("id") String code
    );


    @FormUrlEncoded
    @POST("reportuser")
    Call<SignUp> reportuser(@Header("authorization") String token,
                            @Field("id") String code
    );


    @FormUrlEncoded
    @POST("getdeceased")
    Call<GetUsers> getdeceased(@Header("authorization") String token,
                               @Field("id") String code
    );


    @FormUrlEncoded
    @POST("getreporters")
    Call<GetUsers> getreporters(@Header("authorization") String token,
                                @Field("id") String code
    );


    @FormUrlEncoded
    @POST("getblocked")
    Call<GetUsers> getblockers(@Header("authorization") String token,
                               @Field("id") String code
    );

    @FormUrlEncoded
    @POST("unblock")
    Call<GetUsers> unblock(@Header("authorization") String token,
                           @Field("id") String code
    );

    @FormUrlEncoded
    @POST("contactus")
    Call<GetUsers> contactus(@Header("authorization") String token,
                             @Field("message") String code
    );

    @FormUrlEncoded
    @POST("canmessage")
    Call<GetProfile> canmessage(@Header("authorization") String token,
                                @Field("user_id") String id
    );

    @FormUrlEncoded
    @POST("purchaseplan")
    Call<SignUp> purchaseplan(@Header("authorization") String token,
                                @Field("plan") String plan,@Field("payment") String payment,
                                  @Field("amount") String amount
    );

    @FormUrlEncoded
    @POST("decreasecount")
    Call<FCount> decreasecount(@Header("authorization") String token,
                               @Field("plan") String plan
    );

    @FormUrlEncoded
    @POST("getcount")
    Call<FCount> getcount(@Header("authorization") String token,
                               @Field("plan") String plan
    );


//    @Multipart
//    @POST("signup")
//    Call<String> createsignup(@Part("fname") RequestBody familyCode,
//                                  @Part("lname") RequestBody text,
//                                  @Part("type") RequestBody type,
//                               @Part("email") RequestBody email,
//                               @Part("password") RequestBody password,
//                               @Part("phoneNumber") RequestBody phoneNumber,
//                                  @Part MultipartBody.Part file
//    );
}

