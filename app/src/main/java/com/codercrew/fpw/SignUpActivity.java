package com.codercrew.fpw;

import androidx.appcompat.app.AppCompatActivity;
import androidx.constraintlayout.widget.ConstraintLayout;

import android.app.Activity;
import android.app.AlertDialog;
import android.app.DatePickerDialog;
import android.app.ProgressDialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.os.Bundle;
import android.text.Editable;
import android.text.TextWatcher;
import android.view.View;
import android.view.inputmethod.InputMethodManager;
import android.widget.Button;
import android.widget.CheckBox;
import android.widget.CompoundButton;
import android.widget.DatePicker;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.RadioButton;
import android.widget.RadioGroup;
import android.widget.TextView;
import android.widget.Toast;


import com.chivorn.datetimeoptionspicker.DateTimePickerView;
import com.codercrew.fpw.ApiClient.ApiClient;
import com.codercrew.fpw.ApiClient.ApiInterface;
import com.codercrew.fpw.ApiClient.PolicyActivity;
import com.codercrew.fpw.Utils.SharedPrefrencesMain;
import com.codercrew.fpw.Utils.utils;
import com.codercrew.fpw.model.SignUp;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class SignUpActivity extends AppCompatActivity {
    public static SignUpActivity signUpActivity;
    ConstraintLayout constraintLayout;
    CheckBox checkBox;
    EditText fName,lName,emailAddress,password,cPassword,dOB,fCode,fLabel;
    Button createAccount;
    private int mYear, mMonth, mDay;
    ImageView infoIcon;
    Boolean checked = false;
    TextView policy,termsAnd,conditions;
    int genderSelected =2;
    SharedPrefrencesMain sharedPrefrencesMain;
    ProgressDialog pd;
    private String familyCode = "";
    private String familyLabel = "";
    private boolean isTermsClick = false;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_sign_up);
        signUpActivity = this;
        sharedPrefrencesMain = SharedPrefrencesMain.getInstance(this);

        fName = findViewById(R.id.firstName);
        lName = findViewById(R.id.lastName);
        emailAddress = findViewById(R.id.emailAddress);
        password = findViewById(R.id.password);
        cPassword = findViewById(R.id.cnfrmPassword);
        dOB = findViewById(R.id.dateOfBirth);
        fCode = findViewById(R.id.familyCode);
        fLabel = findViewById(R.id.familyLabel);
        createAccount = findViewById(R.id.createAccount);
        infoIcon = findViewById(R.id.infoIcon);
        checkBox = findViewById(R.id.checkboxSignUp);
        constraintLayout = findViewById(R.id.constraintSignUp);
        policy = findViewById(R.id.policy);
        termsAnd = findViewById(R.id.termsAnd);
        conditions = findViewById(R.id.conditions);
        fCode.setVisibility(View.GONE);
        pd =new ProgressDialog(SignUpActivity.this);
        pd.setMessage(getString(R.string.loading));


        checkBox.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {
                if(checkBox.isChecked()){
                    checked = true;
                    fCode.setVisibility(View.VISIBLE);
                    fLabel.setVisibility(View.GONE);
                }
                if(!checkBox.isChecked())
                {
                    checked = false;
                    fCode.setVisibility(View.GONE);
                    fLabel.setVisibility(View.VISIBLE);
                }
            }
        });

        policy.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                isTermsClick = true;
                Intent intent = new Intent(SignUpActivity.this, PolicyActivity.class);
                intent.putExtra("name","policy");
                startActivity(intent);
            }
        });

        termsAnd.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                isTermsClick = true;
                Intent intent = new Intent(SignUpActivity.this, PolicyActivity.class);
                intent.putExtra("name","terms");
                startActivity(intent);
            }
        });
        conditions.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                isTermsClick = true;
                Intent intent = new Intent(SignUpActivity.this, PolicyActivity.class);
                intent.putExtra("name","terms");
                startActivity(intent);
            }
        });


        createAccount.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                SetValidations();
            }
        });

        infoIcon.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                details();
            }
        });

        constraintLayout.setOnFocusChangeListener(new View.OnFocusChangeListener() {
            @Override
            public void onFocusChange(View v, boolean hasFocus) {
                if (hasFocus) {
                    hideKeyboard(v);
                }
            }
        });

        dOB.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
//                DateTimePickerView dateTimePickerView = new DateTimePickerView.Builder(SignUpActivity.this, new DateTimePickerView.OnTimeSelectListener() {
//                    @Override
//                    public void onTimeSelect(Date date,View v) {//Callback
//                        //yourTextView.setText(getTime(date));
//                        int dayOfMonth = date.getDay();
//                        int monthOfYear = date.getMonth();
//                        int year = date.getYear();
//                        dOB.setText(getDate(dayOfMonth + "-" + (monthOfYear + 1) + "-" + year));
//                    }
//                })
//                .build();
//                dateTimePickerView.show();
                datePickerDialogue();
            }
        });

        emailAddress.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {

            }

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {
                String str = s.toString();
                if(str.length() > 0 && str.contains(" "))
                {
//                    emailAddress.setError("Space is not allowed");
                    emailAddress.setText(s.toString().replace(" ",""));
                    emailAddress.setSelection(emailAddress.getText().length());
                }
            }

            @Override
            public void afterTextChanged(Editable s) {

            }
        });

        password.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {

            }

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {
                String str = s.toString();
                if(str.length() > 0 && str.contains(" "))
                {
                    password.setError("Space is not allowed");
                    password.setText(s.toString().trim());
                }

            }

            @Override
            public void afterTextChanged(Editable s) {

            }
        });

        cPassword.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {

            }

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {

                String str = s.toString();
                if(str.length() > 0 && str.contains(" "))
                {
                    cPassword.setError("Space is not allowed");
                    cPassword.setText(s.toString().trim());
                }
            }

            @Override
            public void afterTextChanged(Editable s) {

            }
        });

        RadioGroup radioGroup = findViewById(R.id.radioGroup);
        RadioButton radioButton1 = findViewById(R.id.male);
        RadioButton radioButton2 = findViewById(R.id.female);



        radioButton1.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                genderSelected = 1;
            }
        });

        radioButton2.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                genderSelected = 0;
            }
        });


    }
    public void details() {
        AlertDialog.Builder builder1 = new AlertDialog.Builder(SignUpActivity.this);
        builder1.setMessage("A family code is a code you will receive automatically after registering or when joining an existing group. You will need to provide a code to join that particular group.\n" +
                " \n " +
                "A family label is a title or nickname you will create for any particular family group you belong to.");
        builder1.setCancelable(true);

        builder1.setPositiveButton(
                "OK",
                new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int id) {
                        dialog.cancel();
                    }
                });
        builder1.show();
    }

    public void datePickerDialogue() {
        // Get Current Date
        final Calendar c = Calendar.getInstance();
        mYear = c.get(Calendar.YEAR);
        mMonth = c.get(Calendar.MONTH);
        mDay = c.get(Calendar.DAY_OF_MONTH);
        DatePickerDialog datePickerDialog = new DatePickerDialog(this,
                new DatePickerDialog.OnDateSetListener() {
                    @Override
                    public void onDateSet(DatePicker view, int year,
                                          int monthOfYear, int dayOfMonth) {

                        dOB.setText(getDate(dayOfMonth + "-" + (monthOfYear + 1) + "-" + year));
//                        datePickerDialog.getDatePicker().setMaxDate(System.currentTimeMillis());
                    }

                }, mYear, mMonth, mDay);
        datePickerDialog.getDatePicker().setMaxDate(System.currentTimeMillis());
        datePickerDialog.show();
    }

    public String getDate(String dte){
        String s = dte;
        SimpleDateFormat sdf = new SimpleDateFormat("dd-MM-yyyy");

        try {
            Date d = sdf.parse(s);
            SimpleDateFormat sdf2 = new SimpleDateFormat("dd MMMM yyyy");
            s = sdf2.format(d);
            return s;
        } catch (ParseException e) {
            e.printStackTrace();
        }
        return s;
    }

    private void SetValidations() {
        if(!utils.CheckInternet(getApplicationContext())){
            Toast.makeText(this, "Check your internet connection!", Toast.LENGTH_SHORT).show();
            return;
        }
        String firstNamePattern = "[a-zA-Z]+[ ]*";
        String lastNamePattern = "[a-zA-Z]+[ ]*+[a-zA-Z]*+[ ]*";
        String emailPattern = "[a-zA-Z0-9._-]+@[a-zA-Z]+\\.+[a-zA-Z]+[ ]*";
        String passwordPattern = "^(?=.*[0-9])(?=.*[A-Z])(?=.*[a-z])(?=.*[@#$%^&+=!])(?=\\S+$).{8,}$";
        //First Name Validations
        if(fName.getText().toString().isEmpty()) {
            fName.setError("enter first name");
            fName.requestFocus();
            return;
        }else
        if(!fName.getText().toString().matches(firstNamePattern)){
            fName.setError("Invalid first name");
            fName.requestFocus();
            return;
        }
        //Last Name Validations
        if(lName.getText().toString().isEmpty()) {
            lName.setError("enter last name");
            lName.requestFocus();
            return;
        }else
        if(!lName.getText().toString().matches(lastNamePattern)){
            lName.setError("Invalid last name");
            lName.requestFocus();
            return;
        }
        //Email Validations
        if(emailAddress.getText().toString().isEmpty()) {
            emailAddress.setError("enter email address");
            emailAddress.requestFocus();
            return;
        }else
        if(!emailAddress.getText().toString().matches(emailPattern)){
            emailAddress.setError("Invalid email address");
            emailAddress.requestFocus();
            return;
        }
        //Password Validations
        if(password.getText().toString().isEmpty()) {
            password.setError("enter password");
            password.requestFocus();
            return;
        }
        else
        if(!password.getText().toString().matches(passwordPattern)) {
            password.setError("Invalid password");
            password.requestFocus();
            Toast.makeText(getApplicationContext(), "Be at least 8 characters long\n" +
                    "Include at least: 1 upper case letter, 1 lower case letter, 1 number and 1 symbol", Toast.LENGTH_SHORT).show();
            return;
        }
        else
        if(!cPassword.getText().toString().equals(password.getText().toString())) {
            cPassword.setError("Password not match");
            cPassword.requestFocus();
            return;
        }
        //DOB Validations
        if(dOB.getText().toString().isEmpty()) {
            dOB.setError("enter date of birth");
            dOB.requestFocus();
            return;
        }

        if(checked){
            if(fCode.getText().toString().isEmpty()){
                fCode.setError("enter family code");
                fCode.requestFocus();
                return;
            }
        }else {
            //Family Label Validations
            if(fLabel.getText().toString().isEmpty()) {
                fLabel.setError("enter family label");
                fLabel.requestFocus();
                return;
            }
        }

        if(genderSelected==2){
            Toast.makeText(getApplicationContext(), "Select gender!", Toast.LENGTH_SHORT).show();
            return;
        }
        if(!isTermsClick){
            Toast.makeText(getApplicationContext(), "Check our terms and conditions to proceed further!", Toast.LENGTH_SHORT).show();
            return;
        }


        if(emailAddress.getText().toString().matches(emailPattern) && password.getText().toString().matches(passwordPattern)
                && fName.getText().toString().matches(firstNamePattern) && lName.getText().toString().matches(lastNamePattern)
        )
        {
            if(checked) {
                familyCode = fCode.getText().toString().trim();
                familyLabel = "";
            }else {
                familyLabel = fLabel.getText().toString().trim();
                familyCode = "";
            }
            hitLoginApi();
        }
    }
    private void hitLoginApi() {
        pd.show();

        Call<SignUp> signup;

        ApiInterface apiInterface = ApiClient.getClient().create(ApiInterface.class);

        signup = apiInterface.modelList1(fName.getText().toString()+" "+lName.getText().toString(),emailAddress.getText().toString().trim(),
                password.getText().toString(),"","","",familyLabel,dOB.getText().toString().trim(),
                familyCode,"",genderSelected
        );


        signup.enqueue(new Callback<SignUp>() {
            @Override
            public void onResponse(Call<SignUp> call, Response<SignUp> response) {
                if (response.isSuccessful()) {
                    pd.dismiss();
                    SignUp model = response.body();
                    if(model!=null)
                        Toast.makeText(SignUpActivity.this, ""+model.getMessage(), Toast.LENGTH_SHORT).show();



                    if (model != null && model.getData()!=null) {
                        pd.dismiss();

                        sharedPrefrencesMain.setToken(model.getData().getToken());
                        sharedPrefrencesMain.setLoginStep("one");
                        sharedPrefrencesMain.setEmail(emailAddress.getText().toString().trim());
                        Intent intent = new Intent(SignUpActivity.this,EmailVerificationActivity.class);
                        intent.putExtra("email",emailAddress.getText().toString().trim());
                        startActivity(intent);
                    }
                }
            }
            @Override
            public void onFailure(Call<SignUp> call, Throwable t) {
                pd.dismiss();

                Toast.makeText(SignUpActivity.this, ""+t.getMessage(), Toast.LENGTH_LONG).show();
            }
        });
    }
    public void onRadioButtonClick(View view) {

    }
    public void hideKeyboard(View view) {
        InputMethodManager inputMethodManager =(InputMethodManager)getSystemService(
                Activity.INPUT_METHOD_SERVICE);
        inputMethodManager.hideSoftInputFromWindow(view.getWindowToken(), 0);
    }
    @Override
    public void onBackPressed() {
        super.onBackPressed();
        finish();
    }

}