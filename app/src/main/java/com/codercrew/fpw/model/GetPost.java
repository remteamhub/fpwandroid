
package com.codercrew.fpw.model;


import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.io.Serializable;
import java.util.ArrayList;

public class GetPost {

    @SerializedName("status")
    @Expose
    private Boolean status;

    @SerializedName("message")
    @Expose
    private String message;

    @SerializedName("data")
    @Expose
    private ArrayList<PostDetails> data;

    public Boolean getStatus() {
        return status;
    }

    public void setStatus(Boolean status) {
        this.status = status;
    }

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }

    public ArrayList<PostDetails> getData() {
        return data;
    }

    public void setData(ArrayList<PostDetails> data) {
        this.data = data;
    }



    public class PostDetails implements Serializable {
        @SerializedName("createdAt")
        @Expose
        private String  createdAt;

        @SerializedName("updatedAt")
        @Expose
        private String updatedAt;

        @SerializedName("id")
        @Expose
        private String id;

        @SerializedName("posterid")
        @Expose
        private String posterid;

        @SerializedName("postername")
        @Expose
        private String postername;

        @SerializedName("commentcount")
        @Expose
        private String commentcount;

        @SerializedName("posterimage")
        @Expose
        private String posterimage;

        @SerializedName("isliked")
        @Expose
        private String isliked;

        @SerializedName("text")
        @Expose
        private String text;

        @SerializedName("video")
        @Expose
        private String  video;

        @SerializedName("image")
        @Expose
        private String image;

        @SerializedName("likecount")
        @Expose
        private String likecount;

        @SerializedName("type")
        @Expose
        private String type;

        @SerializedName("thumb")
        @Expose
        private String thumb;

        @SerializedName("familyCode")
        @Expose
        private String familyCode;

        @SerializedName("caption")
        @Expose
        private String caption;

        public String getCreatedAt() {
            return createdAt;
        }

        public void setCreatedAt(String createdAt) {
            this.createdAt = createdAt;
        }

        public String getUpdatedAt() {
            return updatedAt;
        }

        public void setUpdatedAt(String updatedAt) {
            this.updatedAt = updatedAt;
        }

        public String getPosterid() {
            return posterid;
        }

        public void setPosterid(String posterid) {
            this.posterid = posterid;
        }

        public String getText() {
            return text;
        }

        public void setText(String text) {
            this.text = text;
        }

        public String getVideo() {
            return video;
        }

        public void setVideo(String video) {
            this.video = video;
        }

        public String getImage() {
            return image;
        }

        public void setImage(String image) {
            this.image = image;
        }

        public String getLikecount() {
            return likecount;
        }

        public void setLikecount(String likecount) {
            this.likecount = likecount;
        }

        public String getType() {
            return type;
        }

        public void setType(String type) {
            this.type = type;
        }

        public String getFamilyCode() {
            return familyCode;
        }

        public void setFamilyCode(String familyCode) {
            this.familyCode = familyCode;
        }

        public String getPostername() {
            return postername;
        }

        public void setPostername(String postername) {
            this.postername = postername;
        }

        public String getCommentcount() {
            return commentcount;
        }

        public void setCommentcount(String commentcount) {
            this.commentcount = commentcount;
        }

        public String getPosterimage() {
            return posterimage;
        }

        public void setPosterimage(String posterimage) {
            this.posterimage = posterimage;
        }

        public String getIsliked() {
            return isliked;
        }

        public void setIsliked(String isliked) {
            this.isliked = isliked;
        }

        public String getId() {
            return id;
        }

        public void setId(String id) {
            this.id = id;
        }

        public String getThumb() {
            return thumb;
        }

        public void setThumb(String thumb) {
            this.thumb = thumb;
        }

        public String getCaption() {
            return caption;
        }

        public void setCaption(String caption) {
            this.caption = caption;
        }
    }

}
