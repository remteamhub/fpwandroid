package com.codercrew.fpw;

import androidx.annotation.Nullable;
import androidx.appcompat.app.AppCompatActivity;
import androidx.appcompat.widget.PopupMenu;
import androidx.lifecycle.Observer;
import androidx.lifecycle.ViewModelProvider;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;
import androidx.swiperefreshlayout.widget.SwipeRefreshLayout;

import android.app.ProgressDialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.graphics.drawable.Drawable;
import android.os.Bundle;
import android.os.Handler;
import android.view.MenuItem;
import android.view.View;
import android.view.WindowManager;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.bumptech.glide.load.DataSource;
import com.bumptech.glide.load.engine.GlideException;
import com.bumptech.glide.request.RequestListener;
import com.bumptech.glide.request.target.Target;
import com.codercrew.fpw.Utils.SharedPrefrencesMain;
import com.codercrew.fpw.adapter.FamilyMemberWallAdapter;
import com.codercrew.fpw.adapter.NewsfeedAdapter;
import com.codercrew.fpw.home.MainActivity;
import com.codercrew.fpw.home.interfaces.OnItemClick;
import com.codercrew.fpw.home.ui.newsfeed.NewsfeedFragment;
import com.codercrew.fpw.home.ui.profile.ProfileFragment;
import com.codercrew.fpw.home.ui.profile.ProfileViewModel;
import com.codercrew.fpw.home.ui.profile.adapter.FamilyMembersAdapter;
import com.codercrew.fpw.model.GetPostPagination;
import com.codercrew.fpw.model.GetProfile;
import com.codercrew.fpw.model.SignUp;
import com.codercrew.fpw.model.User;
import com.codercrew.fpw.pagination.PaginationListener;
import com.codercrew.fpw.viewmodel.MemberProfileViewModel;
import com.bumptech.glide.Glide;

import java.util.ArrayList;
import java.util.List;

import static com.codercrew.fpw.pagination.PaginationListener.PAGE_SIZE;
import static com.codercrew.fpw.pagination.PaginationListener.PAGE_START;

public class MemberProfileActivity extends AppCompatActivity implements OnItemClick,View.OnClickListener {
    private ProfileViewModel profileViewModel;
    private FamilyMembersAdapter familyMembersAdapter;
    ProgressDialog pd;
    private List<User> user = new ArrayList<>();
    private SharedPrefrencesMain sharedPrefrencesMain;
    RecyclerView recyclerView;
    User model;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.fragment_profile);

        getWindow().setSoftInputMode(WindowManager.LayoutParams.SOFT_INPUT_ADJUST_PAN);

        //receive
        model = (User) getIntent().getSerializableExtra("user");

        sharedPrefrencesMain = new SharedPrefrencesMain(getApplicationContext());
        pd = new ProgressDialog(MemberProfileActivity.this);
        pd.setCancelable(false);
        pd.setMessage(getString(R.string.loading));
        final ImageView imageViewEdit = findViewById(R.id.imgEdit);
        final ImageView imageViewProfile = findViewById(R.id.imgProfile);
        final TextView textViewName = findViewById(R.id.tvName);
        final TextView textViewBd = findViewById(R.id.tvBd);
        final TextView textViewAdd = findViewById(R.id.tvAddress);
        recyclerView = findViewById(R.id.rvFamilyMembers);
        imageViewEdit.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(getApplicationContext(), EditProfileActivity.class);
                startActivity(intent);
            }
        });


//        if (sharedPrefrencesMain.getName().length() > 0) {
//            String str = sharedPrefrencesMain.getName();
//            String nameCap = str.substring(0, 1).toUpperCase() + str.substring(1);
//            textViewName.setText(nameCap);
//        }

        LinearLayoutManager layoutManager = new LinearLayoutManager(getApplicationContext(), RecyclerView.VERTICAL, false);
//        layoutManager.setStackFromEnd(true);
        recyclerView.setLayoutManager(layoutManager);
        recyclerView.addOnLayoutChangeListener((view, left, top, right, bottom, oldLeft, oldTop, oldRight, oldBottom) -> {
            if (bottom < oldBottom) {
                recyclerView.scrollBy(0, oldBottom - bottom);
            }
        });
        recyclerView.setOnClickListener(this);

        profileViewModel =
                new ViewModelProvider(this).get(ProfileViewModel.class);

        if(!pd.isShowing())
            pd.show();

        new Handler().postDelayed(new Runnable() {
            @Override
            public void run() {
                pd.dismiss();
            }
        },2000);

        profileViewModel.getText().observe(this, new Observer<SignUp>() {
            @Override
            public void onChanged(@Nullable SignUp familyMembersModel) {
                pd.dismiss();
                pd.cancel();
                if (familyMembersModel != null && familyMembersModel.getData() != null) {
                    pd.dismiss();
                    pd.cancel();


                    if (familyMembersModel.getData().getUser().getProfilepic().length() > 0) {
                        Glide.with(AppContext.getAppContext()).load(familyMembersModel.getData().getUser().getProfilepic()).listener(new RequestListener<Drawable>() {
                            @Override
                            public boolean onLoadFailed(@Nullable GlideException e, Object model, Target<Drawable> target, boolean isFirstResource) {
                                pd.dismiss();
                                return false;
                            }

                            @Override
                            public boolean onResourceReady(Drawable resource, Object model, Target<Drawable> target, DataSource dataSource, boolean isFirstResource) {
                                imageViewProfile.setImageResource(R.drawable.user_profile);
                                pd.dismiss();
                                return false;
                            }
                        }).into(imageViewProfile);
                    } else pd.dismiss();

                    String str = familyMembersModel.getData().getUser().getName();
                    String nameCap = str.substring(0, 1).toUpperCase() + str.substring(1);

                    textViewName.setText(nameCap);
                    textViewBd.setText(familyMembersModel.getData().getUser().getDob());
                    if (familyMembersModel.getData().getUser().getCity().length() > 0)
                        textViewAdd.setText(familyMembersModel.getData().getUser().getCity() + "," + familyMembersModel.getData().getUser().getCountry());
                    if (familyMembersModel.getData().getMembers() != null && familyMembersModel.getData().getMembers().size() > 0) {
                        user.clear();
                        for (User use : familyMembersModel.getData().getMembers()) {
                            if (use.getBlocked().equals("0") && use.getDeceased().equals("0"))
                                user.add(use);
                        }

                        //user = familyMembersModel.getData().getMembers();
                        familyMembersAdapter = new FamilyMembersAdapter(getApplicationContext(), user,true, MemberProfileActivity.this);
                        recyclerView.setAdapter(familyMembersAdapter);
                    }
                }
            }
        });

    }

    @Override
    public void onClick(View view, int pos) {
    }

    @Override
    public void onClick(View v) {
        finish();
    }

    @Override
    public void onResume() {
        super.onResume();
        profileViewModel =
                new ViewModelProvider(this).get(ProfileViewModel.class);
        profileViewModel.hitMemberProfileApi(model.getEmail());
    }

}