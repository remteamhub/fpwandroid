package com.codercrew.fpw;

import android.app.ProgressDialog;
import android.os.Bundle;
import android.view.View;
import android.widget.ImageView;

import androidx.annotation.Nullable;
import androidx.appcompat.app.AppCompatActivity;
import androidx.lifecycle.Observer;
import androidx.lifecycle.ViewModelProvider;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import com.codercrew.fpw.Utils.SharedPrefrencesMain;
import com.codercrew.fpw.adapter.LikesAdapter;
import com.codercrew.fpw.home.interfaces.OnItemClick;
import com.codercrew.fpw.model.GetLikes;
import com.codercrew.fpw.viewmodel.LikesViewModel;

import java.util.ArrayList;

public class LikesActivity extends AppCompatActivity implements OnItemClick,View.OnClickListener {

    private LikesAdapter likesAdapter;
    protected LikesViewModel likesViewModel;
    ProgressDialog pd;
    SharedPrefrencesMain sharedPrefrencesMain;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_likes);

        sharedPrefrencesMain = new SharedPrefrencesMain(this);
        pd =new ProgressDialog(this);
        pd.setMessage(getString(R.string.loading));
        pd.show();

        likesViewModel =
                new ViewModelProvider(this).get(LikesViewModel.class);
        final RecyclerView recyclerView = findViewById(R.id.rvBlockedMembers);
        final ImageView imageView = findViewById(R.id.imgBack);
        imageView.setOnClickListener(this);
        recyclerView.setLayoutManager(new LinearLayoutManager(LikesActivity.this,RecyclerView.VERTICAL,false));
        likesViewModel.getText().observe(this, new Observer<ArrayList<GetLikes.LikesDetails>>() {
            @Override
            public void onChanged(@Nullable ArrayList<GetLikes.LikesDetails> familyMembersModel) {
                pd.dismiss();
                if(familyMembersModel!=null) {
                    likesAdapter = new LikesAdapter(LikesActivity.this, familyMembersModel, LikesActivity.this);
                    recyclerView.setAdapter(likesAdapter);
                }
            }
        });

        likesViewModel.hitGetLikesApi(getIntent().getStringExtra("pid"));
    }

    @Override
    public void onClick(View view, int pos) {

    }
    @Override
    public void onClick(View v) {
        finish();
    }
}