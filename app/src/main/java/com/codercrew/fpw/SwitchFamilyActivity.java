package com.codercrew.fpw;

import androidx.annotation.Nullable;
import androidx.appcompat.app.AppCompatActivity;
import androidx.lifecycle.Observer;
import androidx.lifecycle.ViewModelProvider;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import android.app.AlertDialog;
import android.content.DialogInterface;
import android.os.Build;
import android.os.Bundle;
import android.view.View;
import android.widget.ImageView;
import android.widget.TextView;

import com.codercrew.fpw.Utils.SharedPrefrencesMain;
import com.codercrew.fpw.adapter.SwitchFamilyAdapter;
import com.codercrew.fpw.home.interfaces.OnItemClick;
import com.codercrew.fpw.home.model.FamilyModel;
import com.codercrew.fpw.viewmodel.SwitchFamilyViewModel;

import java.util.ArrayList;
import java.util.List;

public class SwitchFamilyActivity extends AppCompatActivity implements OnItemClick, View.OnClickListener{
    public static SwitchFamilyActivity switchFamilyActivity;
    private SwitchFamilyAdapter switchFamilyAdapter;
    private SwitchFamilyViewModel switchFamilyViewModel;
    List<FamilyModel> familyModels = new ArrayList<>();
    private String type,title;
    SharedPrefrencesMain sharedPrefrencesMain;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_switch_family);
        switchFamilyActivity = this;
        sharedPrefrencesMain = new SharedPrefrencesMain(this);
        type = getIntent().getStringExtra("Type");
        switchFamilyViewModel =
                new ViewModelProvider(this).get(SwitchFamilyViewModel.class);

        final RecyclerView recyclerView = findViewById(R.id.rvFamilies);
        final ImageView imageView = findViewById(R.id.imgBack);
        final TextView textViewTitle = findViewById(R.id.tvTitle);

        if(type.equalsIgnoreCase("Switch"))
            title = "Switch Family";
        else title = "Unjoin Family";

        textViewTitle.setText(title);
        imageView.setOnClickListener(this);

        recyclerView.setLayoutManager(new LinearLayoutManager(SwitchFamilyActivity.this,RecyclerView.VERTICAL,false));
        switchFamilyViewModel.getText().observe(this, new Observer<List<FamilyModel>>() {
            @Override
            public void onChanged(@Nullable List<FamilyModel> familyMembersModel) {
                familyModels = familyMembersModel;

                for( int i = 0; i < familyModels.size(); i++ ){
                    FamilyModel state = familyModels.get(i);
                    if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.N) {
                        if(state.getFamilyCode().equals(sharedPrefrencesMain.getFmCode())){
//                        if(familyModels.stream().filter(o -> o.getFamilyCode().equals(sharedPrefrencesMain.getFmCode())).findFirst().isPresent()){
                            familyModels.remove(state);
                            i--;
                        }
                    }
                }

                switchFamilyAdapter = new SwitchFamilyAdapter(SwitchFamilyActivity.this,familyModels, SwitchFamilyActivity.this);
                recyclerView.setAdapter(switchFamilyAdapter);
            }
        });
    }
    @Override
    public void onClick(View view, int pos) {

        if(sharedPrefrencesMain.getFmCode().equals(familyModels.get(pos).getFamilyCode())){
            AlertDialog.Builder builder1 = new AlertDialog.Builder(SwitchFamilyActivity.this);
            builder1.setTitle(title);
            if (type.equalsIgnoreCase("Switch"))
                builder1.setMessage("Family already selected!");
            else builder1.setMessage("Current family can't be removed!");
            builder1.setCancelable(true);


            builder1.setPositiveButton(
                    "ok",
                    new DialogInterface.OnClickListener() {
                        public void onClick(DialogInterface dialog, int id) {
                            dialog.cancel();
                        }
                    });
            builder1.show();
        }else {
            AlertDialog.Builder builder1 = new AlertDialog.Builder(SwitchFamilyActivity.this);
            builder1.setTitle(title);
            builder1.setMessage("Are you sure");
            builder1.setCancelable(true);

            builder1.setPositiveButton(
                    "Yes",
                    new DialogInterface.OnClickListener() {
                        public void onClick(DialogInterface dialog, int id) {
                            if (type.equalsIgnoreCase("Switch"))
                                switchFamilyViewModel.hitChangeFamilyApi(familyModels.get(pos).getFamilyCode());
                            else {
                                switchFamilyViewModel.hitUnJoinFamilyApi(familyModels.get(pos).getFamilyCode());
                            }
                            dialog.cancel();
                        }
                    });
            builder1.setNegativeButton("No", new DialogInterface.OnClickListener() {
                @Override
                public void onClick(DialogInterface dialog, int which) {
                    dialog.cancel();
                }
            });
            builder1.show();
        }

    }
    @Override
    public void onClick(View v) {
        finish();
    }
}