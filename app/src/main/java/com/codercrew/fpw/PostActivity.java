package com.codercrew.fpw;

import androidx.appcompat.app.AppCompatActivity;

import android.app.ProgressDialog;
import android.content.Context;
import android.content.Intent;
import android.database.Cursor;
import android.graphics.Bitmap;
import android.media.MediaScannerConnection;
import android.media.ThumbnailUtils;
import android.net.Uri;
import android.os.AsyncTask;
import android.os.Bundle;
import android.os.Environment;
import android.os.Handler;
import android.os.StrictMode;
import android.provider.MediaStore;
import android.util.Log;
import android.view.MotionEvent;
import android.view.View;
import android.view.inputmethod.InputMethodManager;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.ProgressBar;
import android.widget.TextView;
import android.widget.Toast;

import com.codercrew.fpw.ApiClient.ApiClient;
import com.codercrew.fpw.ApiClient.ApiInterface;
import com.codercrew.fpw.Utils.CountingFileRequestBody;
import com.codercrew.fpw.Utils.ProgressRequestBody;
import com.codercrew.fpw.Utils.SharedPrefrencesMain;
import com.codercrew.fpw.home.ui.newsfeed.NewsfeedViewModel;
import com.codercrew.fpw.home.ui.walls.WallsFragment;
import com.codercrew.fpw.model.GetPost;
import com.bumptech.glide.Glide;

import java.io.File;
import java.io.IOException;
import java.net.URISyntaxException;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;

import okhttp3.MediaType;
import okhttp3.MultipartBody;
import okhttp3.OkHttpClient;
import okhttp3.Request;
import okhttp3.RequestBody;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

import static android.os.Environment.getExternalStoragePublicDirectory;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

public class PostActivity extends AppCompatActivity implements ProgressRequestBody.UploadCallbacks{
    SharedPrefrencesMain sharedPrefrencesMain;
    private NewsfeedViewModel newsfeedViewModel;
    ProgressDialog pd;
    String url,type,desc;
    EditText editTextTitle;
    Long futureTimeInMillis=0L;
    boolean isFutureMessage = false;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_post);

        StrictMode.ThreadPolicy policy = new StrictMode.ThreadPolicy.Builder()
                .permitAll().build();
        StrictMode.setThreadPolicy(policy);

        sharedPrefrencesMain = new SharedPrefrencesMain(getApplicationContext());
        pd =new ProgressDialog(this);
        pd.setIndeterminate(false);
        pd.setCancelable(false);

//        pd.show();
        final ImageView imageViewProfile = findViewById(R.id.imgProfile);
        final ImageView imageViewSelect = findViewById(R.id.ivSelectedPic);
        final  ImageView imageViewPost = findViewById(R.id.ivPost);
        final TextView textViewName = findViewById(R.id.tvName);
        final EditText textViewTitle = findViewById(R.id.tvTitle);
        final EditText editTextCaption = findViewById(R.id.etCaption);

        Intent intent = getIntent();

        type = intent.getStringExtra("type");
        desc = intent.getStringExtra("desc");
        isFutureMessage = intent.getBooleanExtra("isFuture",false);
        futureTimeInMillis = intent.getLongExtra("futureTimeInMillis",0);
        textViewTitle.setText(desc);
        if(intent.getStringExtra("type").equals("image")) {
            String uri = intent.getStringExtra("Bitmap");
            url = uri;
            Glide.with(this).load(uri).into(imageViewSelect);
//            Bitmap bitmap = null;
//            try {
//                bitmap = MediaStore.Images.Media.getBitmap(getContentResolver(), uri);
//                imageViewSelect.setImageBitmap(bitmap);
//            } catch (IOException e) {
//                e.printStackTrace();
//            }
        }else {
            Uri uri = (Uri) intent.getParcelableExtra("Bitmap");
            String[] filePathColumn = {MediaStore.Images.Media.DATA};
            Cursor cursor = getContentResolver().query(uri, filePathColumn, null, null, null);
            cursor.moveToFirst();
            int columnIndex = cursor.getColumnIndex(filePathColumn[0]);
            String picturePath = cursor.getString(columnIndex);
            cursor.close();
//            new VideoCompressAsyncTask(getApplicationContext(),picturePath).execute("false", capturedUri.toString(), f.getPath());

//            File f = new File(getExternalStoragePublicDirectory(Environment.DIRECTORY_MOVIES) + "/Silicompressor/videos");
//            if (f.mkdirs() || f.isDirectory()) {
//                //compress and output new video specs
//                new VideoCompressAsyncTask(this,uri).execute("true", picturePath, f.getPath());
//
//            }
            url = picturePath;
            Bitmap bitmap = ThumbnailUtils.createVideoThumbnail(picturePath, MediaStore.Video.Thumbnails.FULL_SCREEN_KIND);
            imageViewSelect.setImageBitmap(bitmap);
        }
        if(!sharedPrefrencesMain.getProfileUrl().isEmpty())
            Glide.with(this).load(sharedPrefrencesMain.getProfileUrl()).into(imageViewProfile);
        textViewName.setText(sharedPrefrencesMain.getName());

        imageViewPost.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                String cap = "";
                if(!textViewTitle.getText().toString().isEmpty())
                    desc = textViewTitle.getText().toString();
                if(!editTextCaption.getText().toString().isEmpty())
                    cap = editTextCaption.getText().toString();
                if(type.equals("image")) {
                    CheckImage(new File(url),cap,desc);
//                    callApiUploadImage(url,desc,"1",cap);
                }else if(type.equals("video")) {
                    CheckVideo(new File(url),cap,desc);
//                    callApiUploadImage(url,desc,"2",cap);
                }
            }
        });

    }

    private void callApiUploadImage(String path,String text,String type,String caption) {
        if (!path.isEmpty()){
            File file = new File(path);

            MultipartBody.Builder builder = new MultipartBody.Builder();
            builder.setType(MultipartBody.FORM);
            MultipartBody.Part part;
            RequestBody requestBodyFdate=null;
            if(isFutureMessage && futureTimeInMillis!=null)
                requestBodyFdate = RequestBody.create(MediaType.parse("application/octet-stream"),futureTimeInMillis.toString());

            RequestBody requestBodyText = RequestBody.create(MediaType.parse("application/octet-stream"),text);
            RequestBody requestBodyType = RequestBody.create(MediaType.parse("application/octet-stream"),type);
            RequestBody requestBodyCode = RequestBody.create(MediaType.parse("application/octet-stream"),sharedPrefrencesMain.getFmCode());
            RequestBody requestBodyCaption = RequestBody.create(MediaType.parse("application/octet-stream"),caption);

            ProgressRequestBody fileBody;
            if(type.equals("1")) {
                fileBody = new ProgressRequestBody(file,"image", this);
                part = MultipartBody.Part.createFormData("image", file.getName(), fileBody);
            }
            else {
                fileBody = new ProgressRequestBody(file,"video", this);
                part = MultipartBody.Part.createFormData("video",file.getName(),fileBody);
            }
            Call<GetPost> apiResponseUploadImageCall = ApiClient.getClient().create(ApiInterface.class).createImagePost("Bearer "+sharedPrefrencesMain.getToken(),requestBodyCode,requestBodyText,requestBodyType,requestBodyCaption,requestBodyFdate,part);
            apiResponseUploadImageCall.enqueue(new Callback<GetPost>() {
                @Override
                public void onResponse(Call<GetPost> call, Response<GetPost> response) {
                    pd.dismiss();
                    if (response.isSuccessful()){

                        if (response.body()!=null){
                            file.delete();
                            MediaScannerConnection.scanFile(PostActivity.this, new String[]{Environment.getExternalStorageDirectory().toString()}, null, new MediaScannerConnection.OnScanCompletedListener() {
                                /*
                                 *   (non-Javadoc)
                                 * @see android.media.MediaScannerConnection.OnScanCompletedListener#onScanCompleted(java.lang.String, android.net.Uri)
                                 */
                                public void onScanCompleted(String path, Uri uri) {
                                    Log.e("ExternalStorage", "Scanned " + path + ":");
                                    Log.e("ExternalStorage", "-> uri=" + uri);
                                }
                            });
                            WallsFragment.ISUPDATED=true;
                            Toast.makeText(AppContext.getAppContext(), response.body().getMessage(), Toast.LENGTH_SHORT).show();
                            finish();
                        }
                    }
                    else{
                        Toast.makeText(AppContext.getAppContext(), ""+response.errorBody(), Toast.LENGTH_SHORT).show();
                    }
                }

                @Override
                public void onFailure(Call<GetPost> call, Throwable t) {
                    pd.dismiss();
                    Toast.makeText(AppContext.getAppContext(), ""+t.getMessage(), Toast.LENGTH_SHORT).show();
                }
            });
        }else{
            Toast.makeText(AppContext.getAppContext(), "No such file Found", Toast.LENGTH_SHORT).show();
        }
    }


    @Override
    public boolean dispatchTouchEvent(MotionEvent ev) {
        if (getCurrentFocus() != null) {
            InputMethodManager imm = (InputMethodManager) getSystemService(Context.INPUT_METHOD_SERVICE);
            imm.hideSoftInputFromWindow(getCurrentFocus().getWindowToken(), 0);
        }
        return super.dispatchTouchEvent(ev);
    }
    @Override
    public void onProgressUpdate(int percentage) {
        pd.setMessage(percentage+"% Uploading...");
        pd.show();

    }

    @Override
    public void onError() {

    }

    @Override
    public void onFinish() {

    }

    private void CheckImage(File file,String cap,String dsc){
        ProgressDialog progressDialog = new ProgressDialog(PostActivity.this);
        progressDialog.setMessage("Checking Image Validity....");
        progressDialog.show();
        new Thread(new Runnable() {
            @Override
            public void run() {
                OkHttpClient client = new OkHttpClient().newBuilder()
                        .build();
                MediaType mediaType = MediaType.parse("text/plain");
                RequestBody body = new MultipartBody.Builder().setType(MultipartBody.FORM)
                        .addFormDataPart("models","nudity")
                        .addFormDataPart("media",file.getName(),
                                RequestBody.create(MediaType.parse("application/octet-stream"),
                                        file))
                        .addFormDataPart("api_user",getString(R.string.api_key))
                        .addFormDataPart("api_secret",getString(R.string.api_secret))
                        .build();
                Request request = new Request.Builder()
                        .url("https://api.sightengine.com/1.0/check.json")
                        .method("POST", body)
                        .build();
                try {
                    okhttp3.Response response = client.newCall(request).execute();
                    Double raw,safe,partial;
                    try {
                        JSONObject jsonObject = new JSONObject(response.body().string());
                        JSONObject jsonObjectNudity = jsonObject.getJSONObject("nudity");
                        raw = jsonObjectNudity.getDouble("raw");
                        safe = jsonObjectNudity.getDouble("safe");
                        partial = jsonObjectNudity.getDouble("partial");
                        runOnUiThread(new Runnable() {
                            @Override
                            public void run() {
                                progressDialog.dismiss();
                            }
                        });
                        if(raw>=getMax(safe,partial)){
                            Toast.makeText(getApplicationContext(), "Selected image is against our privacy policies", Toast.LENGTH_SHORT).show();
                        }else {
                            runOnUiThread(new Runnable() {
                                @Override
                                public void run() {
                                    if(cap.length()>0 || desc.length()>0){
                                        CheckText(cap+"\n"+desc,"1");
                                    }else
                                        callApiUploadImage(url, desc, "1", cap);
                                }
                            });

                        }
                    } catch (NullPointerException | JSONException e) {
                        progressDialog.dismiss();
                        e.printStackTrace();
                    }
                } catch (IOException e) {
                    runOnUiThread(new Runnable() {
                        @Override
                        public void run() {
                            progressDialog.dismiss();
                        }
                    });
                    e.printStackTrace();
                }
            }
        }).start();
    }

    private void CheckVideo(File file,String cap,String dsc){
//        for (int i =0 ; i<10;i++) {
//            final int val = i;
//            new Handler().postAtFrontOfQueue(new Runnable() {
//                @Override
//                public void run() {
//                    System.out.println(val);
//                    Toast.makeText(getApplicationContext(), "" + val, Toast.LENGTH_SHORT).show();
//                }
//            });
//        }
        ProgressDialog progressDialog = new ProgressDialog(PostActivity.this);
        progressDialog.setMessage("Checking Video Validity....");
        progressDialog.show();
        new Thread(new Runnable() {
            @Override
            public void run() {
                OkHttpClient client = new OkHttpClient().newBuilder()
                        .build();
                MediaType mediaType = MediaType.parse("text/plain");
                RequestBody body = new MultipartBody.Builder().setType(MultipartBody.FORM)
                        .addFormDataPart("models","nudity")
                        .addFormDataPart("media",file.getName(),
                                RequestBody.create(MediaType.parse("application/octet-stream"),
                                        file))
                        .addFormDataPart("api_user",getString(R.string.api_key))
                        .addFormDataPart("api_secret",getString(R.string.api_secret))
                        .build();
                Request request = new Request.Builder()
                        .url("https://api.sightengine.com/1.0/video/check-sync.json")
                        .method("POST", body)
                        .build();
                try {
                    okhttp3.Response response = client.newCall(request).execute();
                    Double raw,safe,partial;
                    boolean isValid = true;
                    try {
                        JSONObject jsonObject = new JSONObject(response.body().string());
                        JSONObject jsonObjectDate = jsonObject.getJSONObject("data");
                        JSONArray jsonArrayFrames = jsonObjectDate.getJSONArray("frames");
                        runOnUiThread(new Runnable() {
                            @Override
                            public void run() {
                                progressDialog.dismiss();
                            }
                        });

                        for(int i = 0; i< jsonArrayFrames.length();i++){
                            JSONObject jsonObjectNudity = jsonArrayFrames.getJSONObject(i).getJSONObject("nudity");
                            raw = jsonObjectNudity.getDouble("raw");
                            safe = jsonObjectNudity.getDouble("safe");
                            partial = jsonObjectNudity.getDouble("partial");
                            if(raw>=getMax(safe,partial)){
                                isValid = false;
                            }
                            if(isValid && i==jsonArrayFrames.length()-1){
                                runOnUiThread(new Runnable() {
                                    @Override
                                    public void run() {
                                        if(cap.length()>0 || desc.length()>0){
                                            CheckText(cap+"\n"+desc,"2");
                                        }else
                                            callApiUploadImage(url, desc, "2", cap);
                                    }
                                });
                            }else if(!isValid && i==jsonArrayFrames.length()-1) {
                                Toast.makeText(getApplicationContext(), "Selected video is against our privacy policies", Toast.LENGTH_SHORT).show();
                            }
                        }
                    } catch (NullPointerException | JSONException e) {
                        runOnUiThread(new Runnable() {
                            @Override
                            public void run() {
                                progressDialog.dismiss();
                            }
                        });
                        e.printStackTrace();
                    }
                } catch (IOException e) {
                    runOnUiThread(new Runnable() {
                        @Override
                        public void run() {
                            progressDialog.dismiss();
                        }
                    });
                    e.printStackTrace();
                }
            }
        }).start();

    }

    private Double getMax(double safe,double partial){
        if(safe>partial)
            return safe;
        else return partial;
    }

    private void CheckText(String text,String type){
        ProgressDialog progressDialog = new ProgressDialog(PostActivity.this);
        progressDialog.setMessage("Checking Validity....");
        progressDialog.show();
        new Thread(new Runnable() {
            @Override
            public void run() {
                OkHttpClient client = new OkHttpClient().newBuilder()
                        .build();
                MediaType mediaType = MediaType.parse("text/plain");
                RequestBody body = new MultipartBody.Builder().setType(MultipartBody.FORM)
                        .addFormDataPart("models","text")
                        .addFormDataPart("text",text)
                        .addFormDataPart("api_user",getString(R.string.api_key))
                        .addFormDataPart("api_secret",getString(R.string.api_secret))
                        .addFormDataPart("lang","en")
                        .addFormDataPart("mode","standard")
                        .build();
                Request request = new Request.Builder()
                        .url("https://api.sightengine.com/1.0/text/check.json")
                        .method("POST", body)
                        .build();
                try {
                    okhttp3.Response response = client.newCall(request).execute();
                    try {
                        JSONObject jsonObject = new JSONObject(response.body().string());
                        JSONObject jsonObjectNudity = jsonObject.getJSONObject("profanity");

                        if(jsonObjectNudity.getJSONArray("matches").length()>0){
                            runOnUiThread(new Runnable() {
                                @Override
                                public void run() {
                                    progressDialog.dismiss();
                                    Toast.makeText(getApplicationContext(), "Caption/Description is against our privacy policies.", Toast.LENGTH_SHORT).show();
                                }
                            });
                        }else {
                            runOnUiThread(new Runnable() {
                                @Override
                                public void run() {
                                    callApiUploadImage(url, desc, type, text);
                                }
                            });

                        }

                    } catch (NullPointerException | JSONException e) {
                        runOnUiThread(new Runnable() {
                            @Override
                            public void run() {
                                progressDialog.dismiss();
                            }
                        });
                        e.printStackTrace();
                    }
                } catch (IOException e) {
                    runOnUiThread(new Runnable() {
                        @Override
                        public void run() {
                            progressDialog.dismiss();
                        }
                    });
                    e.printStackTrace();
                }
            }
        }).start();
    }

}